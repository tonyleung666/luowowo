package cn.wolfcode.luowowo.cache.service.impl;

import cn.wolfcode.luowowo.cache.service.IPoiRedisService;
import cn.wolfcode.luowowo.cache.util.RedisKeys;
import cn.wolfcode.luowowo.cache.util.StatisUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.List;

@Service
public class PoiRedisServiceImpl implements IPoiRedisService {
    @Autowired
    private StringRedisTemplate template;

    public boolean favor(Long uid, Long pid) {
        String favorKey = RedisKeys.USER_FAVOR_POIS.join(uid.toString());
        return StatisUtil.favor(pid, null, favorKey, null);
    }

    public List<Long> getFavorPoisByUser(Long uid) {
        String key = RedisKeys.USER_FAVOR_POIS.join(uid.toString());
        return JSON.parseArray(template.opsForValue().get(key), Long.class);
    }
}
