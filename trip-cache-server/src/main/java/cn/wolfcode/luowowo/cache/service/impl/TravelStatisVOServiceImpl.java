package cn.wolfcode.luowowo.cache.service.impl;

import cn.wolfcode.luowowo.article.domain.Travel;
import cn.wolfcode.luowowo.article.service.ITravelService;
import cn.wolfcode.luowowo.cache.service.ITravelStatisVOService;
import cn.wolfcode.luowowo.cache.util.RedisKeys;
import cn.wolfcode.luowowo.cache.util.StatisUtil;
import cn.wolfcode.luowowo.cache.vo.TravelStatisVO;
import cn.wolfcode.luowowo.common.util.BeanUtil;
import cn.wolfcode.luowowo.common.util.DateUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Service
public class TravelStatisVOServiceImpl implements ITravelStatisVOService {

    @Autowired
    private StringRedisTemplate template;

    @Reference
    private ITravelService travelService;


    public void increaseViewnum(Long tid, int num) {
        StatisUtil.increaseNum(StatisUtil.NUM_TYPE_VIEW,
                getTravelStatisVO(tid), RedisKeys.TRAVEL_STATIS_VO.join(tid.toString()), num);

    }

    public void increaseReplynum(Long tid, int num) {
        StatisUtil.increaseNum(StatisUtil.NUM_TYPE_REPLY,
                getTravelStatisVO(tid), RedisKeys.TRAVEL_STATIS_VO.join(tid.toString()), num);

    }

    public boolean favor(Long uid, Long tid) {
        TravelStatisVO travelStatisVO = getTravelStatisVO(tid);
        String favorKey = RedisKeys.USER_FAVOR_TRAVELS.join(uid.toString());
        String voKey = RedisKeys.TRAVEL_STATIS_VO.join(tid.toString());
        return StatisUtil.favor(tid, travelStatisVO, favorKey, voKey);
    }

    public boolean travelThumbup(Long uid, Long tid) {
        //判断当前请求是第一次顶, 还是第n次
        String key = RedisKeys.TRAVEL_STATIS_THUMB.join(tid.toString(), uid.toString());

        // 如果key不存在:第一次顶
        if (!template.hasKey(key)) {

            // thumbnum + 1
            StatisUtil.increaseNum(StatisUtil.NUM_TYPE_THUMB,
                    getTravelStatisVO(tid), RedisKeys.TRAVEL_STATIS_VO.join(tid.toString()), 1);

            //获取现在到当天最后一秒的间隔;
            Date now = new Date();
            Date endDate = DateUtil.getEndDate(now);
            long interval = DateUtil.getInterval(now, endDate);

            // 创建该key, 有效期设置到当天最后一秒
            template.opsForValue().set(key, "1", interval, TimeUnit.SECONDS);
            return true;
        }

        // 顶过了, 返回false
        return false;
    }

    public void travelShare(Long tid) {
        StatisUtil.increaseNum(StatisUtil.NUM_TYPE_SHARE,
                getTravelStatisVO(tid), RedisKeys.TRAVEL_STATIS_VO.join(tid.toString()), 1);
    }

    public List<Long> getFavorTravelsByUser(Long uid) {
        String key = RedisKeys.USER_FAVOR_TRAVELS.join(uid.toString());
        return JSON.parseArray(template.opsForValue().get(key), Long.class);
    }

    public boolean voKeyExists(Long tid) {
        String key = RedisKeys.TRAVEL_STATIS_VO.join(tid.toString());
        return template.hasKey(key);
    }

    public void setVO(TravelStatisVO vo) {
        String key = RedisKeys.TRAVEL_STATIS_VO.join(vo.getTravelId().toString());
        template.opsForValue().set(key, JSON.toJSONString(vo));
    }

    public List<TravelStatisVO> getVOs(String pattern) {
        List<TravelStatisVO> list = new ArrayList();
        Set<String> keys = template.keys(pattern);
        if (keys != null && keys.size() > 0) {
            for (String key : keys) {
                String voStr = template.opsForValue().get(key);
                TravelStatisVO vo = JSON.parseObject(voStr, TravelStatisVO.class);
                list.add(vo);
            }
        }
        return list;
    }


    public TravelStatisVO getTravelStatisVO(Long tid) {
        String key = RedisKeys.TRAVEL_STATIS_VO.join(tid.toString());
        TravelStatisVO vo = null;
        //1. 判断vo对象是否存在
        if (template.hasKey(key)) {
            //如果vo对象存在, 获取vo并解析
            String voStr = template.opsForValue().get(key);
            vo = JSON.parseObject(voStr, TravelStatisVO.class);
        } else {
            //如果vo对象不存在, 创建该对象, 然后存到redis中
            vo = new TravelStatisVO();
            Travel travel = travelService.get(tid);
            BeanUtil.copyProperties(vo, travel);
            vo.setTravelId(travel.getId());
        }
        setVO(vo);
        return vo;
    }

}