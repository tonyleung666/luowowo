package cn.wolfcode.luowowo.cache.util;

import cn.wolfcode.luowowo.common.util.BeanUtil;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class StatisUtil {
    private static StringRedisTemplate template;

    @Autowired
    private StringRedisTemplate autoTemplate;

    @PostConstruct
    private void setTemplate() {
        template = this.autoTemplate;
    }

    public static final int NUM_TYPE_VIEW = 1; //阅读数
    public static final int NUM_TYPE_REPLY = 2; //评论数
    public static final int NUM_TYPE_FAVOR = 3; //收藏数
    public static final int NUM_TYPE_SHARE = 4; //分享数
    public static final int NUM_TYPE_THUMB = 5; //点赞数

    public static void increaseNum(int type, Object vo, String key, int num) {
        // 统计数据 + num
        switch (type) {
            case NUM_TYPE_VIEW:
                BeanUtil.setProperty(vo, "viewnum",
                        num + Integer.valueOf((String) BeanUtil.getProperty(vo, "viewnum")));
                break;
            case NUM_TYPE_REPLY:
                BeanUtil.setProperty(vo, "replynum",
                        num + Integer.valueOf((String) BeanUtil.getProperty(vo, "replynum")));
                break;
            case NUM_TYPE_FAVOR:
                BeanUtil.setProperty(vo, "favornum",
                        num + Integer.valueOf((String) BeanUtil.getProperty(vo, "favornum")));
                break;
            case NUM_TYPE_SHARE:
                BeanUtil.setProperty(vo, "sharenum",
                        num + Integer.valueOf((String) BeanUtil.getProperty(vo, "sharenum")));
                break;
            case NUM_TYPE_THUMB:
                BeanUtil.setProperty(vo, "thumbsupnum",
                        num + Integer.valueOf((String) BeanUtil.getProperty(vo, "thumbsupnum")));
                break;

        }
        //更新vo对象
        template.opsForValue().set(key, JSON.toJSONString(vo));
    }

    /**
     * 收藏功能
     * @param targetId 被收藏的目标(攻略, 游记, 景点)id
     * @param statisVo 被收藏的目标(攻略, 游记, 景点)statisVo
     * @param favorKey redis中该用户收藏(攻略, 游记, 景点)的key
     * @param voKey    被操作的(攻略, 游记, 景点)的vokey
     * @return
     */
    public static boolean favor (Long targetId, Object statisVo, String favorKey, String voKey){
        List<Long> list = new ArrayList<>();
        //判断当前请求是取消收藏还是收藏
        if (template.hasKey(favorKey)) {
            String listStr = template.opsForValue().get(favorKey);
            // 参数2: list泛型的类型
            list = JSON.parseArray(listStr, Long.class);
        }
        if (list.contains(targetId)) {
            // 如果包含, 则取消收藏, favornum - 1 , 将 targetId 移除list;
            if (statisVo != null && voKey != null){
                increaseNum(NUM_TYPE_FAVOR, statisVo, voKey, -1);
            }
            list.remove(targetId);
        } else {
            // 如果不含, 则收藏, favor + 1, 将 targetId 加入list
            if (statisVo != null && voKey != null){
                increaseNum(NUM_TYPE_FAVOR, statisVo, voKey, 1);
            }
            list.add(targetId);
        }
        template.opsForValue().set(favorKey, JSON.toJSONString(list));
        return list.contains(targetId);
    }

}
