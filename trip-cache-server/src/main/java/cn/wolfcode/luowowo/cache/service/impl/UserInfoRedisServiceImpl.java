package cn.wolfcode.luowowo.cache.service.impl;

import cn.wolfcode.luowowo.cache.service.IUserInfoRedisService;
import cn.wolfcode.luowowo.cache.util.RedisKeys;
import cn.wolfcode.luowowo.common.exception.LogicException;
import cn.wolfcode.luowowo.member.domain.UserInfo;
import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.util.concurrent.TimeUnit;

@Service
public class UserInfoRedisServiceImpl implements IUserInfoRedisService {
    @Autowired
    private StringRedisTemplate template;

    public void setVerifyCode(String phone, String code) {
        String key = RedisKeys.VERIFY_CODE.join(phone);
        template.opsForValue().set(key, code, RedisKeys.VERIFY_CODE.getTime(), TimeUnit.SECONDS);

    }

    public String getVerifyCode(String phone) {
        ValueOperations<String, String> ops = template.opsForValue();
        String key = RedisKeys.VERIFY_CODE.join(phone);
        return template.opsForValue().get(key);
    }

    public void isResendVerifyCode(String phone) {
        String key = RedisKeys.VERIFY_CODE.join(phone);
        Long expire = template.getExpire(key, TimeUnit.SECONDS);
        if ((RedisKeys.VERIFY_CODE.getTime() - expire) < 10) {
            throw new LogicException("验证码请求太频繁, 请稍后再试!");
        }
    }

    public void setToken(String token, UserInfo userInfo) {
        String key = RedisKeys.LOGIN_TOKEN.join(token);
        template.opsForValue().set(key, JSON.toJSONString(userInfo), RedisKeys.LOGIN_TOKEN.getTime(), TimeUnit.SECONDS);
    }

    public UserInfo getUserByToken(String token) {
        if (token == null) {
            return null;
        }

        String key = RedisKeys.LOGIN_TOKEN.join(token);

        String userInfoStr = template.opsForValue().get(key);

        if (userInfoStr == null) {
            return null;
        }

        // 延长token时间
        template.expire(key, RedisKeys.LOGIN_TOKEN.getTime(), TimeUnit.MINUTES);

        return JSON.parseObject(userInfoStr, UserInfo.class);

    }

    @Override
    public String getNewVerifyCode(String key) {
        return template.opsForValue().get(key);
    }

}
