package cn.wolfcode.luowowo.cache.service.impl;

import cn.wolfcode.luowowo.cache.service.IQuestionStatisVOService;
import cn.wolfcode.luowowo.cache.util.RedisKeys;
import cn.wolfcode.luowowo.cache.util.RedisKeysForAnswer;
import cn.wolfcode.luowowo.cache.util.StatisUtil;
import cn.wolfcode.luowowo.cache.vo.QuestionStatisVO;
import cn.wolfcode.luowowo.common.util.BeanUtil;
import cn.wolfcode.luowowo.common.util.DateUtil;
import cn.wolfcode.luowowo.member.domain.UserInfo;
import cn.wolfcode.luowowo.member.service.IUserInfoService;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@Service
public class QuestionStatisVOServiceImpl implements IQuestionStatisVOService {

    @Autowired
    private StringRedisTemplate template;

    @Reference
    private IUserInfoService userInfoService;

    public boolean QuestionThumbup(Long uid, String aid) {
        //判断当前请求是第一次顶, 还是第n次
        String key = RedisKeysForAnswer.Answer_STATIS_THUMB.join(uid.toString(), aid.toString());

        UserInfo userInfo = userInfoService.get(uid);

        // 如果key不存在:第一次顶
        if (!template.hasKey(key)) {

            // thumbnum + 1
            StatisUtil.increaseNum(StatisUtil.NUM_TYPE_THUMB,
                    getQuestionStatisVO(aid,userInfo), RedisKeys.STRATEGY_STATIS_VO.join(aid.toString()), 1);

            //获取现在到当天最后一秒的间隔;
            Date now = new Date();
            Date endDate = DateUtil.getEndDate(now);
            long interval = DateUtil.getInterval(now, endDate);

            // 创建该key, 有效期设置到当天最后一秒
            template.opsForValue().set(key, "1", interval, TimeUnit.SECONDS);

            return true;
        }

        // 顶过了, 返回false
        return false;
    }
    public void setVO(QuestionStatisVO vo) {
        String key = RedisKeysForAnswer.Answer_STATIS_THUMB.join(vo.getAnswerId().toString());
        template.opsForValue().set(key, JSON.toJSONString(vo));
    }
    public QuestionStatisVO getQuestionStatisVO(String tid, UserInfo userInfo) {
        String key = RedisKeysForAnswer.Answer_STATIS_THUMB.join(tid.toString());
        QuestionStatisVO vo = null;
        //1. 判断vo对象是否存在
        if (template.hasKey(key)) {
            //如果vo对象存在, vo.viewnum + 1
            String voStr = template.opsForValue().get(key);
            vo = JSON.parseObject(voStr, QuestionStatisVO.class);
        } else {
            //如果vo对象不存在, 创建该对象, 然后存到redis中
            vo = new QuestionStatisVO();

            BeanUtil.copyProperties(vo, userInfo);
            vo.setAnswerId(tid);
            this.setVO(vo);
        }
        return vo;
    }
}
