package cn.wolfcode.luowowo.cache.service.impl;

import cn.wolfcode.luowowo.article.domain.StrategyDetail;
import cn.wolfcode.luowowo.article.service.IStrategyDetailService;
import cn.wolfcode.luowowo.cache.service.IStrategyStatisVOService;
import cn.wolfcode.luowowo.cache.util.RedisKeys;
import cn.wolfcode.luowowo.cache.util.StatisUtil;
import cn.wolfcode.luowowo.cache.vo.StrategyStatisVO;
import cn.wolfcode.luowowo.common.util.BeanUtil;
import cn.wolfcode.luowowo.common.util.DateUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Service
public class StrategyStatisVOServiceImpl implements IStrategyStatisVOService {



    @Autowired
    private StringRedisTemplate template;

    @Reference
    private IStrategyDetailService strategyDetailService;

    public void increaseViewnum(Long sid, int num) {
        StatisUtil.increaseNum(StatisUtil.NUM_TYPE_VIEW,
                getStrategyStatisVO(sid), RedisKeys.STRATEGY_STATIS_VO.join(sid.toString()), num);
    }

    public void increaseReplynum(Long sid, int num) {
        StatisUtil.increaseNum(StatisUtil.NUM_TYPE_REPLY,
                getStrategyStatisVO(sid), RedisKeys.STRATEGY_STATIS_VO.join(sid.toString()), num);
    }

    public boolean favor(Long uid, Long sid) {
        StrategyStatisVO strategyStatisVO = getStrategyStatisVO(sid);
        String favorKey = RedisKeys.USER_FAVOR_STRATEGIES.join(uid.toString());
        String voKey = RedisKeys.STRATEGY_STATIS_VO.join(sid.toString());
        return StatisUtil.favor(sid, strategyStatisVO, favorKey, voKey);
    }

    public boolean strategyThumbup(Long uid, Long sid) {
        //判断当前请求是第一次顶, 还是第n次
        String key = RedisKeys.STRATEGY_STATIS_THUMB.join(sid.toString(), uid.toString());

        // 如果key不存在:第一次顶
        if (!template.hasKey(key)) {

            // thumbnum + 1
            StatisUtil.increaseNum(StatisUtil.NUM_TYPE_THUMB,
                    getStrategyStatisVO(sid), RedisKeys.STRATEGY_STATIS_VO.join(sid.toString()), 1);

            //获取现在到当天最后一秒的间隔;
            Date now = new Date();
            Date endDate = DateUtil.getEndDate(now);
            long interval = DateUtil.getInterval(now, endDate);

            // 创建该key, 有效期设置到当天最后一秒
            template.opsForValue().set(key, "1", interval, TimeUnit.SECONDS);

            return true;
        }

        // 顶过了, 返回false
        return false;
    }

    public void strategyShare(Long sid) {
        StatisUtil.increaseNum(StatisUtil.NUM_TYPE_SHARE,
                    getStrategyStatisVO(sid), RedisKeys.STRATEGY_STATIS_VO.join(sid.toString()), 1);
    }


    public boolean voKeyExists(Long sid) {
        String key = RedisKeys.STRATEGY_STATIS_VO.join(sid.toString());
        return template.hasKey(key);
    }

    public void setVO(StrategyStatisVO vo) {
        String key = RedisKeys.STRATEGY_STATIS_VO.join(vo.getStrategyDetailId().toString());
        template.opsForValue().set(key, JSON.toJSONString(vo));
    }

    public List<StrategyStatisVO> getVOs(String pattern) {
        List<StrategyStatisVO> list = new ArrayList();
        Set<String> keys = template.keys(pattern);
        if (keys != null && keys.size() > 0) {
            for (String key : keys) {
                String voStr = template.opsForValue().get(key);
                StrategyStatisVO vo = JSON.parseObject(voStr, StrategyStatisVO.class);
                list.add(vo);
            }
        }
        return list;
    }

    public void addHotScore(Long sid, int score) {
        String key = RedisKeys.STRATEGY_ZSET_HOT.getPrefix();
        ZSetOperations<String, String> zopt = template.opsForZSet();
        zopt.incrementScore(key, RedisKeys.STRATEGY_STATIS_VO.join(sid.toString()), score);
    }

    public void addAbroadScore(Long sid, int score) {
        StrategyStatisVO vo = getStrategyStatisVO(sid);
        String key = null;
        if (vo.isIsabroad()) {
            key = RedisKeys.STRATEGY_ZSET_ABROAD.getPrefix();
        } else {
            key = RedisKeys.STRATEGY_ZSET_INLAND.getPrefix();
        }
        template.opsForZSet().incrementScore(key, RedisKeys.STRATEGY_STATIS_VO.join(sid.toString()), score);
    }

    public List<StrategyStatisVO> queryHotCommends() {
        return this.queryCommends(RedisKeys.STRATEGY_ZSET_HOT.getPrefix(), 0, 9);
    }

    public List<StrategyStatisVO> queryAbroadCommends() {
        return this.queryCommends(RedisKeys.STRATEGY_ZSET_ABROAD.getPrefix(), 0, 9);
    }

    public List<StrategyStatisVO> queryInlandCommends() {
        return this.queryCommends(RedisKeys.STRATEGY_ZSET_INLAND.getPrefix(), 0, 9);
    }

    public boolean isColumnExists(String cdKey, Long sid) {
        return template.opsForZSet().rank(cdKey, RedisKeys.STRATEGY_STATIS_VO.join(sid.toString())) != null;
    }

    public List<Long> getFavorStrategiesByUser(Long uid) {
        String key = RedisKeys.USER_FAVOR_STRATEGIES.join(uid.toString());
        return JSON.parseArray(template.opsForValue().get(key), Long.class);
    }

    public List<StrategyStatisVO> queryCommends(String key, long begin, long end) {
        List<StrategyStatisVO> list = new ArrayList();
        Set<String> voKeys = template.opsForZSet().reverseRange(key, begin, end);
        if (voKeys != null && voKeys.size() > 0) {
            for (String voKey : voKeys) {
                String voStr = template.opsForValue().get(voKey);
                list.add(JSON.parseObject(voStr, StrategyStatisVO.class));
            }
        }
        return list;
    }

    public StrategyStatisVO getStrategyStatisVO(Long sid) {
        String key = RedisKeys.STRATEGY_STATIS_VO.join(sid.toString());
        StrategyStatisVO vo = null;
        //1. 判断vo对象是否存在
        if (template.hasKey(key)) {
            //如果vo对象存在, vo.viewnum + 1
            String voStr = template.opsForValue().get(key);
            vo = JSON.parseObject(voStr, StrategyStatisVO.class);
        } else {
            //如果vo对象不存在, 创建该对象, 然后存到redis中
            vo = new StrategyStatisVO();
            StrategyDetail detail = strategyDetailService.get(sid);
            BeanUtil.copyProperties(vo, detail);
            vo.setDestId(detail.getDest().getId());
            vo.setDestName(detail.getDest().getName());
            vo.setStrategyDetailId(detail.getId());
            this.setVO(vo);
        }
        return vo;
    }
}
