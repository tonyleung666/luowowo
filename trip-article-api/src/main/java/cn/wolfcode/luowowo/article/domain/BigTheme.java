package cn.wolfcode.luowowo.article.domain;

import cn.wolfcode.luowowo.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

/**
 * 大主题
 */
@Setter
@Getter
public class BigTheme extends BaseDomain {



    private Long id;        //id
    private String title;  //大主题名称

}