package cn.wolfcode.luowowo.article.domain;

import cn.wolfcode.luowowo.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 景点
 */
@Getter
@Setter
public class Poi extends BaseDomain{

    private Poi parent; //上级景点
    private Destination dest;   //所属目的地
    private String name;    //名称
    private String english; //英文名
    private String info;    //景点信息
    private String summary; //简介
    private String tel;     //电话
    private String web;     //网址
    private String visitTime;   //参考时长
    private String imgUrls;     //图片路径
    private String traffics;    //交通信息
    private String ticket;      //门票信息
    private String openHour;    //开放时间
    private int imgnum;         //图片数量
    private int replynum;       //评论数
    private int deep;           //深度
    private List<Poi> children;  //子景点集合

    //从数据库拿出的imgUrls拼接字符串转换成数组
    public String[] getImgUrls(){
        String[] urls = null;
        if(StringUtils.hasLength(imgUrls)){
            urls = imgUrls.split(";");
        }
        return urls;
    }
}
