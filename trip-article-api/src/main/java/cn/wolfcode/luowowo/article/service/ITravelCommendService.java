package cn.wolfcode.luowowo.article.service;

import cn.wolfcode.luowowo.article.domain.TravelCommend;
import cn.wolfcode.luowowo.article.query.TravelCommendQuery;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * 游记推荐
 */
public interface ITravelCommendService {

    /**
     * 分页
     * @param qo
     * @return
     */
    PageInfo query(TravelCommendQuery qo);

    /**
     * 游记推荐前5列表
     * @return
     */
    List<TravelCommend> getTop5();

}
