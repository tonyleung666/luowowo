package cn.wolfcode.luowowo.article.service;

import cn.wolfcode.luowowo.article.domain.BlackList;

import java.util.List;

public interface IBlackListService {
    int deleteByPrimaryKey(Long id);

    int insert(BlackList record);

    BlackList selectByPrimaryKey(Long id);

    List<BlackList> selectAll();

    int updateByPrimaryKey(BlackList record);

}
