package cn.wolfcode.luowowo.article.domain;

import cn.wolfcode.luowowo.common.domain.BaseDomain;
import cn.wolfcode.luowowo.common.util.DateUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * 航班信息
 */
@Getter
@Setter
@ToString
public class FlightInfo extends BaseDomain{
    private String airline;
    private String flightNo;
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date depTime;

//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date arrTime;

    private FlightAirport depPort;
    private FlightAirport arrPort;
    private Integer price;

    public String getInterval(){
        long between = DateUtil.getInterval(this.getArrTime(), this.getDepTime());
        return DateUtil.secToTime(between);
    }
}
