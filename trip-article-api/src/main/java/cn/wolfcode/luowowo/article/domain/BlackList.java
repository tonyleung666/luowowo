package cn.wolfcode.luowowo.article.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
@Getter
@Setter
public class BlackList implements Serializable {
    private Long id;
    private Long userId;
    private Long blackName;
}