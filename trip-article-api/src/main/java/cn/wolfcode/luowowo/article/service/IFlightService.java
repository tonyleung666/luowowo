package cn.wolfcode.luowowo.article.service;

import cn.wolfcode.luowowo.article.domain.FlightAirport;
import cn.wolfcode.luowowo.article.domain.FlightInfo;
import cn.wolfcode.luowowo.article.domain.FlightOrder;
import cn.wolfcode.luowowo.article.query.FlightInfoQuery;
import cn.wolfcode.luowowo.common.exception.LogicException;

import java.util.List;
import java.util.Map;

/**
 * 航班服务接口
 */
public interface IFlightService {
    /**
     * 根据首字母查询机场
     * @param beginInitial
     * @param endInitial
     * @return
     */
    Map queryByInitial(String beginInitial, String endInitial);

    /**
     * 查询热门城市
     * @return
     */
    List<FlightAirport> queryHots();


    /**
     * 查询航班信息
     * @param qo
     * @return
     */
    List<FlightInfo> queryInfo(FlightInfoQuery qo);

    /**
     * 根据id查询航班信息
     * @param id
     * @return
     */
    FlightInfo getInfoById(Long id);

    /**
     * 新增航班订单
     * @param order
     */
    void saveOrder (FlightOrder order) throws LogicException;

    /**
     * 根据用户查航班
     * @param uid
     * @return
     */
    List<FlightOrder> queryByUser(Long uid);
}
