package cn.wolfcode.luowowo.article.domain;

import cn.wolfcode.luowowo.common.domain.BaseDomain;
import cn.wolfcode.luowowo.member.domain.UserInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@ToString
public class TicketIndent extends BaseDomain {
    private BigDecimal price;
    private ScenicSpot spot;
    private Long ticketNum;
    private String useTime;
    private UserInfo user;
    private String buyname;
    private String phone;
    private String userIdcard;
    private Long status;
    private Date payTime;
}