package cn.wolfcode.luowowo.article.service;

import cn.wolfcode.luowowo.article.domain.TicketIndent;
import cn.wolfcode.luowowo.common.exception.LogicException;

public interface ITicketIndentService {
    TicketIndent save(TicketIndent ticketIndent) throws LogicException;

    void updateByIndent(TicketIndent ticketIndent);

    TicketIndent selectByIndentId(Long id);
}
