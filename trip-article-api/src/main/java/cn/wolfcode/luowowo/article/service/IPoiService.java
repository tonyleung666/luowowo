package cn.wolfcode.luowowo.article.service;

import cn.wolfcode.luowowo.article.domain.Poi;
import cn.wolfcode.luowowo.article.vo.PoiDestVo;

import java.util.List;

/**
 * 景点服务
 */
public interface IPoiService {

    /**
     * 查单个
     *
     * @param id
     * @return
     */
    Poi get(Long id);

    /**
     * 根据上级id查点评数前五的列表
     *
     * @param parentId
     * @return
     */
    List<Poi> queryTop5ByParent(Long parentId);

    /**
     * 修改评论数
     *
     * @param num
     * @param poiId
     */
    void updateReplynum(Long poiId, int num);

    /**
     * 根据目的地id查询deep为1的景点
     *
     * @param destId
     * @return
     */
    Poi getByDestId(Long destId);

    /**
     * 根据目的地id查询排行前三的景点
     *
     * @param destId
     * @return
     */
    List<Poi> queryTop3ByParentId(Long destId);

    /**
     * 查景点及其目的地
     * @param pids
     * @return
     */
    List<PoiDestVo> queryPoisAndDest(List<Long> pids);
}