package cn.wolfcode.luowowo.article.service;


import cn.wolfcode.luowowo.article.domain.StrategyCatalog;
import cn.wolfcode.luowowo.article.query.StrategyCatalogQuery;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * 攻略分类服务
 */
public interface IStrategyCatalogService {

    /**
     * 分页查询
     * @param qo
     * @return
     */
    PageInfo query(StrategyCatalogQuery qo);

    /**
     * 添加/更新
     * @param strategyCatalog
     */
    void saveOrUpdate(StrategyCatalog strategyCatalog);

    /**
     * 查单个
     * @param id
     * @return
     */
    StrategyCatalog get(Long id);

    /**
     * 根据大攻略id查分类
     * @param strategyId
     */
    List<StrategyCatalog> queryByStrategyId(Long strategyId);

    /**
     * 通过目的地id查攻略分类
     * @param id
     * @return
     */
    List<StrategyCatalog> queryCatalogsByDestId(Long id);
}
