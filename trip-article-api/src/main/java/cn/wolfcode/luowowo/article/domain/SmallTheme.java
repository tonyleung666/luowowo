package cn.wolfcode.luowowo.article.domain;

import cn.wolfcode.luowowo.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

/**
 * 小主题
 */
@Setter
@Getter
public class SmallTheme extends BaseDomain {



    private Long id;        //id
    private String smallTitle;  //小主题名称
    private Long parentId;  //大主题id
    private String coverUrl; //小主题的背景图片

}