package cn.wolfcode.luowowo.article.service;

import cn.wolfcode.luowowo.article.domain.Hotel;

import java.util.List;

/**
 * 酒店服务
 */
public interface IHotelService {
    /**
     * 根据目的地id查询酒店
     * @param destId
     * @return
     */
    List<Hotel> queryHotelByDestId(Long destId);


}
