package cn.wolfcode.luowowo.article.service;


import cn.wolfcode.luowowo.article.domain.StrategyContent;
import cn.wolfcode.luowowo.article.domain.StrategyDetail;
import cn.wolfcode.luowowo.article.query.StrategyDetailQuery;
import cn.wolfcode.luowowo.cache.vo.StrategyStatisVO;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * 攻略明细服务
 */
public interface IStrategyDetailService {
    /**
     * 分页查询
     * @param qo
     * @return
     */
    PageInfo query(StrategyDetailQuery qo);
    /**
     * 查询所有
     * @return
     */
    List<StrategyDetail> list();
    /**
     * 查单个
     * @param id
     * @return
     */
    StrategyDetail get(Long id);

    /**
     * 新增或修改
     * @param strategyDetail
     * @param tags
     */
    void saveOrUpdate(StrategyDetail strategyDetail, String tags);

    /**
     * 根据id查内容
     * @param id
     * @return
     */
    StrategyContent getContent(Long id);

    /**
     * 点击量前三的攻略明细
     * @param id
     * @return
     */
    List<StrategyDetail> queryViewNumTop3(Long id);

    /**
     * 根据vo更新detail的统计数据
     * @param vo
     */
    void updateStatis(StrategyStatisVO vo);

    /**
     * 点击量第一的攻略明细
     * @param destId
     * @return
     */
    StrategyDetail queryViewNumTop1(Long destId);

    /**
     * 查多个
     * @param sids
     * @return
     */
    List<StrategyDetail> queryStrategies(List<Long> sids);
}
