package cn.wolfcode.luowowo.article.vo;

import cn.wolfcode.luowowo.article.domain.Destination;
import cn.wolfcode.luowowo.article.domain.Poi;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class PoiDestVo implements Serializable{
    private Destination dest;
    private List<Poi> pois = new ArrayList<>();
}
