package cn.wolfcode.luowowo.article.service;

import cn.wolfcode.luowowo.article.domain.Travel;
import cn.wolfcode.luowowo.article.query.TravelQuery;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * 游记服务
 */
public interface ITravelService {
    /**
     * 查询游记分页
     * @param qo
     * @return
     */
    PageInfo query(TravelQuery qo);

    /**
     * 查单个
     * @param id
     * @return
     */
    Travel get(Long id);

    /**
     * 查阅读量前三的游记
     * @param destId
     * @return
     */
    List<Travel> queryViewNumTop3(Long destId);

    /**
     * 保存或更新
     * @param travel
     * @return
     */
    Long saveOrUpdate(Travel travel);

    /**
     * 查阅游记列表
     * @return
     */
    List<Travel> list();

    /**
     * 获取游记封面
     * @param travelId
     * @return
     */
    Travel getCoverUrlById(Long travelId);
    /**
     * 根据目的地的id查询游记集合
     * @param destId
     * @return
     */
    List<Travel> queryByDestId(Long destId);

    /**
     * 查多个
     * @param tids
     * @return
     */
    List<Travel> queryTravels(List<Long> tids);


}
