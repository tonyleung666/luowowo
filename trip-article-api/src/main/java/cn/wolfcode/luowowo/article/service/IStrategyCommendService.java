package cn.wolfcode.luowowo.article.service;

import cn.wolfcode.luowowo.article.domain.StrategyCommend;
import cn.wolfcode.luowowo.article.query.StrategyCommendQuery;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * 攻略推荐
 */
public interface IStrategyCommendService {

    /**
     * 分页
     * @param qo
     * @return
     */
    PageInfo query(StrategyCommendQuery qo);

    /**
     * 攻略推荐前5列表
     * @return
     */
    List<StrategyCommend> getTop5();
}
