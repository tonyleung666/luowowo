package cn.wolfcode.luowowo.article.service;

import cn.wolfcode.luowowo.article.domain.BigTheme;

import java.util.List;

/**
 * 大主题的服务
 */
public interface IBigThemeService {


    List<BigTheme> queryBigThemeByDestId(Long id);
}
