package cn.wolfcode.luowowo.article.query;

import cn.wolfcode.luowowo.common.query.QueryObject;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Setter
@Getter
public class HotelQuery extends QueryObject {

    private Long destId;//目的地id

    private String destName;//目的地名称

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date checkIn ;  //入住时间

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date checkOut ;//退房时间

    private int num = -1;//出行人数

    public Long getDays(){
        Long days = (this.checkOut.getTime()-this.checkIn.getTime())/(24*3600*1000);
        return days;
    }
}
