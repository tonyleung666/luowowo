package cn.wolfcode.luowowo.article.query;

import cn.wolfcode.luowowo.common.query.QueryObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TravelQuery extends QueryObject {
    private Long destId = -1L;//目的地id
    private int dayType = -1; //出行天数
    private int travelTimeType = -1;//出发时间
    private int perExpendType = -1;//人均花费
    private int orderType = -1;//最新
    private String orderBy = "t.releaseTime DESC";
    private Long userId;//用户id

    public String getOrderBy() {
        return orderType == 1 ? "t.releaseTime DESC" : "t.viewnum DESC";
    }

    public TravelCondition getDays() {
        return TravelCondition.TRAVEL_DAYS.get(dayType);
    }

    public TravelCondition getPerExpends() {
        return TravelCondition.TRAVEL_PER_EXPEND.get(perExpendType);
    }

    public TravelCondition getTravelTime() {
        return TravelCondition.TRAVEL_TIME.get(travelTimeType);
    }
}
