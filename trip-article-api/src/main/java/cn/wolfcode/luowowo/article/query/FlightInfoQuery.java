package cn.wolfcode.luowowo.article.query;

import cn.wolfcode.luowowo.common.query.QueryObject;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
@Getter
@Setter
@ToString
public class FlightInfoQuery extends QueryObject {
    private Long orgCity;
    private Long dstCity;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date depTime;
}
