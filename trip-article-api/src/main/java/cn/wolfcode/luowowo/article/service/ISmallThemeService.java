package cn.wolfcode.luowowo.article.service;

import cn.wolfcode.luowowo.article.domain.SmallTheme;

import java.util.List;

/**
 * 小主题的服务
 */
public interface ISmallThemeService {

    /**
     * 根据目的地id查出小主题
     * @param destId
     * @return
     */
    List<SmallTheme> querySmallThemeByDestId(Long destId);
    /**
     * 根据目的地id查出小主题
     * @param BigThemeId
     * @return
     */
    List<SmallTheme> querySmallThemeByBigThemeId(Long BigThemeId);
}
