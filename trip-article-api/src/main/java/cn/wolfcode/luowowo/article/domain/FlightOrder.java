package cn.wolfcode.luowowo.article.domain;

import cn.wolfcode.luowowo.common.domain.BaseDomain;
import cn.wolfcode.luowowo.member.domain.UserInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 航班订单
 */
@Getter
@Setter
@ToString
public class FlightOrder extends BaseDomain {
   private UserInfo user;			//下单用户
   private FlightInfo flightInfo;	//航班
   private String passenger;		//乘客姓名
   private String idcardNo;			//乘客证件号
   private String linkman;	    	//联系人姓名
   private String linktel;		    //联系人电话

}
