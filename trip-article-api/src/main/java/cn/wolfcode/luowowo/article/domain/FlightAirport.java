package cn.wolfcode.luowowo.article.domain;

import cn.wolfcode.luowowo.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 机场
 */
@Getter
@Setter
@ToString
public class FlightAirport extends BaseDomain{
    public static final int ISHOT_NO = 0;
    public static final int ISHOT_YES = 1;

    private String name;
    private String aircode;
    private String initial;
    private Destination dest;
    private int ishot = ISHOT_NO;

}
