package cn.wolfcode.luowowo.article.service;

import cn.wolfcode.luowowo.article.domain.ScenicSpotTag;

import java.util.List;

public interface IScenicSpotTagService {
    List<ScenicSpotTag> list();
}
