package cn.wolfcode.luowowo.article.domain;

import cn.wolfcode.luowowo.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class ScenicSpot extends BaseDomain {
    private String landPark;
    private BigDecimal ticketPrice;
    private Destination dest;
    private String introduce;
    private String state;
    private String iandParkImg;
    private ScenicSpotTag tag;
    private Integer rank;
    private String address;
    private String beginTime;
    private Integer hot;
    private BigDecimal childPrice;
    private String reputably;
}