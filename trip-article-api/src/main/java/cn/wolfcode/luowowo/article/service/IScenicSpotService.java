package cn.wolfcode.luowowo.article.service;

import cn.wolfcode.luowowo.article.domain.ScenicSpot;
import cn.wolfcode.luowowo.article.query.ScenicSpotQuery;

import java.util.List;

public interface IScenicSpotService {
    /**
     * 查询所有景点门票
     * @return
     */
    List<ScenicSpot> list();

    /**
     * 获取前4个门票信息
     * @return
     */
    List<ScenicSpot> getTicketTop4();

    /**
     * 查询广州的门票信息
     * @param qo
     * @return
     */
    List<ScenicSpot> query(ScenicSpotQuery qo);

    ScenicSpot querySpotById(Long id);
}
