package cn.wolfcode.luowowo.article.service;

import cn.wolfcode.luowowo.article.domain.Destination;
import cn.wolfcode.luowowo.article.query.DestinationQuery;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * 目的的服务
 */
public interface IDestinationService {

    /**
     * 通过层次查询目的地
     * @param deep
     * @return
     */
    List<Destination> getDestsByDeep(int deep);


    /**
     * 添加或更新
     * @param destination
     */
    void saveOrUpdate(Destination destination);

    /**
     * 关联的目的地集合
     * @param rid
     * @return
     */
    List<Destination> getDestByRegionId(Long rid);

    /**
     * 分页查询
     * @param qo
     * @return
     */
    PageInfo query(DestinationQuery qo);

    /**
     * 查询导航吐司
     * @param parentId
     * @return
     */
    List<Destination> getToasts(Long parentId);

    /**
     * 通过目的地获取该目的地的国家
     * @param id
     * @return
     */
    Destination getCountry(Long id);

    /**
     * 获取省份
     * @param id
     * @return
     */
    Destination getProvince(Long id);

    /**
     * 查询所有
     * @return
     */
    List<Destination> list();

    Destination findDestById(Long destId);
    /**
     * 查询目的地的游记数量
     * @return
     * @param id
     */
    Long queryCountOfTravelByDest(Long id);
    /**
     * 查询中国的城市
     * @return
     * @param
     */
    List<Destination> selectCityOfChina(Long parentId);
    /**
     * 根据父id查下一级的目的地
     * @return
     * @param
     */
    List<Destination> queryDestByParentId(Long destId);
}
