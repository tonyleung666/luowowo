package cn.wolfcode.luowowo.article.domain;

import cn.wolfcode.luowowo.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Hotel extends BaseDomain {

    private String name;
    private String englishName;
    private String address;
    private String summary;
    private Long priceForOne;//单价,可以设置多种价格,这里不设置表了,只给一个
    private Long destId;
    private String coverUrl;
    private Double score;//评分
    private String mark;//评价
}
