package cn.wolfcode.luowowo.article.query;

import cn.wolfcode.luowowo.common.query.QueryObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScenicSpotQuery extends QueryObject{
    private Long destId=3261L;
    private Long tagId=1L;
}
