package cn.wolfcode.luowowo.mgrsite.controller;

import cn.wolfcode.luowowo.article.domain.StrategyDetail;
import cn.wolfcode.luowowo.article.query.StrategyDetailQuery;
import cn.wolfcode.luowowo.article.service.*;
import cn.wolfcode.luowowo.common.util.AjaxResult;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.xml.soap.Detail;

@Controller
@RequestMapping("/strategyDetail")
public class StrategyDetailController {

    @Reference
    private IStrategyDetailService strategyDetailService;

    @Reference
    private IDestinationService destinationService;

    @Reference
    private IStrategyService strategyService;

    @Reference
    private IStrategyThemeService strategyThemeService;

    @Reference
    private IStrategyCatalogService strategyCatalogService;

    @Reference
    private  IStrategyTagService strategyTagService;

    @RequestMapping("/list")
    public String list(Model model, @ModelAttribute("qo")StrategyDetailQuery qo){
        //pageInfo
        model.addAttribute("pageInfo", strategyDetailService.query(qo));
        return "strategyDetail/list";
    }

    /**
     * 跳转编辑页面
     * @param model
     * @param id
     * @return
     */
    @RequestMapping("/input")
    public String input(Model model, Long id){
        //strategies
        model.addAttribute("strategies", strategyService.list());
        //themes
        model.addAttribute("themes", strategyThemeService.list());
        if (id != null){
            //strategyDetail
            StrategyDetail strategyDetail = strategyDetailService.get(id);
            strategyDetail.setStrategyContent(strategyDetailService.getContent(id));
            model.addAttribute("strategyDetail", strategyDetail);

            model.addAttribute("tags", strategyTagService.getTagString(id));
        }
        return "strategyDetail/input";
    }

    /**
     * 编辑页面二级联动查询分类
     * @param strategyId
     * @return
     */
    @RequestMapping("/getCatalogByStrategyId")
    @ResponseBody
    public Object getCatalogByStrategyId(Long strategyId){
        return strategyCatalogService.queryByStrategyId(strategyId);
    }

    @RequestMapping("/saveOrUpdate")
    @ResponseBody
    public Object saveOrUpdate(StrategyDetail detail, String tags){
        strategyDetailService.saveOrUpdate(detail, tags);
        return AjaxResult.success();
    }
}
