package cn.wolfcode.luowowo.mgrsite.controller;

import cn.wolfcode.luowowo.article.query.StrategyCommendQuery;
import cn.wolfcode.luowowo.article.service.IStrategyCommendService;
import cn.wolfcode.luowowo.article.service.IStrategyDetailService;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("strategyCommend")
public class StrategyCommendController {

    @Reference
    private IStrategyCommendService strategyCommendService;

    @Reference
    private IStrategyDetailService strategyDetailService;

    @RequestMapping("/list")
    public String list(Model model, @ModelAttribute("qo")StrategyCommendQuery qo){

        //pageInfo  page
        model.addAttribute("pageInfo", strategyCommendService.query(qo));

        model.addAttribute("details", strategyDetailService.list());
        return  "strategyCommend/list";
    }

}
