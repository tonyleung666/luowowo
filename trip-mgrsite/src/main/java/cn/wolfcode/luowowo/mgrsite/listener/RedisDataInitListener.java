package cn.wolfcode.luowowo.mgrsite.listener;

import cn.wolfcode.luowowo.article.domain.StrategyDetail;
import cn.wolfcode.luowowo.article.service.IStrategyDetailService;
import cn.wolfcode.luowowo.cache.service.IStrategyStatisVOService;
import cn.wolfcode.luowowo.cache.util.RedisKeys;
import cn.wolfcode.luowowo.cache.vo.StrategyStatisVO;
import cn.wolfcode.luowowo.common.util.BeanUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RedisDataInitListener implements ApplicationListener<ContextRefreshedEvent> {
    @Reference
    private IStrategyDetailService strategyDetailService;

    @Reference
    private IStrategyStatisVOService strategyStatisVOService;

    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        System.out.println("=========================VO初始化-begin====================");
        List<StrategyDetail> list = strategyDetailService.list();
        StrategyStatisVO vo = null;
        for (StrategyDetail detail : list) {
            if (strategyStatisVOService.voKeyExists(detail.getId())) {
                continue;
            }
            vo = new StrategyStatisVO();
            BeanUtil.copyProperties(vo, detail);
            vo.setDestId(detail.getDest().getId());
            vo.setDestName(detail.getDest().getName());
            vo.setStrategyDetailId(detail.getId());
            strategyStatisVOService.setVO(vo);
        }
        System.out.println("=========================VO初始化-end====================");

        System.out.println("=========================热门推荐初始化-begin====================");
        for (StrategyDetail detail : list) {
            if (strategyStatisVOService.isColumnExists(RedisKeys.STRATEGY_ZSET_HOT.getPrefix(), detail.getId())){
                continue;
            }
            int score = detail.getViewnum() + detail.getReplynum();
            strategyStatisVOService.addHotScore(detail.getId(), score);
        }
        System.out.println("=========================热门推荐初始化-end====================");

        System.out.println("=========================国内推荐初始化-begin====================");
        for (StrategyDetail detail : list) {
            if (strategyStatisVOService.isColumnExists(RedisKeys.STRATEGY_ZSET_INLAND.getPrefix(), detail.getId())){
                continue;
            }
            int score = detail.getFavornum() + detail.getThumbsupnum();
            strategyStatisVOService.addAbroadScore(detail.getId(), score);
        }
        System.out.println("=========================国内推荐初始化-end====================");

        System.out.println("=========================海外推荐初始化-begin====================");
        for (StrategyDetail detail : list) {
            if (strategyStatisVOService.isColumnExists(RedisKeys.STRATEGY_ZSET_ABROAD.getPrefix(), detail.getId())){
                continue;
            }
            int score = detail.getFavornum() + detail.getThumbsupnum();
            strategyStatisVOService.addAbroadScore(detail.getId(), score);
        }
        System.out.println("=========================海外推荐初始化-end====================");
    }
}
