package cn.wolfcode.luowowo.mgrsite.job;

import cn.wolfcode.luowowo.article.service.IStrategyDetailService;
import cn.wolfcode.luowowo.cache.service.IStrategyStatisVOService;
import cn.wolfcode.luowowo.cache.util.RedisKeys;
import cn.wolfcode.luowowo.cache.vo.StrategyStatisVO;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RedisDataPersistenceJob {
    @Reference
    private IStrategyDetailService strategyDetailService;

    @Reference
    private IStrategyStatisVOService strategyStatisVOService;

    @Scheduled(cron = "0/5 * * * * ?")
    public void redisDataPersistence(){
        String pattern = RedisKeys.STRATEGY_STATIS_VO.join("*");
        List<StrategyStatisVO> list = strategyStatisVOService.getVOs(pattern);
        for (StrategyStatisVO vo : list) {
            strategyDetailService.updateStatis(vo);
        }
    }


}
