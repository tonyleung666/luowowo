package cn.wolfcode.luowowo.mgrsite.controller;

import cn.wolfcode.luowowo.article.query.TravelCommendQuery;
import cn.wolfcode.luowowo.article.service.ITravelCommendService;
import cn.wolfcode.luowowo.article.service.ITravelService;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("travelCommend")
public class TravelCommendController {

    @Reference
    private ITravelCommendService travelCommendService;

    @Reference
    private ITravelService travelService;

    @RequestMapping("/list")
    public String list(Model model, @ModelAttribute("qo")TravelCommendQuery qo){

        //pageInfo  page
        model.addAttribute("pageInfo", travelCommendService.query(qo));

        model.addAttribute("details", travelService.list());
        return  "travelCommend/list";
    }


}
