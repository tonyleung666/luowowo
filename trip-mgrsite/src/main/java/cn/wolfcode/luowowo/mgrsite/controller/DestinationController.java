package cn.wolfcode.luowowo.mgrsite.controller;

import cn.wolfcode.luowowo.article.query.DestinationQuery;
import cn.wolfcode.luowowo.article.service.IDestinationService;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("destination")
public class DestinationController {


    @Reference
    private IDestinationService destinationService;


    @RequestMapping("/list")
    public String list(Model model, @ModelAttribute("qo")DestinationQuery qo){

        //pageInfo
        model.addAttribute("pageInfo",destinationService.query(qo));


        model.addAttribute("toasts", destinationService.getToasts(qo.getParentId()));


        return "destination/list";

    }

















}
