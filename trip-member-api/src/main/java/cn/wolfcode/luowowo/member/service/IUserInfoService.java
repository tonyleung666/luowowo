package cn.wolfcode.luowowo.member.service;

import cn.wolfcode.luowowo.common.exception.LogicException;
import cn.wolfcode.luowowo.member.domain.UserInfo;

import java.util.List;

/**
 * 用户服务接口
 */
public interface IUserInfoService {
    /**
     * 查询单个用户
     * @param id
     * @return
     */
    UserInfo get(Long id);

    /**
     * 查询手机号是否已存在
     * @param phone
     * @return true: 已存在, false:不存在
     */
    boolean checkPhone(String phone);

    /**
     * 发送短信验证码
     * @param phone
     */
    void sendVerifyCode(String phone) throws LogicException ;

    /**
     * 用户注册
     * @param phone
     * @param nickname
     * @param password
     * @param rpassword
     * @param verifyCode
     */
    void userRegist(String phone, String nickname, String password, String rpassword, String verifyCode) throws LogicException;

    /**
     * 用户登录
     * @param username
     * @param password
     * @return
     */
    String userLogin(String username, String password) throws LogicException;

    /**
     * 查询所有用户
     * @return
     */
    List<UserInfo> list();

    /**
     * 查询当前用户的关注
     * @param userInfo
     */
    List<UserInfo> getAttentionsByCurrentUser(UserInfo userInfo);

    /**
     * 修改用户信息
     * @param user
     */
    void updateUserInfo(UserInfo user);

    /**
     * 更新绑定手机号
     * @param phone
     * @param newMobile
     */
    void updatePhone(String phone, String newMobile);

    /**
     * 修改积分
     * @param uid
     * @param gid
     */
    void updateIntegralByUserId(Long uid,Long gid);


    /**
     * 注销用户
     * @param id
     */
    void deleteUser(Long id);

    /**
     * 查询黑名单用户
     * @param id
     * @return
     */
    List<UserInfo> queryByUserId(Long id);
}
