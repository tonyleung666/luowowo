package cn.wolfcode.luowowo.member.domain;

import cn.wolfcode.luowowo.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.StringUtils;

import java.util.Date;

/**
 * 用户实体
 */
@Setter
@Getter
public class UserInfo extends BaseDomain {

    public static final int GENDER_SECRET = 0; //保密
    public static final int GENDER_MALE = 1;   //男
    public static final int GENDER_FEMALE = 2;  //女
    public static final int STATE_NORMAL = 0;  //正常
    public static final int STATE_DISABLE = 1;  //冻结

    private String nickname;  //昵称
    private String phone;  //手机
    private String email;  //邮箱
    private String password; //密码
    private int gender = GENDER_SECRET; //性别
    private int level = 0;  //用户级别
    private String city;  //所在城市
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthday; //生日
    private String headImgUrl; //头像
    private String info;  //个性签名
    private int state = STATE_NORMAL; //状态
    private String  attention; //关注
    private String  fans; //粉丝
    private int  integral; //积分

    private Long[] attentionIds;
    private Long[] fansIds;
    //从数据库拿出的id拼接字符串转换成数组
    public Long[] getAttentionIds(){
        return getSomeIds(this.attention);
    }

    public Long[] getFansIds(){
        return getSomeIds(this.fans);
    }

    public Long[] getSomeIds(String param){
        Long[] ids = null;
        if(StringUtils.hasLength(param)){
            String[] strs = param.split(",");
            ids = new Long[strs.length];
            for (int i = 0; i <strs.length ; i++) {
                ids[i] =  Long.parseLong(strs[i]);
            }
        }
        return ids;
    }
    //添加编辑时，前端传入是long数组， 数据库保存的是id拼接的字符串
    public String getRef(){
        if(attentionIds!=null){
            return StringUtils.arrayToDelimitedString(attentionIds, ",");
        }
        return null;
    }

}