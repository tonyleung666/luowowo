package cn.wolfcode.luowowo.member.service;

import cn.wolfcode.luowowo.member.domain.Prop;

import java.util.List;

public interface IPropService {
    List<Prop> selectAll();
}
