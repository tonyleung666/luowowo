package cn.wolfcode.luowowo.member.domain;


import cn.wolfcode.luowowo.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Prop extends BaseDomain{
    private String prop;
    private Long gold;
    private String propImg;
    private String role;
    private Integer total;
}