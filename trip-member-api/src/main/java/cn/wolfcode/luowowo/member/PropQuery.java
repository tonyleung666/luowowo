package cn.wolfcode.luowowo.member;

import cn.wolfcode.luowowo.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PropQuery extends BaseDomain {
    private Long queryType=0L;
}
