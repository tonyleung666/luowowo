package cn.wolfcode.luowowo.member.service;

import cn.wolfcode.luowowo.common.exception.LogicException;
import cn.wolfcode.luowowo.member.domain.Prop;
import cn.wolfcode.luowowo.member.domain.UserProp;

import java.util.List;

public interface IUserPropService {
    List<UserProp> getUserPropByUserId(Long id);

    List<Prop> getMyProps(Long id);

    void insert(Prop prop, Long id1) throws LogicException;
}
