package cn.wolfcode.luowowo.member.service.impl;

import cn.wolfcode.luowowo.cache.service.IUserInfoRedisService;
import cn.wolfcode.luowowo.common.exception.LogicException;
import cn.wolfcode.luowowo.common.util.AssertUtil;
import cn.wolfcode.luowowo.common.util.Consts;
import cn.wolfcode.luowowo.member.domain.UserInfo;
import cn.wolfcode.luowowo.member.mapper.UserInfoMapper;
import cn.wolfcode.luowowo.member.service.IUserInfoService;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Service
public class UserInfoServiceImpl implements IUserInfoService {
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Reference
    private IUserInfoRedisService userInfoRedisService;

    public UserInfo get(Long id) {
        return userInfoMapper.selectByPrimaryKey(id);
    }

    public boolean checkPhone(String phone) {
        int count = userInfoMapper.selectCountByPhone(phone);
        return count > 0;
    }

    public void sendVerifyCode(String phone) {
        userInfoRedisService.isResendVerifyCode(phone);
        String code = UUID.randomUUID().toString().replace("-", "").substring(0, 4);
        StringBuilder sb = new StringBuilder();
        sb.append("您的验证码是: ").append(code).append(",请在" + Consts.VERIFY_CODE_VAI_TIME + "分钟内使用");
        System.out.println(sb);
        userInfoRedisService.setVerifyCode(phone, code);
    }

    public void userRegist(String phone, String nickname, String password, String rpassword, String verifyCode) throws LogicException {
        //1. 判断参数是否为空
        AssertUtil.hasLength(phone, "手机号不能为空!");
        AssertUtil.hasLength(nickname, "昵称不能为空!");
        AssertUtil.hasLength(password, "密码不能为空!");
        AssertUtil.hasLength(rpassword, "确认密码不能为空!");
        AssertUtil.hasLength(verifyCode, "验证码不能为空!");

        //2. 判断两次密码是否一致
        AssertUtil.isEquels(password, rpassword, "两次密码必须一致!");

        //3. 判断手机号格式是否合法(拓展)
        AssertUtil.isMobile(phone);

        //4. 判断手机号是否唯一
        if (checkPhone(phone)) {
            throw new LogicException("该手机号已被注册!");
        }

        //5. 判断验证码是否一致, 或者是否过期
        String code = userInfoRedisService.getVerifyCode(phone);
        if (code == null || !code.equalsIgnoreCase(verifyCode)) {
            throw new LogicException("短信验证码错误或已过期!");
        }

        //6. 注册
        UserInfo userInfo = new UserInfo();
        userInfo.setPhone(phone);
        userInfo.setNickname(nickname);
        userInfo.setPassword(password);
        userInfo.setState(UserInfo.STATE_NORMAL);
        userInfo.setLevel(1);
        userInfo.setHeadImgUrl("/images/default.jpg");
        userInfoMapper.insert(userInfo);
    }

    public String userLogin(String username, String password)  throws LogicException {
        AssertUtil.hasLength(username,"账号不能为空");
        AssertUtil.hasLength(password, "密码不能为空");
        UserInfo userInfo = userInfoMapper.selectUserByUsernameAndPassword(username, password);
        if (userInfo == null){
            throw new LogicException("账号或密码错误!");
        }
        String token = UUID.randomUUID().toString().replace("-","");
        userInfoRedisService.setToken(token, userInfo);
        return token;
    }

    public List<UserInfo> list() {
        return userInfoMapper.selectAll();
    }

    //查询当前用户的关注
    @Override
    public List<UserInfo> getAttentionsByCurrentUser(UserInfo userInfo) {
        Long[] attentionIds = userInfo.getAttentionIds();
        List<UserInfo> attentions;
        if(attentionIds!=null){
            attentions = userInfoMapper.selectUserByUserIds(attentionIds);
            return attentions;
        }
        return Collections.emptyList();
    }

    @Override
    public void updateUserInfo(UserInfo user) {
        userInfoMapper.updateUserInfo(user);
    }

    @Override
    public void updatePhone(String phone, String newMobile) {
        List<String> allphone=userInfoMapper.getAllPhone();
        if (allphone.contains(newMobile)){
            throw new LogicException("手机号已经被绑定");
        }
        userInfoMapper.updateUserPhone(phone,newMobile);
    }

    /**
     * 修改积分
     * @param uid
     * @param gid
     */
    public void updateIntegralByUserId(Long uid, Long gid) {
        userInfoMapper.updateIntegralByUserId(uid,gid);
    }


    @Override
    public void deleteUser(Long id) {
        userInfoMapper.deleteByPrimaryKey(id);
    }

    @Override
    public List<UserInfo> queryByUserId(Long id) {
        return userInfoMapper.queryByUserId(id);
    }
}
