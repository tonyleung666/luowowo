package cn.wolfcode.luowowo.member.service.impl;

import cn.wolfcode.luowowo.member.domain.Prop;
import cn.wolfcode.luowowo.member.mapper.PropMapper;
import cn.wolfcode.luowowo.member.service.IPropService;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class PropService implements IPropService {

    @Autowired
    private PropMapper propMapper;

    public List<Prop> selectAll() {
        return propMapper.selectAll();
    }
}
