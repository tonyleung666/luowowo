package cn.wolfcode.luowowo.member.mapper;

import cn.wolfcode.luowowo.member.domain.UserProp;
import java.util.List;

public interface UserPropMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserProp record);

    UserProp selectByPrimaryKey(Integer id);

    List<UserProp> selectAll();

    int updateByPrimaryKey(UserProp record);

    List<UserProp> selectUserPropByUserId(Long user_id);

    List<Long> selectPropIdByUserId(Long id);
}