package cn.wolfcode.luowowo.member.mapper;

import cn.wolfcode.luowowo.member.domain.UserInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserInfoMapper {
    int deleteByPrimaryKey(Long id);

    int insert(UserInfo record);

    UserInfo selectByPrimaryKey(Long id);

    List<UserInfo> selectAll();

    int updateByPrimaryKey(UserInfo record);

    int selectCountByPhone(String phone);

    UserInfo selectUserByUsernameAndPassword(@Param("username") String username, @Param("password") String password);

    List<UserInfo> selectUserByUserIds(Long[] attentionIds);

    void updateUserInfo(UserInfo user);

    List<String> getAllPhone();

    void updateUserPhone(@Param("phone") String phone, @Param("newMobile") String newMobile);

    void updateIntegralByUserId(@Param("uid") Long uid, @Param("gid") Long gid);

    List<UserInfo> queryByUserId(Long id);
}