package cn.wolfcode.luowowo.member.mapper;

import cn.wolfcode.luowowo.member.domain.Prop;
import java.util.List;

public interface PropMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Prop record);

    Prop selectByPrimaryKey(Integer id);

    List<Prop> selectAll();

    int updateByPrimaryKey(Prop record);

    Prop selectByPropId(Long propId);

    void updateTotalById(Long id);
}