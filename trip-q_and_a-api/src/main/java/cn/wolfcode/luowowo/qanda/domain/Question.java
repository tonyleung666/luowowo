package cn.wolfcode.luowowo.qanda.domain;


import cn.wolfcode.luowowo.article.domain.Destination;
import cn.wolfcode.luowowo.comment.domain.Answer;
import cn.wolfcode.luowowo.common.domain.BaseDomain;
import cn.wolfcode.luowowo.member.domain.UserInfo;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class Question extends BaseDomain{
    private UserInfo user;
    private String title;
    private QuestionContent content;
    private Date createTime;
    private Destination dest;
    private QuestionTag tag;
    private int viewnum;
    private int answernum;
    private int statu;
    private Answer answer;
}
