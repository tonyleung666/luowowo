package cn.wolfcode.luowowo.qanda.domain;

import cn.wolfcode.luowowo.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QuestionContent extends BaseDomain{
    private String content;
}
