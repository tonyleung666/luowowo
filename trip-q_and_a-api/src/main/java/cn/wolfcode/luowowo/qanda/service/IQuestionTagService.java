package cn.wolfcode.luowowo.qanda.service;

import cn.wolfcode.luowowo.qanda.domain.QuestionTag;

import java.util.List;

public interface IQuestionTagService {
    List<QuestionTag> query();

    QuestionTag findTagById(Long tagId);
}
