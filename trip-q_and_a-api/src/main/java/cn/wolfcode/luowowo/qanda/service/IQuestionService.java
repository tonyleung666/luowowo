package cn.wolfcode.luowowo.qanda.service;

import cn.wolfcode.luowowo.member.domain.UserInfo;
import cn.wolfcode.luowowo.qanda.domain.Question;

import java.util.List;

public interface IQuestionService {
    void insert(Question question, UserInfo userInfo);

    List<Question> query();

    Question selectQuestionById(Long id);
}
