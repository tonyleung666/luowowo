package cn.wolfcode.luowowo.qanda.domain;

import cn.wolfcode.luowowo.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;


@Getter
@Setter
public class QuestionTag extends BaseDomain implements Serializable {
    private String tag;
}
