package cn.wolfcode.luowowo.search.repository;

import cn.wolfcode.luowowo.search.domain.StrategyTemplate;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StrategySearchRepository extends ElasticsearchRepository<StrategyTemplate, Long> {
    /**
     * 通过主题id查询列表
     * @param themeId
     * @return
     */
    List<StrategyTemplate> findByThemeId(Long themeId);

    /**
     * 根据目的地名称查询
     * @param destName
     * @return
     */
    List<StrategyTemplate> findByDestName(String destName);
}
