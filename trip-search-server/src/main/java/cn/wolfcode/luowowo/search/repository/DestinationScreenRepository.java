package cn.wolfcode.luowowo.search.repository;


import cn.wolfcode.luowowo.search.domain.DestScreenVO;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DestinationScreenRepository extends ElasticsearchRepository<DestScreenVO, Long> {
    /**
     * 根据目的地名称查询
     * @param destId
     * @return
     */
    DestScreenVO findByDestId(Long destId);

    DestScreenVO findByDestName(String destName);
}
