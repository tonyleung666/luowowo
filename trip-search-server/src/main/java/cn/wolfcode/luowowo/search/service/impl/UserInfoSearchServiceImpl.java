package cn.wolfcode.luowowo.search.service.impl;

import cn.wolfcode.luowowo.search.domain.UserInfoTemplate;
import cn.wolfcode.luowowo.search.repository.UserInfoSearchRepository;
import cn.wolfcode.luowowo.search.service.IUserInfoSearchService;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class UserInfoSearchServiceImpl implements IUserInfoSearchService {
    
    @Autowired
    private UserInfoSearchRepository repository;
    
    public void save(UserInfoTemplate template) {
        repository.save(template);
    }

    public List<UserInfoTemplate> findByDestName(String destName) {
        return repository.findByDestName(destName);
    }
}
