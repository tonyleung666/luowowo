package cn.wolfcode.luowowo.search.service.impl;

import cn.wolfcode.luowowo.search.domain.TravelTemplate;
import cn.wolfcode.luowowo.search.repository.TravelSearchRepository;
import cn.wolfcode.luowowo.search.service.ITravelSearchService;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.awt.image.ReplicateScaleFilter;
import java.util.List;

@Service
public class TravelSearchServiceImpl implements ITravelSearchService {
    
    @Autowired
    private TravelSearchRepository repository;
    
    public void save(TravelTemplate template) {
        repository.save(template);
    }

    public List<TravelTemplate> findByDestName(String destName) {
        return repository.findByDestName(destName);
    }

}
