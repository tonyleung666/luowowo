package cn.wolfcode.luowowo.search.service.impl;


import cn.wolfcode.luowowo.article.domain.Poi;
import cn.wolfcode.luowowo.article.domain.StrategyDetail;
import cn.wolfcode.luowowo.article.service.IPoiService;
import cn.wolfcode.luowowo.article.service.IStrategyDetailService;
import cn.wolfcode.luowowo.search.domain.DestScreenVO;
import cn.wolfcode.luowowo.search.repository.DestinationScreenRepository;
import cn.wolfcode.luowowo.search.service.IDestinationScreenService;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;

import java.util.ArrayList;
import java.util.List;

@Service
public class DestinationScreenServiceImpl implements IDestinationScreenService {
    @Autowired
    private DestinationScreenRepository repository;
    @Reference
    private IStrategyDetailService strategyDetailService;
    @Reference
    private IPoiService poiService;

    public List<DestScreenVO> queryDestScreenVO(Integer monthNum) {
        List<DestScreenVO> list1 = new ArrayList<>();
        int count = 0;
        //NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder();
        //设置排序,根据游记个数
        //builder.withPageable(PageRequest.of(1,8, Sort.Direction.DESC,"count"));
        Iterable<DestScreenVO> all = repository.findAll(Sort.by(Sort.Direction.DESC,"count"));
        //builder.withSort(SortBuilders.fieldSort("count").order(SortOrder.DESC));
        //Page<DestScreenVO> page = repository.search(builder.build());
        //如果需要分页直接返回分页的page过去,不用下面
        //List<DestScreenVO> list = page.getContent();
        for (DestScreenVO destScreenVO : all) {
            if (destScreenVO.getMonthNums().size()>0){
                if (destScreenVO.getMonthNums().contains(monthNum)){
                    list1.add(destScreenVO);
                    count ++;
                    if (count >= 8){
                        break;
                    }
                }
            }
        }
        return list1;
    }

    public void save(DestScreenVO vo) {
        repository.save(vo);
    }

    //public List<DestScreenVO> queryDestScreenVOByQo(DestinationScreenQuery qo) {
    public List<DestScreenVO> queryDestScreenVOByQo(Long timeId,Long themeId,Long dayId,Integer monthNum) {
        //根据主题id查询
        List<DestScreenVO> list = new ArrayList<>();
        List<DestScreenVO> list1 = new ArrayList<DestScreenVO>();
        if (monthNum != -1){
            List<DestScreenVO> destScreenVOS = this.queryDestScreenVO(monthNum);
            for (DestScreenVO destScreenVO : destScreenVOS) {
                if (destScreenVO.getSmallThemeId() != null && destScreenVO.getSmallThemeId().size()>0) {
                    StrategyDetail strategyDetail = strategyDetailService.queryViewNumTop1(destScreenVO.getDestId());
                    if (strategyDetail != null){
                        destScreenVO.setTitle(strategyDetail.getSubTitle());
                    }
                    List<Poi> poiTop3List = poiService.queryTop3ByParentId(destScreenVO.getDestId());
                    if (poiTop3List!= null && poiTop3List.size()>0){
                        destScreenVO.setPoiList(poiTop3List);
                    }
                }
                list.add(destScreenVO);
            }
        }else {

        //List<DestScreenVO> list1 = new ArrayList<DestScreenVO>();
        NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder();
        //设置排序,根据游记个数
        //builder.withPageable(PageRequest.of(1,8, Sort.Direction.DESC,"count"));
        //builder.withSort(SortBuilders.fieldSort("count").order(SortOrder.DESC));
        //Page<DestScreenVO> page = repository.search(builder.build());
        Iterable<DestScreenVO> all = repository.findAll(Sort.by(Sort.Direction.DESC,"count"));
        //如果需要分页直接返回分页的page过去,不用下面
        //List<DestScreenVO> list = new ArrayList<>();
        for (DestScreenVO destScreenVO : all) {
            if (destScreenVO.getSmallThemeId() != null && destScreenVO.getSmallThemeId().size()>0) {
                StrategyDetail strategyDetail = strategyDetailService.queryViewNumTop1(destScreenVO.getDestId());
                if (strategyDetail != null){
                    destScreenVO.setTitle(strategyDetail.getSubTitle());
                }
                List<Poi> poiTop3List = poiService.queryTop3ByParentId(destScreenVO.getDestId());
                if (poiTop3List!= null && poiTop3List.size()>0){
                    destScreenVO.setPoiList(poiTop3List);
                }
            }
            list.add(destScreenVO);
        }
        }
        int count = 0;

        if(timeId != -1){
            count ++;
        }
        if (themeId != -1){
            count ++;
        }
        if (dayId != -1){
            count ++;
        }
        if (count == 0){
            if (list.size()>10){
                List<DestScreenVO> sublist = new ArrayList<>();
                int sublistCount = 0;
                for (DestScreenVO destScreenVO : list) {
                    sublist.add(destScreenVO);
                    sublistCount++;
                    if (sublistCount == 10){
                        break;
                    }
                }
                return sublist;
            }
            return list;

        }
        /*if(qo.getTimeId() == -1){
            count ++;
        }
        if (qo.getThemeId() == -1){
            count ++;
        }
        if (qo.getDayId() == -1){
            count ++;
        }*/
        int countId = 0;
        for (DestScreenVO destScreenVO : list) {
            if (destScreenVO.getSmallThemeId().contains(timeId)){
                countId ++;
            }
            if (destScreenVO.getSmallThemeId().contains(themeId)){
                countId ++;
            }
            if (destScreenVO.getSmallThemeId().contains(dayId)){
                countId ++;
            }
            if (countId == count){
                list1.add(destScreenVO);
            }
            countId = 0;
        }
        if (list1.size()>10){
            List<DestScreenVO> sublist = new ArrayList<>();
            int sublistCount = 0;
            for (DestScreenVO destScreenVO : list) {
                sublist.add(destScreenVO);
                sublistCount++;
                if (sublistCount == 10){
                    break;
                }
            }
            return sublist;
        }
        return list1;
    }

    public List<DestScreenVO> queryDestScreenVOByDestName(Long timeId, Long themeId, Long dayId, Integer monthNum, String destName) {
        //根据主题id查询
        List<DestScreenVO> list = new ArrayList<>();
        NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder();

        DestScreenVO destScreenVO = repository.findByDestName(destName);
        if (destScreenVO.getSmallThemeId() != null && destScreenVO.getSmallThemeId().size() > 0) {
            StrategyDetail strategyDetail = strategyDetailService.queryViewNumTop1(destScreenVO.getDestId());
            if (strategyDetail != null) {
                destScreenVO.setTitle(strategyDetail.getSubTitle());
            }
            List<Poi> poiTop3List = poiService.queryTop3ByParentId(destScreenVO.getDestId());
            if (poiTop3List != null && poiTop3List.size() > 0) {
                destScreenVO.setPoiList(poiTop3List);
            }
        }
        list.add(destScreenVO);
        return list;
    }

}
