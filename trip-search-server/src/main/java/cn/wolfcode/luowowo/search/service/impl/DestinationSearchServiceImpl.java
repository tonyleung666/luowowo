package cn.wolfcode.luowowo.search.service.impl;

import cn.wolfcode.luowowo.search.domain.DestinationTemplate;
import cn.wolfcode.luowowo.search.repository.DestinationSearchRepository;
import cn.wolfcode.luowowo.search.service.IDestinationSearchService;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class DestinationSearchServiceImpl implements IDestinationSearchService {

    @Autowired
    private DestinationSearchRepository repository;

    public void save(DestinationTemplate template) {
        repository.save(template);
    }

    public DestinationTemplate findByName(String name) {
        return repository.findByName(name);
    }
}
