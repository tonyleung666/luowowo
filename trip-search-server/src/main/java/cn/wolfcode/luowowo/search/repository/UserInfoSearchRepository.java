package cn.wolfcode.luowowo.search.repository;

import cn.wolfcode.luowowo.search.domain.UserInfoTemplate;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserInfoSearchRepository extends ElasticsearchRepository<UserInfoTemplate, Long> {
    /**
     * 根据目的地名称查询
     * @param destName
     * @return
     */
    List<UserInfoTemplate> findByDestName(String destName);
}
