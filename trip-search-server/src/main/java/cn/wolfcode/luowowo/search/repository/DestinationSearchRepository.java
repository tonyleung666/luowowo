package cn.wolfcode.luowowo.search.repository;

import cn.wolfcode.luowowo.search.domain.DestinationTemplate;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DestinationSearchRepository extends ElasticsearchRepository<DestinationTemplate, Long> {
    /**
     * 根据目的地名称查询
     * @param name
     * @return
     */
    DestinationTemplate findByName(String name);
}
