package cn.wolfcode.luowowo.website.web.controller;

import cn.wolfcode.luowowo.article.domain.TravelCommend;
import cn.wolfcode.luowowo.article.query.TravelQuery;
import cn.wolfcode.luowowo.article.service.ITravelCommendService;
import cn.wolfcode.luowowo.article.service.ITravelService;
import cn.wolfcode.luowowo.cache.service.IUserInfoRedisService;
import cn.wolfcode.luowowo.search.domain.DestinationTemplate;
import cn.wolfcode.luowowo.search.domain.StrategyTemplate;
import cn.wolfcode.luowowo.search.domain.TravelTemplate;
import cn.wolfcode.luowowo.search.domain.UserInfoTemplate;
import cn.wolfcode.luowowo.search.query.SearchQueryObject;
import cn.wolfcode.luowowo.search.service.*;
import cn.wolfcode.luowowo.search.vo.SearchResult;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class IndexController {
    @Reference
    private IUserInfoRedisService userInfoRedisService;

    @Reference
    private ITravelCommendService travelCommendService;

    @Reference
    private ITravelService travelService;

    @Reference
    private IDestinationSearchService destinationSearchService;

    @Reference
    private IStrategySearchService strategySearchService;

    @Reference
    private ITravelSearchService travelSearchService;

    @Reference
    private IUserInfoSearchService userInfoSearchService;

    @Reference
    private ISearchService searchService;

    @RequestMapping("/q")
    public String search(Model model, @ModelAttribute("qo") SearchQueryObject qo) {
        switch (qo.getType()) {
            case SearchQueryObject.SEARCH_TYPE_DEST:
                return searchDest(model, qo);
            case SearchQueryObject.SEARCH_TYPE_STRATEGY:
                return searchStrategy(model, qo);
            case SearchQueryObject.SEARCH_TYPE_TRAVEL:
                return searchTravel(model, qo);
            case SearchQueryObject.SEARCH_TYPE_USER:
                return searchUser(model, qo);
            default:
                return searchAll(model, qo);
        }
    }


    // 查目的地
    private String searchDest(Model model, SearchQueryObject qo) {
        // 判断当前关键字是不是一个目的地
        DestinationTemplate dest = destinationSearchService.findByName(qo.getKeyword());

        // 如果是: 查询关联的 攻略 /游记 /用户
        if (dest != null) {

            SearchResult result = new SearchResult();
            List<StrategyTemplate> ss = strategySearchService.findByDestName(dest.getName());
            result.setTotal(result.getTotal() + ss.size());
            if (ss.size() > 5) {
                ss = ss.subList(0, 5);
            }

            List<TravelTemplate> ts = travelSearchService.findByDestName(dest.getName());
            result.setTotal(result.getTotal() + ts.size());
            if (ts.size() > 5) {
                ts = ts.subList(0, 5);
            }


            List<UserInfoTemplate> us = userInfoSearchService.findByDestName(dest.getName());
            result.setTotal(result.getTotal() + us.size());
            if (us.size() > 5) {
                us = us.subList(0, 5);
            }

            result.setStrategys(ss);
            result.setTravels(ts);
            result.setUsers(us);
            model.addAttribute("data", result);
        }

        model.addAttribute("dest", dest);
        // 如果不是: 提示查询不到
        return "index/searchDest";
    }

    // 查全部
    private String searchAll(Model model, SearchQueryObject qo) {
        SearchResult result = new SearchResult();

        Page<DestinationTemplate> ds = searchService.searchWithHighlight(DestinationTemplate.INDEX_NAME, DestinationTemplate.TYPE_NAME,
                DestinationTemplate.class, qo, "name", "info");

        Page<UserInfoTemplate> us = searchService.searchWithHighlight(UserInfoTemplate.INDEX_NAME, UserInfoTemplate.TYPE_NAME,
                UserInfoTemplate.class, qo, "nickname", "destName");

        Page<TravelTemplate> ts = searchService.searchWithHighlight(TravelTemplate.INDEX_NAME, TravelTemplate.TYPE_NAME,
                TravelTemplate.class, qo, "title", "summary");

        Page<StrategyTemplate> ss = searchService.searchWithHighlight(StrategyTemplate.INDEX_NAME, StrategyTemplate.TYPE_NAME,
                StrategyTemplate.class, qo, "title", "subTitle", "summary");

        result.setDests(ds.getContent());
        result.setUsers(us.getContent());
        result.setTravels(ts.getContent());
        result.setStrategys(ss.getContent());
        result.setTotal(ds.getTotalElements() + us.getTotalElements() + ts.getTotalElements() + ss.getTotalElements());

        model.addAttribute("data", result);
        return "index/searchAll";
    }

    // 查用户
    private String searchUser(Model model, SearchQueryObject qo) {

        Page<UserInfoTemplate> us = searchService.searchWithHighlight(UserInfoTemplate.INDEX_NAME, UserInfoTemplate.TYPE_NAME,
                UserInfoTemplate.class, qo, "nickname", "destName");

        model.addAttribute("page", us);
        return "index/searchUser";
    }

    // 查游记
    private String searchTravel(Model model, SearchQueryObject qo) {
        Page<TravelTemplate> ts = searchService.searchWithHighlight(TravelTemplate.INDEX_NAME, TravelTemplate.TYPE_NAME,
                TravelTemplate.class, qo, "title", "summary");

        model.addAttribute("page", ts);
        return "index/searchTravel";
    }

    // 查攻略
    private String searchStrategy(Model model, SearchQueryObject qo) {
        Page<StrategyTemplate> ss = searchService.searchWithHighlight(StrategyTemplate.INDEX_NAME, StrategyTemplate.TYPE_NAME,
                StrategyTemplate.class, qo, "title", "subTitle", "summary");

        model.addAttribute("page", ss);
        return "index/searchStrategy";
    }

    @RequestMapping("")
    public String index(Model model) {
        List<TravelCommend> top5 = travelCommendService.getTop5();
        model.addAttribute("tcs", top5);
        return "index/index";
    }

    @RequestMapping("/index/travelPage")
    public String travelPage(Model model, @ModelAttribute("qo") TravelQuery qo) {
        model.addAttribute("pageInfo", travelService.query(qo));
        return "index/travelPageTpl";
    }

}
