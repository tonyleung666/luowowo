package cn.wolfcode.luowowo.website.web.controller;

import cn.wolfcode.luowowo.article.domain.FlightAirport;
import cn.wolfcode.luowowo.article.domain.FlightInfo;
import cn.wolfcode.luowowo.article.domain.FlightOrder;
import cn.wolfcode.luowowo.article.query.FlightInfoQuery;
import cn.wolfcode.luowowo.article.service.IFlightService;
import cn.wolfcode.luowowo.common.util.AjaxResult;
import cn.wolfcode.luowowo.common.util.DateUtil;
import cn.wolfcode.luowowo.member.domain.UserInfo;
import cn.wolfcode.luowowo.website.annotation.UserParam;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/flight")
public class FlightController {

    @Reference
    private IFlightService flightService;

    @RequestMapping("/order")
    public String order(Model model, Long id){
        FlightInfo info = flightService.getInfoById(id);
        model.addAttribute("info", info);
        long between = DateUtil.getInterval(info.getArrTime(), info.getDepTime());
        String interval = DateUtil.secToTime(between);
        model.addAttribute("interval", interval);
        return "flight/order";
    }


    @RequestMapping("/index")
    public String index(Model model){
        List<FlightAirport> hotFlights = flightService.queryHots();
        model.addAttribute("hotFlights", hotFlights);
        Map initialA = flightService.queryByInitial("A", "E");
        model.addAttribute("initialA", initialA);
        Map initialF = flightService.queryByInitial("F", "J");
        model.addAttribute("initialF", initialF);
        Map initialK = flightService.queryByInitial("K", "P");
        model.addAttribute("initialK", initialK);
        Map initialQ = flightService.queryByInitial("Q", "W");
        model.addAttribute("initialQ", initialQ);
        Map initialX = flightService.queryByInitial("X", "Z");
        model.addAttribute("initialX", initialX);

        return "flight/index";
    }

    @RequestMapping("/search")
    @ResponseBody
    public Object search(@ModelAttribute("qo") FlightInfoQuery qo){
        List<FlightInfo> list = flightService.queryInfo(qo);
        return AjaxResult.success().addData(list);
    }

    @RequestMapping("/deal")
    @ResponseBody
    public Object deal(@UserParam UserInfo userInfo, FlightOrder order){
        if (userInfo == null){
            return new AjaxResult(false, "请先登录!");
        }
        order.setUser(userInfo);
        flightService.saveOrder(order);
        return AjaxResult.success();
    }

}
