package cn.wolfcode.luowowo.website.web.advice;

import cn.wolfcode.luowowo.common.exception.LogicException;
import cn.wolfcode.luowowo.common.util.AjaxResult;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ControllerAdvice
public class CommonExceptionHandler {
    @ExceptionHandler(LogicException.class)
    @ResponseBody
    public AjaxResult displayExp(Exception e, HttpServletResponse resp) throws IOException {
        e.printStackTrace();
        return new AjaxResult(false, e.getMessage());
    }

    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    public AjaxResult runTimeExp(Exception e, HttpServletResponse resp) throws IOException {
        e.printStackTrace();
        return new AjaxResult(false, "系统繁忙, 请联系客服!");
    }
}
