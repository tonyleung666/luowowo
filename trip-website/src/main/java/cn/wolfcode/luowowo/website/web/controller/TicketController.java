package cn.wolfcode.luowowo.website.web.controller;

import cn.wolfcode.luowowo.article.domain.ScenicSpot;
import cn.wolfcode.luowowo.article.domain.ScenicSpotTag;
import cn.wolfcode.luowowo.article.domain.TicketIndent;
import cn.wolfcode.luowowo.article.query.ScenicSpotQuery;
import cn.wolfcode.luowowo.article.service.IScenicSpotService;
import cn.wolfcode.luowowo.article.service.IScenicSpotTagService;
import cn.wolfcode.luowowo.article.service.ITicketIndentService;
import cn.wolfcode.luowowo.common.exception.LogicException;
import cn.wolfcode.luowowo.common.util.AjaxResult;
import cn.wolfcode.luowowo.member.domain.UserInfo;
import cn.wolfcode.luowowo.website.annotation.UserParam;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("ticket")
public class TicketController {

    @Reference
    private IScenicSpotService scenicSpotService;

    @Reference
    private IScenicSpotTagService scenicSpotTagService;

    @Reference
    private ITicketIndentService ticketIndentService;

    @RequestMapping("")
    public String list(Model model){
        List<ScenicSpot> top=scenicSpotService.getTicketTop4();
        List<ScenicSpot> spots=scenicSpotService.list();
        ScenicSpotQuery qo=new ScenicSpotQuery();
        List<ScenicSpot> list=scenicSpotService.query(qo);
        List<ScenicSpotTag> tags=scenicSpotTagService.list();
        model.addAttribute("list",list);
        model.addAttribute("spots",spots);
        model.addAttribute("tags",tags);
        model.addAttribute("top",top);
        return "ticket/index";
    }

    @RequestMapping("query1")
    public String query(Model model, @ModelAttribute("qo") ScenicSpotQuery qo){
        List<ScenicSpot> list = scenicSpotService.query(qo);
        model.addAttribute("ts",list);
        return "ticket/indexTagTpl";
    }

    @RequestMapping("query2")
    public String query2(Model model, @ModelAttribute("qo") ScenicSpotQuery qo){
        List<ScenicSpot> list = scenicSpotService.query(qo);
        List<ScenicSpotTag> tags=scenicSpotTagService.list();
        model.addAttribute("ts",list);
        model.addAttribute("tags",tags);
        return "ticket/indexTpl";
    }


    @RequestMapping("detail")
    public String detail(Model model,Long id){
        ScenicSpot ticket=scenicSpotService.querySpotById(id);
        model.addAttribute("ticket",ticket);
        return "ticket/detail";
    }

    @RequestMapping("addOrder")
    public String addOrder(Model model,Long id){
        ScenicSpot ticket=scenicSpotService.querySpotById(id);
        model.addAttribute("ticketDetail",ticket);
        return "ticket/addOrder";
    }

    @RequestMapping("submit")
    @ResponseBody
    public Object submit(TicketIndent ticketIndent,Model model, @UserParam UserInfo userInfo){
        if(userInfo==null){
            throw new LogicException("你还没登录");
         }
        System.out.println(ticketIndent);
        ticketIndent.setUser(userInfo);
        ticketIndent = ticketIndentService.save(ticketIndent);
        System.out.println(ticketIndent.getId());
        model.addAttribute("ticket",ticketIndent);
        return AjaxResult.success().addData(ticketIndent.getId());
    }

    @RequestMapping("pay")
    @ResponseBody
    public Object submit(Long id,Long status){
        TicketIndent ticketIndent = new TicketIndent();
        ticketIndent.setId(id);
        ticketIndent.setPayTime(new Date());
        ticketIndent.setStatus(status);
        ticketIndentService.updateByIndent(ticketIndent);
        return AjaxResult.success();
    }
}
