package cn.wolfcode.luowowo.website.web.controller;

import cn.wolfcode.luowowo.article.domain.Poi;
import cn.wolfcode.luowowo.article.service.IDestinationService;
import cn.wolfcode.luowowo.article.service.IPoiService;
import cn.wolfcode.luowowo.cache.service.IPoiRedisService;
import cn.wolfcode.luowowo.comment.domain.PoiComment;
import cn.wolfcode.luowowo.comment.domain.PoiCommentReply;
import cn.wolfcode.luowowo.comment.query.PoiCommentQuery;
import cn.wolfcode.luowowo.comment.service.IPoiCommentReplyService;
import cn.wolfcode.luowowo.comment.service.IPoiCommentService;
import cn.wolfcode.luowowo.common.util.AjaxResult;
import cn.wolfcode.luowowo.member.domain.UserInfo;
import cn.wolfcode.luowowo.website.annotation.UserParam;
import cn.wolfcode.luowowo.website.util.ModelUtils;
import cn.wolfcode.luowowo.website.util.UploadUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/poi")
public class PoiController {
    @Reference
    private IPoiService poiService;

    @Reference
    private IPoiRedisService poiRedisService;

    @Reference
    private IPoiCommentService commentService;

    @Reference
    private IPoiCommentReplyService replyService;

    @Reference
    private IDestinationService destinationService;

    @RequestMapping("/list")
    public String list(Model model, Long destId){
        Poi poi = poiService.getByDestId(destId);
        if (poi != null) {
            model.addAttribute("poi", poi);
            List<Poi> list = poiService.queryTop5ByParent(poi.getId());
            model.addAttribute("tops", list);
        }
        ModelUtils.addToastsAndDest(destinationService, model, destId);
        return "poi/list";
    }

    @RequestMapping("/detail")
    public String detail(Model model, Long id, @UserParam UserInfo userInfo){
        PoiCommentQuery qo = new PoiCommentQuery();
        qo.setPoiId(id);
        Poi poi = poiService.get(id);
        model.addAttribute("poi", poi);
        Page cmtPage = commentService.query(qo);
        model.addAttribute("cmtPage", cmtPage);
        if (userInfo!=null) {
            List<Long> pids = poiRedisService.getFavorPoisByUser(userInfo.getId());
            model.addAttribute("pids", pids);
            System.out.println(pids);
        }

        return "poi/detail";
    }

    @Value("${file.path}")
    private String filePath;

    @RequestMapping("/imgUpload")
    @ResponseBody
    public Object uploadImg(MultipartFile pic) {
        String fileName = UploadUtil.upload(pic, filePath + "/poi");
        System.out.println(fileName);
        Map<String, Object> data = new HashMap<>();
        Map<String, Object> map = new HashMap<>();
        map.put("msg", "上传成功!");
        map.put("sta", true);
        map.put("previewSrc", fileName);
        data.put("result", map);
        return map;
    }

    @RequestMapping("/addCommnet")
    @ResponseBody
    public Object commentAdd(PoiComment comment, @UserParam UserInfo userInfo) {
        comment.setUsername(userInfo.getNickname());
        comment.setHeadUrl(userInfo.getHeadImgUrl());
        comment.setUserId(userInfo.getId());
        comment.setLevel(userInfo.getLevel());
        commentService.save(comment);

        return AjaxResult.success();
    }

    @RequestMapping("/addReply")
    public String commentAdd(Model model, PoiCommentReply reply, @UserParam UserInfo userInfo) {
        reply.setUserId(userInfo.getId());
        reply.setUsername(userInfo.getNickname());
        reply.setHeadUrl(userInfo.getHeadImgUrl());

        reply = replyService.addReply(reply);
        model.addAttribute("reply", reply);
        return "poi/replyTpl";
    }

    @RequestMapping("/thumbUp")
    @ResponseBody
    public Object thumbUp(@UserParam UserInfo userInfo, String cmtId) {
        if (userInfo == null) {
            // 未登录
            AjaxResult result = new AjaxResult(false, "请先登录!");
            result.setCode(AjaxResult.NO_LOGIN);
            return result;
        }
        // 顶成功:true;  顶过了:false
        boolean ret = commentService.commentThumbUp(cmtId, userInfo.getId());

        return new AjaxResult(ret, "").addData(commentService.get(cmtId));
    }

    // 收藏景点
    @RequestMapping("/favor")
    @ResponseBody
    public Object favor(@UserParam UserInfo userInfo, Long pid) {
        if (userInfo == null) {
            // 未登录
            AjaxResult result = new AjaxResult(false, "请先登录!");
            result.setCode(AjaxResult.NO_LOGIN);
            return result;
        }
        // 收藏:true;  取消收藏:false
        boolean ret = poiRedisService.favor(userInfo.getId(), pid);
        return new AjaxResult(ret, "");
    }
}
