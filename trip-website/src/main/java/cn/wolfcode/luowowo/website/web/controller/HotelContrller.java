package cn.wolfcode.luowowo.website.web.controller;

import cn.wolfcode.luowowo.article.domain.Destination;
import cn.wolfcode.luowowo.article.domain.Hotel;
import cn.wolfcode.luowowo.article.domain.StrategyDetail;
import cn.wolfcode.luowowo.article.query.HotelQuery;
import cn.wolfcode.luowowo.article.service.IDestinationService;
import cn.wolfcode.luowowo.article.service.IHotelService;
import cn.wolfcode.luowowo.article.service.IStrategyDetailService;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class HotelContrller {
    @Reference
    private IDestinationService destinationService;
    @Reference
    private IHotelService hotelService;
    @Reference
    private IStrategyDetailService strategyDetailService;

    @RequestMapping("/checkhotel")
    public String hotel(Model model){
        List<Destination> destList = destinationService.selectCityOfChina(1L);
        model.addAttribute("dest",destList);
        return "hotel/hotel";
    }
    @RequestMapping("/hotel")
    public String hoteldest(Model model, @ModelAttribute("qo")HotelQuery qo){
        model.addAttribute("qo",qo);
        List<Hotel> hotelList = hotelService.queryHotelByDestId(qo.getDestId());
        model.addAttribute("hotelList",hotelList);
        List<Destination> destList = destinationService.queryDestByParentId(qo.getDestId());
        model.addAttribute("destList",destList);
        StrategyDetail strategyDetail = strategyDetailService.queryViewNumTop1(qo.getDestId());
        model.addAttribute("strategyDetail",strategyDetail);
        //原本下面的逻辑在业务层写,这里由于方便先这样写
        List<Destination> cityList = destinationService.selectCityOfChina(1L);
        List<Destination> cityListTop4 = new ArrayList<>();
        int count = 0;
        Long destId = qo.getDestId();
        for (Destination destination : cityList) {
            //System.out.println(qo.getDays());
            //注意是Long类型不能用!=  用.equals
            if (!destination.getId().equals(destId)){
                cityListTop4.add(destination);
                count++;
            }
            if (count == 4){
                break;
            }
        }

        model.addAttribute("cityListTop4",cityListTop4);

        //前端页面的目的地下拉框回显
        List<Destination> destList1 = destinationService.selectCityOfChina(1L);
        model.addAttribute("dest",destList1);


        return "hotel/dingjiudian";
    }
}
