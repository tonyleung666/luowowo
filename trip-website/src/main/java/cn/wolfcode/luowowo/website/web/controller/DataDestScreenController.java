package cn.wolfcode.luowowo.website.web.controller;

import cn.wolfcode.luowowo.article.domain.*;
import cn.wolfcode.luowowo.article.service.*;
import cn.wolfcode.luowowo.search.domain.DestScreenVO;
import cn.wolfcode.luowowo.search.query.DestinationScreenQuery;
import cn.wolfcode.luowowo.search.service.IDestinationScreenService;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
public class DataDestScreenController {
    @Reference
    private IStrategyDetailService strategyDetailService;

    @Reference
    private IDestinationService destinationService;

    @Reference
    private ITravelService travelService;
    @Reference
    private IDestinationScreenService destinationScreenService;
    @Reference
    private ISmallThemeService smallThemeService;
    @Reference
    private IBigThemeService bigThemeService;

    @RequestMapping("/addDestScreen")
    @ResponseBody
    public Object addDestScree(Model model, @ModelAttribute("qo")DestinationScreenQuery qo){

        //目的地主题搜索的DestscreenVo----------------------------------------------------------------------------
        List<Destination> all = destinationService.list();
        List<StrategyDetail> strategyAll = strategyDetailService.list();

        for (Destination dest : all) {
            DestScreenVO vo = new DestScreenVO();
            Long count = destinationService.queryCountOfTravelByDest(dest.getId());
            List<Travel> travelList = travelService.queryByDestId(dest.getId());
            //判断游记里面的travelTime里面是几月份的,设置进vo的月份集合里面
            List<Integer> monthNumList = new ArrayList<>();
            List<Integer> travelDays = new ArrayList<>();
            List<Long> smallThemeIds = new ArrayList<>();

            //根据目的地小主题表集合
            List<SmallTheme> smallList = smallThemeService.querySmallThemeByDestId(dest.getId());
            //根据目的地查大主题表集合
            List<BigTheme> bigList = bigThemeService.queryBigThemeByDestId(dest.getId());
            if (travelList != null && travelList.size()>0){
                for (Travel travel : travelList) {
                    int monthNum = travel.getTravelTime().getMonth() + 1;
                    if (!monthNumList.contains(monthNum)) {
                        monthNumList.add(monthNum);
                    }
                    /*int days = travel.getDays();
                    if (!travelDays.contains(days)) {

                        travelDays.add(days);
                    }*/
                }
            }
            if (smallList != null && smallList.size() > 0){
                for (SmallTheme smallTheme : smallList) {
                    smallThemeIds.add(smallTheme.getId());
                }
            }
            vo.setDestId(dest.getId());
            vo.setDestName(dest.getName());
            vo.setCoverUrl(dest.getCoverUrl());
            vo.setInfo(dest.getInfo());
            vo.setMonthNums(monthNumList);
            vo.setSmallThemeId(smallThemeIds);
            if(count != null){
                vo.setCount(count);
            }else {
                vo.setCount(0L);
            }

            destinationScreenService.save(vo);


        }
        return "ok了";
    }

}
