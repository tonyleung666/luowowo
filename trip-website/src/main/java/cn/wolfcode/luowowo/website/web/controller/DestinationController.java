package cn.wolfcode.luowowo.website.web.controller;

import cn.wolfcode.luowowo.article.domain.*;
import cn.wolfcode.luowowo.article.query.StrategyCatalogQuery;
import cn.wolfcode.luowowo.article.query.TravelQuery;
import cn.wolfcode.luowowo.article.service.*;
import cn.wolfcode.luowowo.search.domain.DestScreenVO;
import cn.wolfcode.luowowo.search.query.DestinationScreenQuery;
import cn.wolfcode.luowowo.search.service.IDestinationScreenService;
import cn.wolfcode.luowowo.website.util.ModelUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/destination")
public class DestinationController {
    @Reference
    private IDestinationService destinationService;

    @Reference
    private IRegionService regionService;

    @Reference
    private ITravelService travelService;

    @Reference
    private IStrategyCatalogService catalogService;

    @Reference
    private IStrategyDetailService detailService;

    @Reference
    private IDestinationScreenService destinationScreenService;

    @Reference
    private ISmallThemeService smallThemeService;

    @RequestMapping("")
    public String index(Model model,@ModelAttribute("qo") DestinationScreenQuery qo) {
        List<Region> hotRegions = regionService.queryHotRegions();
        model.addAttribute("hotRegions", hotRegions);
        if (qo.getMonthNum()== -1) {
            List<DestScreenVO> list = destinationScreenService.queryDestScreenVO(1);
            model.addAttribute("vos", list);
        }else {
            List<DestScreenVO> list = destinationScreenService.queryDestScreenVO(qo.getMonthNum());
            model.addAttribute("vos", list);
        }

        //yearsCom通过大主题id=1 查出小主题的集合
        List<SmallTheme> yearsCom = smallThemeService.querySmallThemeByBigThemeId(1L);
        model.addAttribute("yearsCom",yearsCom);
        //season通过大主题id=2 查出小主题的集合
        List<SmallTheme> season = smallThemeService.querySmallThemeByBigThemeId(2L);
        model.addAttribute("season",season);
        //goType通过大主题id=3 查出小主题的集合
        List<SmallTheme> goType = smallThemeService.querySmallThemeByBigThemeId(3L);
        model.addAttribute("goType",goType);
        //holiday通过大主题id=4 查出小主题的集合
        List<SmallTheme> holiday = smallThemeService.querySmallThemeByBigThemeId(4L);
        model.addAttribute("holiday",holiday);
        return "destination/index";
    }

    @RequestMapping("/getHotDestByRegionId")
    public String getHotDestByRegionId(@ModelAttribute("regionId") Long regionId, Model model) {
        List<Destination> dests = destinationService.getDestByRegionId(regionId);
        int size = dests.size();
        List<Destination> leftDests = dests.subList(0, size % 2 == 0 ? size / 2 : size / 2 + 1);
        List<Destination> rightDests = dests.subList(size % 2 == 0 ? size / 2 : size / 2 + 1, size);
        model.addAttribute("leftDests", leftDests);
        model.addAttribute("rightDests", rightDests);
        return "destination/hotdestTpl";
    }

    @RequestMapping("/guide")
    public String guide(Long id, Model model) {
        ModelUtils.addToastsAndDest(destinationService, model, id);
        //catalogs
        model.addAttribute("catalogs", catalogService.queryCatalogsByDestId(id));

        //点击量前三攻略
        List<StrategyDetail> strategyDetails = detailService.queryViewNumTop3(id);
        model.addAttribute("strategyDetails", strategyDetails);
        return "destination/guide";
    }

    @RequestMapping("/surveyPage")
    public String surveyPage(Model model, @ModelAttribute("qo") StrategyCatalogQuery qo) {
        ModelUtils.addToastsAndDest(destinationService, model, qo.getDestId());
        return "destination/survey";
    }

    @RequestMapping("/survey")
    public String survey(Model model, @ModelAttribute("qo") StrategyCatalogQuery qo) {
        //catalogs
        List<StrategyCatalog> catalogs = catalogService.queryCatalogsByDestId(qo.getDestId());
        model.addAttribute("catalogs", catalogs);

        //catalog
        if (catalogs.size() > 0) {
            for (StrategyCatalog catalog : catalogs) {
                if (catalog.getId() == qo.getCatalogId()) {
                    model.addAttribute("catalog", catalog);
                    List<StrategyDetail> details = catalog.getDetails();
                    if (details.size() > 0) {
                        StrategyDetail detail = details.get(0);
                        detail.setStrategyContent(detailService.getContent(detail.getId()));
                        model.addAttribute("detail", detail);
                    }
                    break;
                }
            }
        }
        return "destination/surveyTpl";
    }

    @RequestMapping("/travels")
    public String travels(Model model, @ModelAttribute("qo") TravelQuery qo) {
        PageInfo pageInfo = travelService.query(qo);
        model.addAttribute("pageInfo", pageInfo);
        return "destination/travelTpl";
    }
}
