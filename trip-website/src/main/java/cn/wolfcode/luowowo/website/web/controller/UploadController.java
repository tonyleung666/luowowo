package cn.wolfcode.luowowo.website.web.controller;

import cn.wolfcode.luowowo.website.util.UploadUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class UploadController {
    @Value("${file.path}")
    private String filePath;

    @RequestMapping("/coverImageUpload")
    @ResponseBody
    public Object uploadImg(MultipartFile pic) {
        String fileName= UploadUtil.upload(pic, filePath);
        return fileName;
    }

}
