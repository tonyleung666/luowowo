package cn.wolfcode.luowowo.website.web.controller;


import cn.wolfcode.luowowo.article.domain.SmallTheme;
import cn.wolfcode.luowowo.article.service.ISmallThemeService;
import cn.wolfcode.luowowo.search.domain.DestScreenVO;
import cn.wolfcode.luowowo.search.query.DestinationScreenQuery;
import cn.wolfcode.luowowo.search.service.IDestinationScreenService;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/destinationscreen")
public class DestinationScreenController {
    @Reference
    private IDestinationScreenService destinationScreenService;

    @Reference
    private ISmallThemeService smallThemeService;

    @RequestMapping("/destscreen")
    public String destscreen(Model model, @ModelAttribute("qo")DestinationScreenQuery qo){
        //通过qo去查vo
        //返回一个DestScreenVO集合过去
        /*List<DestScreenVO> list =  destinationScreenService.queryDestScreenVOByQo(qo);
        List<DestScreenVO> destScreenVOS = list.subList(0, 10);
        model.addAttribute("destList",destScreenVOS);*/

        //dateList
        List<SmallTheme> dateList = smallThemeService.querySmallThemeByBigThemeId(4L);
        model.addAttribute("dateList",dateList);
        //yearList
        List<SmallTheme> yearList = smallThemeService.querySmallThemeByBigThemeId(1L);
        model.addAttribute("yearList",yearList);
        //seasonList
        List<SmallTheme> seasonList = smallThemeService.querySmallThemeByBigThemeId(2L);
        model.addAttribute("seasonList",seasonList);
        //goList
        List<SmallTheme> goList = smallThemeService.querySmallThemeByBigThemeId(3L);
        model.addAttribute("goList",goList);

        return "destination/destFilter";
    }
    @RequestMapping("/filterCommend")
    //@ResponseBody
    public Object commendandselect(Model model,@ModelAttribute("qo") DestinationScreenQuery qo){
        Long timeId = qo.getTimeId();
        Long themeId = qo.getThemeId();
        Long dayId = qo.getDayId();
        String destName = qo.getDestName();
        Integer monthNum = qo.getMonthNum();
        if (!destName.equals("1")){
            List<DestScreenVO> list =  destinationScreenService.queryDestScreenVOByDestName(timeId,themeId,dayId,monthNum,destName);
            //List<DestScreenVO> list =  destinationScreenService.queryDestScreenVOByQo(qo);
            //System.out.println(list.size());

            System.out.println(list);
            model.addAttribute("destList",list);
            return "destination/destFilterComment";

        }
        List<DestScreenVO> list =  destinationScreenService.queryDestScreenVOByQo(timeId,themeId,dayId,monthNum);
        //List<DestScreenVO> list =  destinationScreenService.queryDestScreenVOByQo(qo);
        //System.out.println(list.size());

        System.out.println(list);
        model.addAttribute("destList",list);
        return "destination/destFilterComment";
        //return "ok";
    }
    @RequestMapping("/monthScreen")
    public String monthScreen(Model model, @ModelAttribute("qo")DestinationScreenQuery qo){
        if (qo.getMonthNum()== -1) {
            List<DestScreenVO> list = destinationScreenService.queryDestScreenVO(1);
            model.addAttribute("vos", list);
        }else {
            List<DestScreenVO> list = destinationScreenService.queryDestScreenVO(qo.getMonthNum());
            model.addAttribute("vos", list);
        }
        return "destination/timeComment";
    }
    @RequestMapping("/titleSelect")
    public String titleSelect(Model model, @ModelAttribute("qo")DestinationScreenQuery qo){
        //yearsCom通过大主题id=1 查出小主题的集合
        List<SmallTheme> smallTitleList = smallThemeService.querySmallThemeByBigThemeId(qo.getBigThemeId());
        model.addAttribute("smallTitleList",smallTitleList);

        /*//yearsCom通过大主题id=1 查出小主题的集合
        List<SmallTheme> yearsCom = smallThemeService.querySmallThemeByBigThemeId(1L);
        model.addAttribute("yearsCom",yearsCom);
        //season通过大主题id=2 查出小主题的集合
        List<SmallTheme> season = smallThemeService.querySmallThemeByBigThemeId(2L);
        model.addAttribute("season",season);
        //goType通过大主题id=3 查出小主题的集合
        List<SmallTheme> goType = smallThemeService.querySmallThemeByBigThemeId(3L);
        model.addAttribute("goType",goType);
        //holiday通过大主题id=4 查出小主题的集合
        List<SmallTheme> holiday = smallThemeService.querySmallThemeByBigThemeId(4L);
        model.addAttribute("holiday",holiday);*/

        return "destination/titleselect";
    }
}
