package cn.wolfcode.luowowo.website.web.controller;

import cn.wolfcode.luowowo.comment.domain.ShopComment;
import cn.wolfcode.luowowo.comment.service.IShopCommentService;
import cn.wolfcode.luowowo.common.exception.LogicException;
import cn.wolfcode.luowowo.common.util.AjaxResult;
import cn.wolfcode.luowowo.member.PropQuery;
import cn.wolfcode.luowowo.member.domain.Prop;
import cn.wolfcode.luowowo.member.domain.UserInfo;
import cn.wolfcode.luowowo.member.service.IPropService;
import cn.wolfcode.luowowo.member.service.IUserInfoService;
import cn.wolfcode.luowowo.member.service.IUserPropService;
import cn.wolfcode.luowowo.website.annotation.UserParam;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;

/**
 * 积分商城
 */

@Controller
@RequestMapping("prop")
public class PropController {

    @Reference
    private IPropService propService;

    @Reference
    private IUserPropService userPropService;

    @Reference
    private IUserInfoService userInfoService;

    @Reference
    private IShopCommentService shopCommentService;

    @RequestMapping("")
    public String list(Model model, PropQuery qo, @UserParam UserInfo userInfo){
        if(userInfo!=null){
            model.addAttribute("userProp",userInfoService.get(userInfo.getId()));
        }
        model.addAttribute("props",propService.selectAll());
        return "itemshop/list";
    }

    @RequestMapping("p")
    public String list2(Model model, PropQuery qo, @UserParam UserInfo userInfo){
        if(qo.getQueryType()==0L){
            if(userInfo!=null){
                model.addAttribute("userProp",userInfoService.get(userInfo.getId()));
            }
            model.addAttribute("props",propService.selectAll());
            return "itemshop/itemTempl";
        }else{
            if(userInfo==null){
                throw new LogicException("你还没登录");
            }
            model.addAttribute("userProp",userPropService.getUserPropByUserId(userInfo.getId()));
            List<Prop> myProps = userPropService.getMyProps(userInfo.getId());
            model.addAttribute("myProps",myProps);
            return "itemshop/myItemTempl";
        }
    }
    @RequestMapping("buy")
    @ResponseBody
    public Object buy(Prop prop,@UserParam UserInfo userInfo){
        if(userInfo== null){
            throw new LogicException("亲你还没登录噢");
        }
        try {
            userPropService.insert(prop,userInfo.getId());
        }catch (LogicException e){
            e.printStackTrace();
            return new AjaxResult(false,e.getMessage());
        }
        return AjaxResult.success();
    }

    @RequestMapping("AddComment")
    @ResponseBody
    public Object AddComment(String comment ,String username,@UserParam UserInfo userInfo){
        if(userInfo==null){
            throw new LogicException("亲,请先登录!");
        }
        if(username!=null){
            comment=username+": "+comment;
        }
        ShopComment shopComment=new ShopComment();
        shopComment.setContent(comment);
        shopComment.setUserId(userInfo.getId());
        shopComment.setUsername(userInfo.getNickname());
        shopComment.setHeadUrl(userInfo.getHeadImgUrl());
        shopComment.setCreateTime(new Date());
        shopComment.setLevel(userInfo.getLevel());
        shopCommentService.save(shopComment);
        return AjaxResult.success();
    }

    @RequestMapping("/comment")
    public String comment(Model model) {
        Page page = shopCommentService.query();
        model.addAttribute("page", page);
        return "itemshop/commentTempl";
    }
}
