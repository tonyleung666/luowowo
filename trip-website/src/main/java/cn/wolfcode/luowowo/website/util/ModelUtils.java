package cn.wolfcode.luowowo.website.util;

import cn.wolfcode.luowowo.article.domain.Destination;
import cn.wolfcode.luowowo.article.service.IDestinationService;
import org.springframework.ui.Model;

import java.util.List;

public interface ModelUtils {
    static void addToastsAndDest(IDestinationService destinationService, Model model, Long destId) {
        List<Destination> toasts = destinationService.getToasts(destId);
        Destination dest = toasts.remove(toasts.size() > 0 ? toasts.size() - 1 : 0);
        model.addAttribute("dest", dest);
        model.addAttribute("toasts", toasts);
    }
}
