package cn.wolfcode.luowowo.website.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 登录检查注解
 * 贴有这个注解请求方法, 必须进行登录检查
 */
@Target({ElementType.PARAMETER})  //贴在方法上
@Retention(RetentionPolicy.RUNTIME)  //运行时期有效
public @interface UserParam {
}