package cn.wolfcode.luowowo.website.web.controller;

import cn.wolfcode.luowowo.cache.util.RedisKeys;
import cn.wolfcode.luowowo.common.exception.LogicException;
import cn.wolfcode.luowowo.common.util.AjaxResult;
import cn.wolfcode.luowowo.member.service.IUserInfoService;
import cn.wolfcode.luowowo.website.util.CookieUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;

/**
 * 登录和注册相关功能控制器
 */
@Controller
public class LoginController {
    @Reference
    private IUserInfoService userInfoService;

    // 检查手机号
    @RequestMapping("/checkPhone")
    @ResponseBody
    public Object checkPhone(String phone){
        return !userInfoService.checkPhone(phone);
    }


    //发送验证码
    @RequestMapping("/sendVerifyCode")
    @ResponseBody
    public Object sendVerifyCode(String phone) throws LogicException{
            userInfoService.sendVerifyCode(phone);
            return AjaxResult.success();
    }

    //用户注册
    @RequestMapping("/userRegist")
    @ResponseBody
    public Object userRegist(String phone, String nickname, String password, String rpassword, String verifyCode){
            userInfoService.userRegist(phone, nickname, password, rpassword, verifyCode);
            return new AjaxResult(true,"注册成功!");
    }

    //用户登录
    @RequestMapping("/userLogin")
    @ResponseBody
    public Object userLogin(String username, String password, HttpServletResponse response){
        String token = userInfoService.userLogin(username, password);
        CookieUtils.addCookie("token", token, RedisKeys.LOGIN_TOKEN.getTime().intValue(), response);
        return AjaxResult.success();
    }
}

