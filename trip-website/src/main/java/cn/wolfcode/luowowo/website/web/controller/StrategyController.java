package cn.wolfcode.luowowo.website.web.controller;

import cn.wolfcode.luowowo.article.domain.StrategyDetail;
import cn.wolfcode.luowowo.article.query.StrategyDetailQuery;
import cn.wolfcode.luowowo.article.service.IDestinationService;
import cn.wolfcode.luowowo.article.service.IStrategyCommendService;
import cn.wolfcode.luowowo.article.service.IStrategyDetailService;
import cn.wolfcode.luowowo.article.service.IStrategyTagService;
import cn.wolfcode.luowowo.cache.service.IStrategyStatisVOService;
import cn.wolfcode.luowowo.comment.domain.StrategyComment;
import cn.wolfcode.luowowo.comment.query.StrategyCommentQuery;
import cn.wolfcode.luowowo.comment.service.IStrategyCommentService;
import cn.wolfcode.luowowo.common.util.AjaxResult;
import cn.wolfcode.luowowo.member.domain.UserInfo;
import cn.wolfcode.luowowo.search.query.StrategySearchQuery;
import cn.wolfcode.luowowo.search.service.IStrategySearchService;
import cn.wolfcode.luowowo.search.vo.StatisVO;
import cn.wolfcode.luowowo.website.annotation.UserParam;
import cn.wolfcode.luowowo.website.util.ModelUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/strategy")
public class StrategyController {
    @Reference
    private IStrategyDetailService detailService;

    @Reference
    private IStrategyTagService strategyTagService;

    @Reference
    private IDestinationService destinationService;

    @Reference
    private IStrategyCommentService commentService;

    @Reference
    private IStrategyStatisVOService strategyStatisVOService;

    @Reference
    private IStrategyCommendService strategyCommendService;

    @Reference
    private IStrategySearchService strategySearchService;

    @RequestMapping("")
    public String index(Model model) {
        //commends
        model.addAttribute("commends", strategyCommendService.getTop5());

        //hotCds
        model.addAttribute("hotCds", strategyStatisVOService.queryHotCommends());

        //abroadCds
        model.addAttribute("abroadCds", strategyStatisVOService.queryAbroadCommends());

        //unabroadCds
        model.addAttribute("unabroadCds", strategyStatisVOService.queryInlandCommends());

        //themeCds
        List<Map<String, Object>> list = strategySearchService.queryThemeCommend();
        model.addAttribute("themeCds", list);

        //主题攻略条件列表: themes
        List<StatisVO> themes = strategySearchService.queryConditionThemes();
        model.addAttribute("themes", themes);

        //国内条件列表: chinas
        List<StatisVO> chinas = strategySearchService.queryConditionProvinces();
        model.addAttribute("chinas", chinas);


        //国外条件列表: abroads
        List<StatisVO> abroads = strategySearchService.queryConditionCountries();
        model.addAttribute("abroads", abroads);

        return "strategy/index";
    }

    @RequestMapping("/searchPage")
    public String searchPage(Model model, @ModelAttribute("qo")StrategySearchQuery qo) {
        Page page = strategySearchService.query(qo);
        model.addAttribute("page", page);
        return "strategy/searchPageTpl";
    }

    /**
     * 攻略明细
     * @param model
     * @param id
     * @return
     */
    @RequestMapping("/detail")
    public String detail(Model model, Long id) {
        StrategyDetail detail = detailService.get(id);
        detail.setStrategyContent(detailService.getContent(id));
        model.addAttribute("detail", detail);

        // 阅读数
        strategyStatisVOService.increaseViewnum(id, 1);

        // 热门推荐加分
        strategyStatisVOService.addHotScore(id, 1);

        // 共享vo对象
        model.addAttribute("vo", strategyStatisVOService.getStrategyStatisVO(id));
        return "strategy/detail";
    }

    /**
     * 攻略列表
     *
     * @param model
     * @param qo
     * @return
     */
    @RequestMapping("/list")
    public String list(Model model, @ModelAttribute("qo") StrategyDetailQuery qo) {
        ModelUtils.addToastsAndDest(destinationService, model, qo.getDestId());
        model.addAttribute("tags", strategyTagService.list());
        model.addAttribute("pageInfo", detailService.query(qo));
        return "strategy/list";
    }

    @RequestMapping("/commentAdd")
    @ResponseBody
    public Object commentAdd(StrategyComment comment, @UserParam UserInfo userInfo) {
        comment.setCity(userInfo.getCity());
        comment.setHeadUrl(userInfo.getHeadImgUrl());
        comment.setUserId(userInfo.getId());
        comment.setLevel(userInfo.getLevel());
        commentService.save(comment);

        // 热门推荐加分
        strategyStatisVOService.addHotScore(comment.getDetailId(), 1);

        //评论数 +1
        strategyStatisVOService.increaseReplynum(comment.getDetailId(), 1);
        return AjaxResult.success().addData(strategyStatisVOService.getStrategyStatisVO(comment.getDetailId()).getReplynum());
    }

    /**
     * 攻略评论列表
     *
     * @param model
     * @param qo
     * @return
     */
    @RequestMapping("/comment")
    public String comment(Model model, @ModelAttribute("qo") StrategyCommentQuery qo) {
        Page page = commentService.query(qo);
        model.addAttribute("page", page);

        return "strategy/commentTpl";
    }

    /**
     * 攻略评论点赞
     *
     * @param toid
     * @param fromid
     * @return
     */
    @RequestMapping("/commentThumbUp")
    @ResponseBody
    public Object commentThumbUp(String toid, Long fromid) {
        commentService.commentThumbUp(toid, fromid);
        return AjaxResult.success();
    }

    // 收藏
    @RequestMapping("/favor")
    @ResponseBody
    public Object favor(@UserParam UserInfo userInfo, Long sid) {
        if (userInfo == null) {
            // 未登录
            AjaxResult result = new AjaxResult(false, "请先登录!");
            result.setCode(AjaxResult.NO_LOGIN);
            return result;
        }
        // 收藏:true;  取消收藏:false
        boolean ret = strategyStatisVOService.favor(userInfo.getId(), sid);

        // 海内外推荐加分
        strategyStatisVOService.addAbroadScore(sid, ret ? 1 : -1);

        return new AjaxResult(ret, "").addData(strategyStatisVOService.getStrategyStatisVO(sid));
    }

    @RequestMapping("/strategyThumbup")
    @ResponseBody
    public Object strategyThumbup(@UserParam UserInfo userInfo, Long sid) {
        if (userInfo == null) {
            // 未登录
            AjaxResult result = new AjaxResult(false, "请先登录!");
            result.setCode(AjaxResult.NO_LOGIN);
            return result;
        }

        // 顶成功:true;  顶过了:false
        boolean ret = strategyStatisVOService.strategyThumbup(userInfo.getId(), sid);

        // 海内外推荐加分
        if (ret) {
            strategyStatisVOService.addAbroadScore(sid, 1);
        }

        return new AjaxResult(ret, "").addData(strategyStatisVOService.getStrategyStatisVO(sid));
    }

    @RequestMapping("/share")
    @ResponseBody
    public Object share(@UserParam UserInfo userInfo, Long sid) {
        if (userInfo == null) {
            // 未登录
            AjaxResult result = new AjaxResult(false, "请先登录!");
            result.setCode(AjaxResult.NO_LOGIN);
            return result;
        }

        strategyStatisVOService.strategyShare(sid);

        return AjaxResult.success().addData(strategyStatisVOService.getStrategyStatisVO(sid));
    }
}

