<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title></title>
  <link href="/styles/base.css" rel="stylesheet" type="text/css">
    <link href="/styles/base.css" rel="stylesheet" type="text/css">
    <link href="/styles/replyDetail.css" rel="stylesheet" type="text/css">
  <link href="/styles/wendaDetail.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/js/jquery.js"></script>
    <script type="text/javascript" src="/js/plugins/jquery-form/jquery.form.js"></script>
    <link href="/js/plugins/jqPaginator/jqPagination.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/js/system/common.js"></script>
    <script type="text/javascript" src="/js/common.js"></script>
    <script src="/js/jquery-upload/jquery.ui.widget.js"></script>
    <script src="/js/jquery-upload/jquery.iframe-transport.js"></script>
    <script src="/js/jquery-upload/jquery.fileupload.js"></script>
    <style type="text/css">
        .bar {
            margin-top:10px;
            height:10px;
            max-width: 370px;
            background: green;
        }
    </style>
    <script>
        $(function () {
            $(function () {

                //回复
                $(".comment_list").on("click", ".re_reply", function () {
                    var cmtid = $(this).data("cmtid");
                    var username = $(this).data("username");
                    var replyid = $(this).data("replyid");
                    $('#replyContent'+ cmtid).attr("placeholder", "回复：" + username);
                    $('#replyContent'+ cmtid).focus();

                    $('#type' + cmtid).val(1);
                    $('#refComment' + cmtid).val(replyid);
                });

                $('._j_comment').click(function () {
                    var cmtid = $(this).data("cmtid");
                    $("#replyContent"+ cmtid).attr("placeholder", "");

                    $('#type' + cmtid).val(0);
                    $('#refComment' + cmtid).val(null);

                    $("#replyContent"+ cmtid).focus();
                });

                //发表回复
                $(".btn_submit_reply").click(function () {
                    var cmtid = $(this).data("cmtid");
                    if (!$("#replyContent" + cmtid).val()) {
                        popup("评论不能为空");
                        return;
                    }
                    $("#replyForm" + cmtid).ajaxSubmit(function (data) {
                        $("#commentContent").val("");
                        $("#commentContent").attr("placeholder", "");
                        $("#reply_list"+cmtid).append(data);
                        window.location.reload();
                    })
                })
            })
        })
    </script>
</head>

<body style="position: relative;">
  <div class="topBar">
    <div class="topBarC">
      <div class="logo"><a title="马蜂窝自由行" href="javascript:;">马蜂窝自由行</a></div>
      <div class="t_nav">
        <ul id="pnl_nav" data-cs-t="headnav_wo">
          <li data-cs-p="index">
            <strong class="t"><a href="/">首页</a></strong>
          </li>
          <li data-cs-t="wenda" data-cs-p="wenda">
            <strong class="t"><a data-cs-p="from_wo_nav" href="/question">问答</a></strong>
          </li>
          <li data-cs-t="things" data-cs-p="things">
            <strong class="t"><a data-cs-p="from_wo_nav" href="javascript:;">马蜂窝周边</a></strong>
          </li>
          <li data-cs-p="together">
            <strong class="t"><a href="javascript:;">结伴</a></strong>
          </li>
          <li data-cs-p="group">
            <strong class="t"><a href="javascript:;">小组</a></strong>
          </li>
          <li data-cs-p="mall">
            <strong class="t"><a href="javascript:;">蜂首俱乐部</a></strong>
          </li>
          <li class="drop" data-cs-p="other">
            <strong class="t"><a href="javascript:;">更多<b></b></a></strong>
          </li>
        </ul>
      </div>
      <div class="t_search">
        <form method="GET" action="/search/s.php" name="search">
          <input type="text" class="key" value="" name="q" id="word">
          <input type="submit" value="" class="btn">
        </form>
      </div>

      <div class="t_info">
        <div class="pagelet-block">
          <ul class="user_info">
            <li class="daka">
              <span class="daka_btn" id="_j_dakabtn" data-japp="daka">
                <a role="button" title="打卡" class="daka_before">打卡</a>
                <a role="button" title="打卡推荐" class="daka_after">打卡推荐</a>
              </span>
            </li>
            <li id="pnl_user_msg" data-hoverclass="on" class="msg _j_hoverclass">
              <span id="oldmsg" class="oldmsg"><a href="javascript:;" class="infoItem">消息<b></b></a></span>
              <ul id="head-msg-box" class="drop-bd">
                <li><a href="javascript:;" rel="nofollow">私信</a></li>
                <li><a href="javascript:;" rel="nofollow">小组消息</a></li>
                <li><a href="javascript:;" rel="nofollow">系统通知</a></li>
                <li><a href="javascript:;" rel="nofollow">问答消息</a></li>
                <li><a href="javascript:;" rel="nofollow">回复消息</a></li>
                <li><a href="javascript:;" rel="nofollow">喜欢与收藏</a></li>
                <li><a href="javascript:;" rel="nofollow">好友动态</a></li>
              </ul>
            </li>
            <li class="ub-item ub-new-msg" id="head-new-msg">
            </li>
            <li class="account _j_hoverclass" data-hoverclass="on" id="pnl_user_set">
              <span class="t"><a class="infoItem" href="javascript:;"><img
                    src="http://b2-q.mafengwo.net/s12/M00/35/B7/wKgED1uqIs-AMYTwAAAX-VIKIo0071.png?imageMogr2%2Fthumbnail%2F%2132x32r%2Fgravity%2FCenter%2Fcrop%2F%2132x32%2Fquality%2F90"
                    width="32" height="32" align="absmiddle"><b></b></a></span>
              <div class="uSet c">
                <div class="asset">
                  <a class="coin" href="javascript:;" target="_blank" rel="nofollow">蜂蜜 0</a>
                  /
                  <a class="coin" href="javascript:;" target="_blank" id="head-my-honey" rel="nofollow"
                    data-cs-p="coin">金币 579</a>
                </div>
                <a href="javascript:;">我的马蜂窝<b class="tb-level">LV.3</b></a>
                <a href="javascript:;" target="_blank">写游记</a>
                <a href="javascript:;" target="_blank">预约游记</a>
                <a href="javascript:;" target="_blank">我的足迹</a>
                <a href="javascript:;" target="_blank">我的问答</a>
                <a href="javascript:;" target="_blank">我的好友</a>
                <a href="javascript:;" target="_blank">我的收藏</a>
                <a href="javascript:;" target="_blank">我的路线</a>
                <a href="javascript:;" target="_blank">我的订单</a>
                <a href="javascript:;" target="_blank">我的优惠券</a>
                <a href="javascript:;" target="_blank">设置</a>
                <a href="javascript:;">退出</a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="wrapper">
    <div class="detail-wrap clearfix">
      <div class="col-main">
        <div class="q-detail">
          <div class="q-content">
            <div class="q-title">
              <a href="/wenda/area-10065.html" target="_blank" class="location"><i></i>${(question.user.city)!}</a>
              <h1>
                <a href="/wenda/detail-18458675.html">${question.title}</a>
              </h1>
            </div>
            <div class="q-desc">
              ${(question.content.content)!}
              <div class="area_tags _j_tip_mdd" data-mddid="10065"><a data-cs-p="qa_mdd" class="at_link"
                  href="/travel-scenic-spot/mafengwo/10065.html" target="_blank">${(question.dest.name)!}</a>
                <div class="at_info" style="display:none;"></div>
              </div>${(question.title)!}</div>
            <div class="q-info1 clearfix">
              <div class="q-tags fl">
                <a class="a-tag" href="/wenda/area-10065.html" target="_blank">${(question.dest.name)!}</a>
              </div>
              <div class="pub-bar fr">
                <a href="/wenda/u/43303516/answer.html" class="photo" target="_blank"> <img
                    src="${question.user.headImgUrl}"
                    width="16" height="16"></a>
                <a class="name" href="/wenda/u/43303516/answer.html" target="_blank">${(question.user.nickname)!}</a>
                <span class="time"><span>${(question.createTime?string('yyyy年MM月dd日 HH:mm:ss'))!}</span></span>
              </div>
            </div>
          </div>
          <div class="q-operate clearfix">
            <div class="fl">
              <!-- 问题分享 -->
              <div class="q-share cate-share">
                <a class="_js_showShare"><i class="q-share-icon"></i>分享</a>
                <div class="share-pop _j_share_pop hide clearfix" data-title="为什么说在北京通勤等于取经？上下班到底有多苦？"
                  data-qid="18458675">
                  <a title="分享到新浪微博" class="sina _j_do_share" data-site="wb"></a>
                  <a title="分享到QQ空间" class="zone _j_do_share" data-site="qz"></a>
                  <a title="分享到微信" class="weixin _j_do_share_wx" data-site="wx"></a>
                </div>
              </div>
              <!-- 邀请回答 -->
              <div class="seek-help _j_tip_box">
                <a class="_j_seek_help_new">邀请蜂蜂回答</a>
              </div>
              <!-- 举报 -->
              <div class="admin_hide tip-off">
                <a data-japp="report" data-refer="http://www.mafengwo.cn/wenda/detail-18458675.html"
                  data-refer-uid="43303516" data-app="qa.question" data-busi-id="qid:18458675">举报</a>
              </div>
            </div>
            <div class="fr">
              <span class="atten-num">887浏览</span>
              <span class="atten-num"><span class="_j_same_num">1</span>人关注</span>

              <a class="btn-atten _j_same_question " rel="nofollow" data-status="1"><span>关注</span></a>

              <a class="btn-answer _j_btn_goanswer" rel="nofollow" href="/question/input?id=${question.id}">回答</a>
            </div>
          </div>
        </div>
        <div class="answer-wrap">
          <div class="hd">
            <a href="javascript:;" class="view_all">查看全部12个回答</a>
            <div style="display:none;"><span id="_j_anum">12</span>个回答</div>
          </div>
            <style>
                .mod-innerScenic .more {
                    margin-top: 20px;
                    text-align: center;
                }

                .mod-innerScenic .more a {
                    display: inline-block;
                    width: 160px;
                    height: 50px;
                    background-color: #fff;
                    border: 1px solid #fc9c27;
                    line-height: 50px;
                    color: #ff9d00;
                    font-size: 14px;
                    border-radius: 4px;
                    text-align: center;
                }

                .mod-innerScenic .num {
                    width: 40px;
                }
            </style>
              <div class="l-comment">

                  <div class="clearfix com-form">
                      <!--评论-->
                      <div data-anchor="commentlist">

                          <div id="pagelet-block-15f9d6d9ad9f6c363d2d27120e8a6198" class="pagelet-block">
                              <div class="mod mod-reviews" data-cs-p="评论列表">
                                  <div class="mhd mhd-large">评论列表</div>
                                  <div class="review-nav">
                                      <ul class="clearfix">
                                          <li data-type="0" data-category="0" class="on"><span class="divide"></span><a
                                                  href="javascript:void(0);"><span>全部</span></a></li>
                                      </ul>
                                  </div>
                                  <div class="loading-img" style="display: none;"><img
                                          src="http://images.mafengwo.net/images/weng/loading3.gif"> Loading...
                                  </div>
                                  <div class="_j_commentlist">
                                      <div class="rev-list" >
                                          <ul>
                                            <#list (page.content)! as cmt>
                                                <li class="rev-item comment-item clearfix">
                                                    <div class="user">
                                                        <a class="avatar" href="/#" ><img src="${cmt.headUrl!}" width="48" height="48"></a>
                                                        <span class="level">LV.${cmt.userLavel!}</span></div>
                                                    <a class="useful" data-id="191407415" title="点赞">
                                                        <i></i><span class="useful-num">${cmt.thumbupnum!}</span>
                                                    </a>
                                                    <a class="name" href="/u/52068941.html" target="_blank">${cmt.username!}</a>
                                                    <p class="rev-txt">${cmt.content!}
                                                    </p>


                                                    <div class="info clearfix">
                                                        <a class="btn-comment _j_comment" title="添加评论"
                                                           data-cmtid="${cmt.id}">评论</a>
                                                        <span class="time">${cmt.answerTime?string('yyyy-MM-dd HH:mm:ss')}</span>
                                                        </span>
                                                    </div>

                                                    <div class="comment add-reply-${cmt.id}">
                                                      <#if cmt.reply ??>
                                                          <ul class="more_reply_box comment_list" id="reply_list${cmt.id}">
                                                            <#list cmt.reply as reply>
                                                              <#if reply.type == 0>
                                                                  <li>
                                                                      <a href="/#">
                                                                          <img src="${reply.headUrl!}"
                                                                               width="16" height="16">${reply.username!}
                                                                      </a>
                                                                      ： ${reply.content!}
                                                                      <a class="_j_reply re_reply" data-replyid="${reply.id}" data-cmtid="${cmt.id}"
                                                                         data-username="${reply.username!}" title="添加回复">回复</a>
                                                                      <br><span class="time">${reply.createTime?string('yyyy-MM-dd HH:mm:ss')}</span>
                                                                  </li>
                                                              <#else>
                                                                  <li>
                                                                      <a href="/#">
                                                                          <img src="${reply.headUrl!}"
                                                                               width="16" height="16">${reply.username!}
                                                                      </a>
                                                                      回复${reply.refComment.username!}：${reply.content!}
                                                                      <a class="_j_reply re_reply" data-replyid="${reply.id}" data-cmtid="${cmt.id}"
                                                                         data-username="${reply.username!}" title="添加回复">回复</a>
                                                                      <br><span class="time">${reply.createTime?string('yyyy-MM-dd HH:mm:ss')}</span>
                                                                  </li>
                                                              </#if>
                                                            </#list>
                                                          </ul>
                                                      </#if>
                                                    <div class="add-comment hide reply-form">
                                                        <form id="replyForm${cmt.id}" action="/question/commentAdd" method="post">
                                                            <input type="hidden" name="answerId" value="${cmt.id!}">
                                                            <input type="hidden" name="type" value="0" id="type${cmt.id}">
                                                            <input type="hidden" name="refComment.id" id="refComment${cmt.id}">
                                                            <textarea class="comment_reply" name="content" id="replyContent${cmt.id}"
                                                                      style="overflow: hidden; color: rgb(204, 204, 204);"></textarea>
                                                            <a class="btn btn_submit_reply" data-cmtid="${cmt.id}">回复</a>
                                                        </form>
                                                    </div>
                                                </div>
                                                </li>
                                            </#list>
                                          </ul>
                                      </div>

                                      <div align="right" class="m-pagination">
                                          <span class="count">共<span>${page.totalPages!}</span>页 / <span>${page.totalElements!}</span>条</span>
                                        <#list 1..page.totalPages as i>
                                            <a class="pi" data-page="${i}" rel="nofollow" title="第${i}页">${i}</a>
                                        </#list>
                                          <a class="pi pg-next" data-page="2" rel="nofollow" title="后一页">后一页</a>
                                          <a class="pi pg-last" data-page="5" rel="nofollow" title="末页">末页</a>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>






<#--
                      <div class="img"><img
                              src=" http://n1-q.mafengwo.net/s12/M00/35/98/wKgED1uqIreAU9QZAAAXHQMBZ74008.png?imageMogr2%2Fthumbnail%2F%2148x48r%2Fgravity%2FCenter%2Fcrop%2F%2148x48%2Fquality%2F100 ">
                      </div>
                      <div class="fm-tare user-log">
                          <textarea class="_j_comment_content" id="content${a.id}"></textarea>
                          <button type="button" class="_j_save_comment" id="commentBtn${a.id}" data-answerid="${a.id!}">评论</button>
                      </div>
                  </div>-->

              </div>
        </div>
      </div>
      <div class="col-side"></div>
    </div>
  </div>
      <script type="text/javascript">
          $('._j_submit').click(function () {
              $('#commentForm').ajaxSubmit(function (data) {
                  window.location.reload();
              });
          });

          $(function () {
              $("._j_starlist a").mouseover(function () {
                  var index = $(this).index()+1;
                  var text = $(this).attr("title");

                  $(this).closest("div").prev().addClass("star"+index);
                  $(this).closest("div").parent().next().html(text);
              }).mouseout(function () {
                  var index = $(this).index()+1;
                  var text = $(this).attr("title");
                  $(this).closest("div").prev().removeClass("star"+index);
                  $(this).closest("div").parent().next().html(text);

                  var x = $(this).closest("div").prev().prev().val();
                  if(x == index){
                      $(this).closest("div").prev().addClass("star"+x);
                  }


              }).click(function () {
                  var index = $(this).index()+1;
                  var text = $(this).attr("title");
                  $(this).closest("div").prev().addClass("star"+index);
                  $(this).closest("div").parent().next().html(text);

                  $(this).closest("div").prev().prev().val(index);

              })
          })

      </script>
</body>

</html>