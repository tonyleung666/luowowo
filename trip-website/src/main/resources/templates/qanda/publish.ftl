<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title></title>
    <link href="/styles/base.css" rel="stylesheet" type="text/css">
    <link href="/styles/public.css" rel="stylesheet" type="text/css">
    <link href="/styles/addtravelnote.css" rel="stylesheet" type="text/css">
    <link href="/js/ueditor/themes/default/css/umeditor.css" type="text/css" rel="stylesheet">
    <link href="/js/plugins/datepicker/datepicker.css" rel="stylesheet">
    <script type="text/javascript" src="/js/jquery/jquery.js"></script>
    <script type="text/javascript" charset="utf-8" src="/js/ueditor/umeditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="/js/ueditor/umeditor.min.js"></script>
    <script type="text/javascript" src="/js/ueditor/lang/zh-cn/zh-cn.js"></script>
    <script src="/js/plugins/datepicker/datepicker.js"></script>
    <script src="/js/plugins/jquery-form/jquery.form.js"></script>

    <link href="/js/plugins/chosen/chosen.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/js/plugins/chosen/chosen.jquery.js"></script>
    <script type="text/javascript" src="/js/system/common.js"></script>
    <script type="text/javascript" src="/js/addtravelnote.js"></script>


    <script>
        //保存或发表
        $(function () {
            $(".chose_pic").click(function () {
                $("#coverBtn").click();
            });
            $("#coverBtn").change(function () {
                if(this.value){
                    $("#coverForm").ajaxSubmit(function (data) {
                        $(".choseBtn").html(" + 重新选择");
                        $("#coverImage").attr("src", "/" +data);
                        $("#coverValue").val("/" + data);
                    })
                }
            });

            $(".qt-post-btn").click(function () {
                /*var labels = [];
                $('.pi-tagitem span').each(function (index, value) {
                    labels.push($(value).text());
                })

                $("#tagInput").val(labels.join(","))*/
                $("#state").val($(this).data("state"));
                $("#editForm").ajaxSubmit(function (data) {
                    if(data.success){
                        window.location.href = "/question"
                    }else{
                        popup("操作失败");
                    }
                })
            })
        })

    </script>
</head>

<body>

  <div class="lww_header">
    <div class="header_wrap">
      <div class="header_logo">
        <a href="javascript:;" class="lww_logo"></a>
      </div>
      <ul class="header_nav">
          <li name="index"><a href="/">首页</a></li>
          <li  name="destination"><a href="/destination">目的地</a></li>
          <li  name="strategy" ><a href="/strategy">旅游攻略</a></li>
          <li  name="travel" ><a href="/travel">旅游日记</a></li>
          <li  name=""><a href="javascript:;">去旅行<i class="icon_caret_down"></i></a></li>
          <li name=""><a href="/question">社区<i class="icon_caret_down"></i></a></li>
      </ul>
      <div class="header_search">
        <input type="text" />
        <a class="icon_search"></a>
      </div>
      <div class="login_info">
        <div class="head_user">
          <a href="javascript:;">
            <img src="/images/user.png" />
            <i class="icon_caret_down"></i>
          </a>
        </div>
        <div class="header_msg">
          消息<i class="icon_caret_down"></i>
        </div>
        <div class="header_daka">
          <a href="javascript:;">打卡</a>
        </div>
      </div>
    </div>
    <div class="shadow"></div>
  </div>
  <div class="wrapper">
    <div class="qt-container clearfix">
      <div class="qt-main">
        <div class="crumb">
          <a href="/wenda/">旅游问答</a> &gt; <span>我要提问</span>
        </div>
          <form action="/coverImageUpload" method="post" id="coverForm">
              <input type="file" name="pic" id="coverBtn" style="display: none;">
          </form>
          <form class="forms" action="/question/addQuestion" method="post" id="editForm">
        <div class="qt-tit">
          <h5>问题标题</h5>
          <div class="qt-con">
            <input type="text" placeholder="标题不小于10字哦" name="title" class="_j_title">
            <span class="count"><span class="_j_title_num">0</span>/80 字</span>
            <span class="_j_min_num hide">10</span>
            <span class="error err-tips _j_title_error">标题不能少于10字</span>
          </div>
        </div>

        <div class="qt-details">
          <h5><a title="添加问答内容" class="icon active" id="_j_show_content"></a>问题详细内容</h5>
            <script id="editor" type="text/plain" style="width:100%;height:500px;"></script>
        </div>
        <div class="qt-tit">
                <h5>旅游地点</h5>
                <div class="qt-con">
                <select name="dest.id" data-placeholder="请选择目的地" id="region" style="width: 150px;">
                <option value="-1">--请选择--</option>
                <#list dests! as r>
                <option value="${r.id!}">${r.name!}</option>
                </#list>
                </select>
                 </div>
        </div>

        <div class="qt-tit">
                <h5>标签</h5>
                <div class="qt-con">
                <select name="tag.id" data-placeholder="请选择标签" id="tag" style="width: 150px;">
                <option value="-1">--请选择--</option>
                <#list tags! as t>
                <option value="${t.id!}">${t.tag!}</option>
                </#list>
                </select>
                </div>
        </div>
            <div class="publish_question">
              <a class="qt-post-btn _j_publish" title="发布问题">发布问题</a>
            </div>
      </div>
        </form>
    </div>
  </div>
</body>

</html>