<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title></title>
  <link href="/styles/base.css" rel="stylesheet" type="text/css">
  <link href="/js/datepicker/datepicker.css" rel="stylesheet">
  <link href="/styles/wenda.css" rel="stylesheet" type="text/css">
    <link href="/styles/public.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" src="/js/jquery/jquery.js"></script>
    <style>
        ._j_rank_list.hidden {
            display: none;
        }
    </style>
    <script>
        $(function () {
            //顶：点赞
            $(".zan").click(function () {

                var sid = $(this).data("sid");
                $.get("/question/answerThumbup", {sid:sid}, function (data) {
                    if(data.success){
                        $(".support_num").html(data.data.thumbsupnum);
                        popup("顶成功啦"); //

                    }else{
                        if(data.code == 102){
                            popup(data.msg);
                        }else{
                            popup("今天你已经顶过了"); //
                        }
                    }
                });
            });

            $('._j_rank_change_flag').click(function () {
                $('._j_rank_change_flag').removeClass('on');
                $(this).addClass('on');
            })
           /* $("._j_pager_box").on("click", function (ev) {
                var id = $(ev.target).closest("li").data("id");
                window.location.href = "/question/answerTop?questionId=" + id
            })*/
            $(".rank").on("click", "._j_rank_change_flag", function () {
                var id = $(this).data("rank")
                $("._j_rank_list.shown").removeClass("shown").addClass("hidden")
                $("#rank_" + id).addClass("shown").removeClass("hidden")
            })

        })

    </script>
</head>



<body style="position: relative;">
  <div class="topBar">
    <div class="topBarC">
      <div class="logo"><a title="马蜂窝自由行" href="javascript:;">马蜂窝自由行</a></div>
      <div class="t_nav">
        <ul id="pnl_nav" data-cs-t="headnav_wo">
          <li data-cs-p="index">
            <strong class="t"><a href="/">首页</a></strong>
          </li>
          <li data-cs-t="wenda" data-cs-p="wenda">
            <strong class="t"><a data-cs-p="from_wo_nav" href="javascript:;">问答</a></strong>
          </li>
          <li data-cs-t="things" data-cs-p="things">
            <strong class="t"><a data-cs-p="from_wo_nav" href="javascript:;">马蜂窝周边</a></strong>
          </li>
          <li data-cs-p="together">
            <strong class="t"><a href="javascript:;">结伴</a></strong>
          </li>
          <li data-cs-p="group">
            <strong class="t"><a href="javascript:;">小组</a></strong>
          </li>
          <li data-cs-p="mall">
            <strong class="t"><a href="/prop">积分商城</a></strong>
          </li>
          <li class="drop" data-cs-p="other">
            <strong class="t"><a href="javascript:;">更多<b></b></a></strong>
          </li>
        </ul>
      </div>
      <div class="t_search">
        <form method="GET" action="/search/s.php" name="search">
          <input type="text" class="key" value="" name="q" id="word">
          <input type="submit" value="" class="btn">
        </form>
      </div>

      <div class="t_info">
        <div class="pagelet-block">
          <ul class="user_info">
            <li class="daka">
              <span class="daka_btn" id="_j_dakabtn" data-japp="daka">
                <a role="button" title="打卡" class="daka_before">打卡</a>
                <a role="button" title="打卡推荐" class="daka_after">打卡推荐</a>
              </span>
            </li>
            <li id="pnl_user_msg" data-hoverclass="on" class="msg _j_hoverclass">
              <span id="oldmsg" class="oldmsg"><a href="javascript:;" class="infoItem">消息<b></b></a></span>
              <ul id="head-msg-box" class="drop-bd">
                <li><a href="javascript:;" rel="nofollow">私信</a></li>
                <li><a href="javascript:;" rel="nofollow">小组消息</a></li>
                <li><a href="javascript:;" rel="nofollow">系统通知</a></li>
                <li><a href="javascript:;" rel="nofollow">问答消息</a></li>
                <li><a href="javascript:;" rel="nofollow">回复消息</a></li>
                <li><a href="javascript:;" rel="nofollow">喜欢与收藏</a></li>
                <li><a href="javascript:;" rel="nofollow">好友动态</a></li>
              </ul>
            </li>
            <li class="ub-item ub-new-msg" id="head-new-msg">
            </li>
            <li class="account _j_hoverclass" data-hoverclass="on" id="pnl_user_set">
              <span class="t"><a class="infoItem" href="javascript:;"><img
                    src="http://b2-q.mafengwo.net/s12/M00/35/B7/wKgED1uqIs-AMYTwAAAX-VIKIo0071.png?imageMogr2%2Fthumbnail%2F%2132x32r%2Fgravity%2FCenter%2Fcrop%2F%2132x32%2Fquality%2F90"
                    width="32" height="32" align="absmiddle"><b></b></a></span>
              <div class="uSet c">
                <div class="asset">
                  <a class="coin" href="javascript:;" target="_blank" rel="nofollow">蜂蜜 0</a>
                  /
                  <a class="coin" href="javascript:;" target="_blank" id="head-my-honey" rel="nofollow"
                    data-cs-p="coin">金币 579</a>
                </div>
                <a href="javascript:;">我的马蜂窝<b class="tb-level">LV.3</b></a>
                <a href="javascript:;" target="_blank">写游记</a>
                <a href="javascript:;" target="_blank">预约游记</a>
                <a href="javascript:;" target="_blank">我的足迹</a>
                <a href="javascript:;" target="_blank">我的问答</a>
                <a href="javascript:;" target="_blank">我的好友</a>
                <a href="javascript:;" target="_blank">我的收藏</a>
                <a href="javascript:;" target="_blank">我的路线</a>
                <a href="javascript:;" target="_blank">我的订单</a>
                <a href="javascript:;" target="_blank">我的优惠券</a>
                <a href="javascript:;" target="_blank">设置</a>
                <a href="javascript:;">退出</a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="wrapper wrapper-new">

    <div class="col-main">
    <#list questions as q>
      <div class="newcate-wrap _j_qa_list">
        <div class="bd newcate-bd">
          <ul class="_j_pager_box">
            <li class="item clearfix _j_question_item" data-qid="793682">
              <div class="title">
                <a href="/question/selectQuestionById?id=${q.id}" target="_blank">${(q.title)!}</a> </div>
              <div class="container">
                <#if q.answer??>
                <div class="avatar"><a href="javascript:;" target="_blank" class="_j_filter_click">
                    <img class="_j_filter_click" src="${(q.answer.headUrl)!}"></a>
                </div>
                <div class="user-info">
                  <a class="name _j_filter_click" href="javascript:;" target="_blank">${(q.answer.username)!}</a>
                  <a class="level _j_filter_click" href="javascript:;" target="_blank" rel="nofollow">LV.${(q.answer.userLavel)!}</a>
                </div>
                    <a href="/question/selectQuestionById?id=${q.id}">
                      <div class="desc clearfix">
                          <img src="${q.answer.coverUrl}" width="150" height="100">
                          <p>
                          ${(q.answer.content)!}
                          </p>
                      </div>
                    </a>

                <div class="tags">
                  <a class="a-tag _j_filter_click" href="javascript:;" target="_blank">${(q.tag.tag)!}</a>
                  <a class="a-tag _j_filter_click" href="javascript:;" target="_blank">${(q.dest.name)!}</a>
                </div>
                <div class="operate">
                 <div class="zan"  data-sid="${q.answer.id!}"><i class="i05 "></i><em
                         class="support_num">${(vo.thumbsupnum)!}${q.answer.thumbupnum}</em></div>
                  <div class="mdd"><a href="javascript:;" class="_j_filter_click" target="_blank"><i
                        class="_j_filter_click"></i>${(q.dest.name)!}</a></div>
                  <div class="cate-share">
                    <a class="_js_showShare _j_filter_click">分享 ${q.answer.sharenum!}</a>
                  </div>
                  <span class="reply"><a href="/question/selectQuestionById?id=${q.id}">回答 (${q.answer.favornum})</a></span>
                  <span class="browse">浏览 (${q.answer.viewnum})</span>
                  <span class="date">${(q.answer.answerTime?string('yyyy年MM月dd日 HH:mm:dd'))!}</span>
                </div>
              </div>
            </#if>
            </li>
          </ul>
        </div>
      </div>
      </#list>
    </div>

    <div class="col-side">
        <div class="publish_question">
            <a href="/question/createQuestion" class="qt-post-btn _j_publish" title="我要提问">我要提问</a>
        </div>
      <div class="rank _j_rank" style="margin-top: 20px;">
        <div class="hd">排行榜<ul class="tab-time">
            <li class="_j_rank_change_date" data-type="0"><span>今日</span></li>
            <li class="_j_rank_change_date on" data-type="1"><span>本周</span></li>
            <li class="_j_rank_change_date" data-type="2"><span>本月</span></li>
          </ul>
        </div>
        <div class="bd">
          <ul class="tab-num" data-cs-p="rank_list">
            <li class="_j_rank_change_flag on" data-rank="0" data-cs-d="金牌数">金牌数</li>
            <li class="_j_rank_change_flag" data-rank="1" data-cs-d="回答数">回答数</li>
            <li class="_j_rank_change_flag" data-rank="2" data-cs-d="被顶次数">被顶次数</li>
          </ul>
            <ul class="rank-list _j_rank_list hidden" id="rank_0">
            <#list viewTop as u>
                <li class="r-top r-top1 clearfix">
                    <em class="num"></em>
                    <div class="user no_qid">
                        <a class="avatar" href="javascript:;" target="_blank" rel="nofollow"><img
                                src="${u.headUrl}"></a>
                        <span class="name"><a href="javascript:;" target="_blank"
                                              rel="nofollow">${u.username}</a></span>
                        <span class="level"><a href="javascript:;" target="_blank"
                                               rel="nofollow">${u.userLavel}</a></span>
                    </div>
                    <span class="num">${u.viewnum}</span>
                </li>
            </#list>
            </ul>

            <ul class="rank-list _j_rank_list shown" id="rank_2">
            <#list thumbupTop as u>
                <li class="r-top r-top1 clearfix">
                    <em class="num"></em>
                    <div class="user no_qid">
                        <a class="avatar" href="javascript:;" target="_blank" rel="nofollow"><img
                                src="${u.headUrl}"></a>
                        <span class="name"><a href="javascript:;" target="_blank"
                                              rel="nofollow">${u.username}</a></span>
                        <span class="level"><a href="javascript:;" target="_blank"
                                               rel="nofollow">${u.userLavel}</a></span>
                    </div>
                    <span class="num">${u.thumbupnum}</span>
                </li>
            </#list>
            </ul>
            <ul class="rank-list _j_rank_list hidden" id="rank_1">
            <#list shareTop as u>
                <li class="r-top r-top1 clearfix">
                    <em class="num">1</em>
                    <div class="user no_qid">
                        <a class="avatar" href="javascript:;" target="_blank" rel="nofollow"><img
                                src="${u.headUrl}"></a>
                        <span class="name"><a href="javascript:;" target="_blank"
                                              rel="nofollow">${u.username}</a></span>
                        <span class="level"><a href="javascript:;" target="_blank"
                                               rel="nofollow">${u.userLavel}</a></span>
                    </div>
                    <span class="num">${u.sharenum}</span>
                </li>
            </#list>
            </ul>
        </div>
          <#--<div class="pagelet-block">-->
              <#--<form action="/question/answerUserPage" method="post" id="searchForm">-->
                  <#--<input type="hidden" name="-->
<#--goldType" value="0" id="goldType">-->
                  <#--<input type="hidden" name="answerType" value="1" id="answerType">-->
                  <#--<input type="hidden" name="likeType" value="2" id="likeType">-->
                  <#--<div id="_j_tn_content">-->
                      <#--<!--游记&ndash;&gt;-->
                  <#--</div>-->
              <#--</form>-->
          <#--</div>-->
      </div>
    </div>
  </div>
</body>

</html>