<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <link href="/styles/base.css" rel="stylesheet" type="text/css">
    <link href="/styles/addtravelnote.css" rel="stylesheet" type="text/css">
    <link href="/js/ueditor/themes/default/css/umeditor.css" type="text/css" rel="stylesheet">
    <link href="/js/plugins/datepicker/datepicker.css" rel="stylesheet">
    <script type="text/javascript" src="/js/jquery/jquery.js"></script>
    <script type="text/javascript" charset="utf-8" src="/js/ueditor/umeditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="/js/ueditor/umeditor.min.js"></script>
    <script type="text/javascript" src="/js/ueditor/lang/zh-cn/zh-cn.js"></script>
    <script src="/js/plugins/datepicker/datepicker.js"></script>
    <script src="/js/plugins/jquery-form/jquery.form.js"></script>

    <link href="/js/plugins/chosen/chosen.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/js/plugins/chosen/chosen.jquery.js"></script>
    <script type="text/javascript" src="/js/system/common.js"></script>
    <script type="text/javascript" src="/js/addtAnswernote.js"></script>
    <script>
        $(function () {
            $(".chose_pic").click(function () {
                $("#coverBtn").click();
            });
            $("#coverBtn").change(function () {
                if(this.value){
                    $("#coverForm").ajaxSubmit(function (data) {
                        $(".choseBtn").html(" + 重新选择");
                        $("#coverImage").attr("src", "/" +data);
                        $("#coverValue").val("/" + data);
                    })
                }
            });



            //保存或发表
            $(".btn_submit").click(function () {
                /*var labels = [];
                $('.pi-tagitem span').each(function (index, value) {
                    labels.push($(value).text());
                })

                $("#tagInput").val(labels.join(","))*/
                $("#editForm").ajaxSubmit(function (data) {
                    if(data.success){
                        window.location.href = "/question/selectQuestionById?id=" + data.data
                    }else{
                        popup("操作失败");
                    }
                })
            })

            //目的地
            $("#region").chosen();

        })
    </script>
</head>


<body>
    <#assign currentNav="travel">
    <#include "../common/navbar.ftl">

    <!--文件上传-->
    <form action="/coverImageUpload" method="post" id="coverForm">
        <input type="file" name="pic" id="coverBtn" style="display: none;">
    </form>



    <div class="wrapper">
        <div class="ap-head">
            <h1>发表回复</h1>
            <div class="clearfix"></div>
        </div>
        <div class="ap-wrap">
            <div class="ap-main">
                <form class="forms" action="/question/addAnswer" method="post" id="editForm">
                    <input type="hidden" name="questionId" value="${(question.id)!}">
                    <dl>
                        <dd>
                            <div class="chose_pic">
                                <img src="${(answer.coverUrl)!}" id="coverImage"><span class="choseBtn">+ 选择封面</span>
                                <input type="hidden" name="coverUrl" id="coverValue">
                            </div>
                        </dd>
                    </dl>
                    <dl>
                        <div class="pi-base">
                            <div class="pi-row">
                                <div class="pi-col pi-date" style="margin-left: 2%;"> <label for="isopen">旅游地点</label>
                                    <div class="pi-dropdown "style="text-align: left" >
                                        <select name="destId" data-placeholder="请选择目的地" id="region" style="width: 150px;">
                                            <#list dests as r>
                                                <option value="${r.id!}">${r.name!}</option>
                                            </#list>
                                        </select>
                                    </div>
                                </div>
                                <div class="pi-col pi-date" style="margin-left: 2%;"> <label for="isopen">标签</label>
                                    <div class="pi-dropdown "style="text-align: left" >
                                        <select name="tagId" data-placeholder="请选择标签" id="region" style="width: 150px;">
                                        <#list tags as t>
                                            <option value="${t.id!}">${t.tag!}</option>
                                        </#list>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </dl>
                    <#--<dl>
                        <div class="pi-tags">
                            <label>游记标签</label>
                            <div class="pi-addTag"> <span class="added _j_added"></span> <input class="pi-addinput"
                                    type="text" name="tt" placeholder="添加游记标签，手动输入的标签使用回车键分隔">
                            </div>
                            <input type="hidden" id="tagInput" name="tags">
                            <script>
                                <#if tags??>
                                <#list tags as t>
                                    $('._j_added').append('<a class="pi-tagitem" role="button"><span>${t}</span><i class="rm_tag">×</i></a>');
                                </#list>
                                </#if>
                            </script>
                        </div>
                    </dl>-->
                    <dl class="body cf">
                        <dd id="content_div">
                            <script id="editor" type="text/plain" style="width:100%;height:500px;"></script>
                        </dd>
                    </dl>
                    <div class="btns">
                        <div class="accept_pact">
                            <input type="checkbox" checked="">
                            我已阅读并同意<a href="javascript:;" title="《骡窝窝游记协议》" target="_blank">《骡窝窝游记协议》</a>
                        </div>
                        <input class="btn_submit" value="回复" type="button"">


                    </div>
                </form>
            </div>
        </div>
    </div>
    <#include "../common/footer.ftl">
</body>

</html>