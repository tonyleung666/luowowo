<html class=" hasFontSmoothing-true">
<head>
    <script type="text/javascript" async="" charset="utf-8"
            src="http://c.cnzz.com/core.php?web_id=30065558&amp;t=q"></script>
    <script type="text/javascript" async="" charset="utf-8"
            src="http://w.cnzz.com/c.php?id=30065558&amp;async=1"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>【酒店预订】台北酒店价格查询，台北酒店推荐 - 骡窝窝</title>
    <meta name="Description"
          content="骡窝窝酒店预订，为您搜索台北各区域酒店价格查询及预订信息。通过对酒店价格、位置、设施、品牌、星级及用户真实点评为你推荐高性价比酒店，在线预订酒店，价格优惠实时折扣.">
    <meta name="Keywords" content="台北酒店预订，台北酒店价格查询，台北酒店推荐">


    <script src="https://hm.baidu.com/hm.js?8288b2ed37e5bc9b4c9f7008798d2de0"></script>


    <link href="http://css.mafengwo.net/css/cv/css+base:css+jquery.suggest:css+plugins:css+plugins+jquery.jgrowl:css+other+popup:css+mfw-header.2015^YlVS^1559526017.css"
          rel="stylesheet" type="text/css">


    <script language="javascript"
            src="http://js.mafengwo.net/js/cv/js+jquery-1.8.1.min:js+global+json2:js+M+Module:js+M+M:js+M+Log:js+m.statistics:js+advert+inspector^alw^1563531411.js"
            type="text/javascript" crossorigin="anonymous"></script>
    <script type="text/javascript">
        var __mfw_uid = parseInt('32085284');

        $(function () {
            /*$(".btn-sss").click(function () {
                $("#editForm").submit()
            })*/
            $("#post_to_hotel a").click(function () {
                $("#editForm").submit()
            })

            $('.btn-search a').click(function () {
                alert("别点了,这个功能没有建表去实现!");
            })
            $('.collect a').click(function () {
                alert("预订就先这样吧,前端页面太恶心,写不出来");
            })

        })
    </script>

    <script language="javascript"
            src="http://js.mafengwo.net/js/cv/js+common+jquery.plugins:js+common+widgets:js+mfw.storage^ZlI^1537192880.js"
            type="text/javascript" crossorigin="anonymous"></script>
    <link href="http://css.mafengwo.net/css/cv/css+hotel+new_hotel_v6.2:css+mdd+map-mark.v2:css+hotel+datepicker-range:css+hotel+number_guests_picker:css+mdd+hotel_fav:css+sales+m-toolbar:css+jquery-ui-1.9.1.custom.min^YlJS^1552035728.css"
          rel="stylesheet" type="text/css">

    <script async=""
            src="http://js.mafengwo.net/js/cv/js+Dropdown:
            js+pageletcommon+pageHeadUserInfoWWWNormal:
            js+jquery.tmpl:js+M+module+InputListener:
            js+M+module+SuggestionXHR:js+M+module+DropList:
            js+M+module+Suggestion:js+M+module+MesSearchEvent:
            js+SiteSearch:js+AHeader:js+jquery.jgrowl.min:
            js+hotel+mfwmap+mfwmap-util:js+hotel+mfwmap+mfwmap-event:
            js+hotel+mfwmap+mfwmap-runtime-google:js+hotel+mfwmap+mfwmap-runtime-amap:
            js+hotel+mfwmap+mfwmap-runtime-leaflet:js+hotel+mfwmap+mfwmap:
            js+hotel+mfwmap+mfwmap-overlays:js+hotel+module+ListTips:
            js+M+module+Storage:js+hotel+module+Log:js+hotel+module+Search:
            js+hotel+module+ModuleProvider:js+hotel+module+Captcha:
            js+hotel+module+Dialog:js+hotel+module+Hash:js+xdate:
            js+hotel+module+BookingDate:js+hotel+module+BookingGuests:
            js+hotel+list_mvc_model:js+corelib+handlebars-2.0.0:
            js+hotel+module+FestivalDateConfig:js+jquery-ui-core:
            js+jquery-ui-datepicker:js+hotel+module+DateRangePicker:
            js+hotel+module+NumberGuestsPicker:js+hotel+list_mvc_filter_view:js+jquery.scrollTo:
            js+M+module+dialog+Layer:js+M+module+dialog+DialogBase:js+M+module+dialog+Dialog:
            js+M+module+dialog+alert:js+hotel+module+FavDialog:js+hotel+list_mvc_data_view:
            js+hotel+list_v6:js+hotel+pc_app_guide:js+M+module+PageAdmin:js+M+module+Cookie:
            js+M+module+ResourceKeeper:js+AMessage:js+M+module+FrequencyVerifyControl:
            js+M+module+FrequencySystemVerify:js+ALogin:js+M+module+ScrollObserver:
            js+M+module+QRCode:js+AToolbar:js+ACnzzGaLog:js+ARecruit:js+ALazyLoad^YlFSRQ^1562127144.js"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="http://css.mafengwo.net/css/plugins/leaflet/leaflet.css?1537192876">
    <script async="" src="http://js.mafengwo.net/js/plugins/leaflet.js?1537192880" crossorigin="anonymous"></script>
    <script async="" src="http://js.mafengwo.net/js/BrowserState.js?1542357400" crossorigin="anonymous"></script>
</head>
<body style="position: relative;">


<style>
    @keyframes glowling {
        0% {
            background-position: -820px 0;
        }
        100% {
            background-position: 820px 0;
        }
    }

    .hotel-btns {
        position: relative;
    }

    .room_loading_wrapper {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: #fff;
    }

    .room_loading_wrapper li {
        height: 15px;
        margin-top: 20px;
        -webkit-animation-duration: 1.5s;
        animation-duration: 1.5s;
        -webkit-animation-fill-mode: forwards;
        animation-fill-mode: forwards;
        -webkit-animation-iteration-count: infinite;
        animation-iteration-count: infinite;
        -webkit-animation-name: glowling;
        animation-name: glowling;
        -webkit-animation-timing-function: linear;
        animation-timing-function: linear;
        background: no-repeat #f6f6f6;
        background-image: linear-gradient(270deg, #f6f6f6, #ebebeb 20%, #f6f6f6 40%, #f6f6f6);
    }

    .room_loading_wrapper li:nth-child(2) {
        width: 80%;
    }

    .room_loading_wrapper li:nth-child(3) {
        width: 45%;
    }

    .room_loading_wrapper li:nth-child(4) {
        width: 55%;
    }

    .room_loading_wrapper li:nth-child(5) {
        width: 70%;
    }

    .icon-sale0417 {
        margin: 2px 0 0 4px;
        display: inline-block;
        width: 70px;
        height: 20px;
        background-image: url(http://images.mafengwo.net/images/hotel/hotel_index/20180417-icon.png);
        background-position: bottom;
        background-size: contain;
        background-repeat: no-repeat;
        overflow: hidden;
        vertical-align: top;
    }

    .icon-sale0420 {
        margin: 2px 0 0 4px;
        display: inline-block;
        width: 60px;
        height: 20px;
        background-image: url(http://images.mafengwo.net/images/hotel/activity/2018_0417/20180420-icon.png);
        background-position: bottom;
        background-size: contain;
        background-repeat: no-repeat;
        overflow: hidden;
        vertical-align: top;
    }

    .hotel-sale-banner {
        position: relative;
        width: 100%;
        height: 120px;
        background-image: url(http://images.mafengwo.net/images/hotel/hotel_index/20180417-banner.png);
        background-repeat: no-repeat;
        background-size: cover;
        background-position: bottom;
        margin-bottom: 20px;
    }

    .hotel-new-icon {
        position: absolute;
        display: block;
        width: 238px;
        height: 210px;
        bottom: -1px;
        left: 5px;
        z-index: -1;
        background-image: url('http://css.mafengwo.net/images/hotel/new-customer-icon.gif');
        background-size: 100% 100%;
    }

    .hotel-new-icon:hover {
        z-index: 0;
        background-image: url('http://css.mafengwo.net/images/hotel/new-customer-hover-icon.png');
    }

    .hotel-new-icon .close {
        position: absolute;
        display: block;
        width: 25px;
        height: 25px;
        right: 0;
        bottom: 83px;
        border-radius: 50%;
    }

    .toolbar-item-top, .toolbar-item-feedback, .toolbar-item-code {
        padding-left: 40px;
    }

</style>

<div class="wrapper">
    <div class="top-info clearfix">
        <div class="crumb">
            <span class="tit">您在这里：</span>
            <div class="item">
                <div class="drop">
                    <span class="hd"><a href="/hotel/hotel" target="_blank">酒店<i></i></a></span>
                </div>
            </div>
        </div>
        <div class="weather-wrapper">
            <link href="http://css.mafengwo.net/weather/littleWeather.css?1530619858" rel="stylesheet" type="text/css">
        </div>
    </div>
    <form class="form-hotel" action="/hotel" method="get" id="editForm">
        <div class="hotel-searchbar clearfix">
            <#--<div class="hs-item hs-item-search" id="_j_hotel_search">
                <input type="text" value="" id="_j_search_input" autocomplete="off" name="name">
                <div class="hotel-suggest simsun" id="_j_search_suggest" style="display:none;"></div>
                <a class="hs-icon hs-icon-search" href="javascript:;" id="_j_search_btn"></a>
            </div>-->


            <div class="hs-item hs-item-people1" id="_j_booking_number_guests1">
            <#--<span>人数</span>
            <i class="icon-person"></i>-->
                </label> 出行目的地:</label>
                <select class="form-control" id="bonus" name="destId">
                <#if dest??>
                    <#list dest as d >
                        <option value=${d.id}  <#if qo.destId = d.id>selected</#if>    >${d.name}</option>
                    </#list>
                </#if>
                </select>
            </div>
            <div class="hs-item hs-item-search" id="_j_hotel_search">
                <input type="text" value="${(qo.checkIn?string('yyyy-MM-dd'))!}" id="_j_search_input" autocomplete="off" name="checkIn">
                <div class="hotel-suggest simsun" id="_j_search_suggest" style="display:none;"></div>
            </div>


            <div class="hs-item hs-item-search" id="_j_hotel_search">
                <input type="text" value="${(qo.checkOut?string('yyyy-MM-dd'))!}"" id="_j_search_input" autocomplete="off" name="checkOut">
                <div class="hotel-suggest simsun" id="_j_search_suggest" style="display:none;"></div>
            </div>


            <#--<div class="hs-item hs-item-date-wrapper" id="_j_booking_date">

                <div class="hs-item hs-item-date" id="_j_check_in_date">
                    <span></span>
                    <input type="text" placeholder="入住日期" value="wo"  id="dp1563708930698" class="hasDatepicker"
                           name="checkIn">
                    <i class="hs-icon hs-icon-date"></i>
                </div>
                <div class="hs-item hs-item-date" id="_j_check_out_date">
                    <span></span>
                    <input type="text" placeholder="离店日期"  id="dp1563708930699" class="hasDatepicker"
                           name="checkOut">
                    <i class="hs-icon hs-icon-date"></i>
                </div>
            </div>-->

            <#--<div class="hs-item hs-item-people number-guests-picker" id="_j_booking_number_guests">
                <span></span>
                <i class="icon-person"></i>
                <div class="ngp-dropdown _j_ngp_dropdown" style="display:none;">
                    <div class="item _j_ngp_room_item">
                        <div class="row-guests clearfix _j_ngp_row_guests"><span class="label"></span>
                            <div class="ngp-select">
                                <div class="select-trigger _j_ngp_select_trigger"><span></span>
                                    <div class="caret"><i></i></div>
                                </div>
                                <ul style="display:none;">
                                    <li data-value="1">1</li>
                                    <li data-value="2">2</li>
                                    <li data-value="3">3</li>
                                    <li data-value="4">4</li>
                                    <li data-value="5">5</li>
                                    <li data-value="6">6</li>
                                    <li data-value="7">7</li>
                                </ul>
                            </div>
                            <div class="ngp-select">
                                <div class="select-trigger _j_ngp_select_trigger"><span>0 儿童</span>
                                    <div class="caret"><i></i></div>
                                </div>
                                <ul style="display:none;">
                                    <li data-value="0">0</li>
                                    <li data-value="1">1</li>
                                    <li data-value="2">2</li>
                                    <li data-value="3">3</li>
                                    <li data-value="4">4</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row-children clearfix _j_ngp_row_children" style="display:none;"><span
                                class="label">儿童年龄</span>
                        </div>
                    </div>
                    <div class="item item-action clearfix _j_ngp_action_item"><span class="tips"></span><a
                            class="btn-action" href="javascript:;">确 认</a></div>
                </div>
            </div>-->

        <div class="hs-item hs-item-people1" id="_j_booking_number_guests1">
            <label for="course"> 入住人数:</label>
            <select class="form-control" id="peoplenum" name="num">
                <option value="1" <#if qo.num = 1>selected</#if> >1</option>
                <option value="2" <#if qo.num = 2>selected</#if> >2</option>
                <option value="3" <#if qo.num = 3>selected</#if> >3</option>
                <option value="4" <#if qo.num = 4>selected</#if> >4</option>
                <option value="5" <#if qo.num = 5>selected</#if> >5</option>
                <option value="6" <#if qo.num = 6>selected</#if> >6</option>
                <option value="7" <#if qo.num = 7>selected</#if> >7</option>
            </select>
            <div class="hs-item hs-item-search" id="post_to_hotel">
               <a class="hs-icon hs-icon-search" href="javascript:;" id="_j_search_btn"></a>
           </div>
        </div>
            <div class="hs-item hs-item-action btn-search" id="_j_price_btn1">
                <a class="hs-btn" href="javascript:;" class="showmessage">查看价格</a>
            </div>

        </div>
    </form>
    <div class="area-main clearfix">
        <div class="area-wrapper" id="_j_area_wrapper" style="">
            <dl class="item-area clearfix _j_area_list">
                <dt>区域:</dt>
                <dd>
                    <ul class="area-nav clearfix restrictheight" style="height: 84px;">
                        <li><a href="javascript:;" class="_j_area_name on" data-id="-1">全部</a></li>
                        <#if destList??>
                            <#list destList as dest>
                                <li>
                                    <a href="/destination/guide?id=241" class="_j_area_name" data-id=${dest.destId!}>${dest.name!}</a>
                                </li>
                            </#list>
                        </#if>
                    </ul>
                    <#--<a class="toggle" href="javascript:;" style="" data-count="15"><i></i>展开共14个区域</a>-->
                </dd>
                <dt>周边:</dt>
                <dd>
                    <ul class="area-nav clearfix">
                        <#if cityListTop4??>
                            <#list cityListTop4 as city>
                                <li>
                                    <a href="javascript:;" target="_blank">${city.name}</a>
                                </li>
                            </#list>
                        </#if>
                    </ul>
                </dd>
                <dt>攻略第一:</dt>
                <dd>
                    <#if strategyDetail??>
                        <a href="/strategy/detail?id=${strategyDetail.id}">${strategyDetail.subTitle!}</a>
                    </#if>
                </dd>
            </dl>
        </div>


        <div class="container">


            <div class="h-tab" id="_j_feature_tab">
                <ul class="ul-tab">
                    <li>
                        <a href="javascript:;" data-id="0" class="on">
                            全部
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" data-id="35322">
                            101夜景
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" data-id="35025">
                            带娃就住这
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" data-id="34790">
                            网红酒店
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" data-id="34942">
                            文艺青旅
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" data-id="39774">
                            米其林酒店榜
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" data-id="25890">
                            低价住好房
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" data-id="35316">
                            西门町人气酒店
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" data-id="19646">
                            Airbnb爱彼迎民宿
                            <span class="warn-mark"><span class="warn-mark-icon"></span><span class="warn-tips">通过Airbnb爱彼迎您可以预订全球191个国家逾百万民宿。无论是树屋、船屋、城堡或房车，都可以成为您下次旅行时入住的家。您还可以在Airbnb爱彼迎找到志同道合的房东，如设计师、艺术家、手工匠人、美食家等等。<i></i></span></span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="hotel-toolbar clearfix">
                <div class="htb-item htb-item-price" id="_j_price_slider">
                    <div class="htb-title"><span>¥0 - 4000</span>价格</div>
                    <div class="price-range">
                        <div class="range-bar ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
                            <div class="ui-slider-range ui-widget-header ui-corner-all"
                                 style="left: 0%; width: 100%;"></div>
                            <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0"
                                  style="left: 0%;"></span><span class="ui-slider-handle ui-state-default ui-corner-all"
                                                                 tabindex="0" style="left: 100%;"></span></div>
                    </div>
                </div>
                <div class="htb-item htb-item-dropdown" id="_j_star_filter">
                    <div class="htb-title">住宿等级</div>
                    <div class="htb-select">
                        <div class="sel-trigger _j_dropdown_filter_trigger">
                            <i class="i-sel-arrow"></i>
                            <span>不限</span>
                        </div>
                    </div>
                </div>
                <div class="htb-item htb-item-dropdown" id="_j_type_filter">
                    <div class="htb-title">住宿类型</div>
                    <div class="htb-select">
                        <div class="sel-trigger _j_dropdown_filter_trigger">
                            <i class="i-sel-arrow"></i>
                            <span>不限</span>
                        </div>

                    </div>
                </div>
                <div class="htb-item htb-item-dropdown" id="_j_facility_filter">
                    <div class="htb-title">设施</div>
                    <div class="htb-select">
                        <div class="sel-trigger _j_dropdown_filter_trigger">
                            <i class="i-sel-arrow"></i>
                            <span>不限</span>
                        </div>
                        <div class="sel-dropdown _j_dropdown_filter_content scroll-list">
                            <div class="empty">
                                <a class="btn" href="javascript:;">清空</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="htb-item htb-item-dropdown" id="_j_brand_filter">
                    <div class="htb-title">品牌</div>
                    <div class="htb-select">
                        <div class="sel-trigger _j_dropdown_filter_trigger">
                            <i class="i-sel-arrow"></i>
                            <span>不限</span>
                        </div>
                        <div class="sel-dropdown _j_dropdown_filter_content scroll-list" style="width: 260px;">

                            <div class="empty">
                                <a class="btn" href="javascript:;">清空</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="htb-item htb-item-dropdown" id="_j_airbnb_facility_filter" style="display:none;">
                    <div class="htb-title">设施</div>
                    <div class="htb-select">
                        <div class="sel-trigger _j_dropdown_filter_trigger">
                            <i class="i-sel-arrow"></i>
                            <span>不限</span>
                        </div>
                    </div>
                </div>
                <div class="htb-item htb-item-dropdown" id="_j_airbnb_property_type_filter" style="display:none;">
                    <div class="htb-title">住宿类型</div>
                    <div class="htb-select">
                        <div class="sel-trigger _j_dropdown_filter_trigger">
                            <i class="i-sel-arrow"></i>
                            <span>不限</span>
                        </div>
                    </div>
                </div>

            </div>

            <div class="hotel-sortbar clearfix">
                <div class="sortbar-info">
                    <div class="total" id="_j_total_text"><span>567</span> 家酒店</div>
                    <span class="hotel-switch" id="_j_available_switch">
                有房<span class="mui-switch"><i></i></span>全部酒店
            </span>
                    <div class="checkbox-list clearfix" id="_j_checkbox_list"><a class="mui-checkbox"
                                                                                 href="javascript:;"
                                                                                 data-id="19455"><i
                            class="icon-checkbox"></i>临地铁</a><a class="mui-checkbox" href="javascript:;"
                                                                data-id="hasFaved"><i
                            class="icon-checkbox"></i>收藏</a></div>
                    <div class="htb-search" id="_j_keyword_filter">
                        <div class="htb-searchbar">
                            <input type="text" placeholder="搜索关键词">
                            <a class="htb-icon-search" href="javascript:;"></a>
                            <i class="icon-search-clear" style="display:none;">×</i>
                        </div>
                    </div>
                </div>
                <div class="sortbar-nav" id="_j_sorter"><a href="javascript:;" data-type="comment" class="on">综合排序</a>
                    <a href="javascript:;" data-type="hot" class="sales-sort">销量</a>
                    <a class="price-sort" href="javascript:;" data-type="price"><i></i>价格</a>
                </div>
            </div>
            <div class="n-content">
                <div class="hotel-loading" id="_j_hotel_list_loading" style="display: none;"><i class="loading-m"></i>
                </div>
                <#--<div class="hotel-list" id="_j_hotel_list">
                <#if hotel??>
                    <#list hotel as h>
                        <div class="hotel-item clearfix _j_hotel_item" data-id="9031088" data-is-merge_room="0"
                             data-name="${h.name!}" data-lat="25.042788" data-lng="121.50931" data-is-airbnb="0"
                             data-cs-t="酒店list页点击入口分布">
                            <div class="hotel-pic">
                                <a href="${h.address!}" class="_j_hotel_info_link" target="_blank"
                                   data-cs-p="图片">
                                    <img src="${h.corverUrl!}"
                                         alt="" style="width: 330px;">
                                </a>

                            </div>
                            <div class="hotel-title">
                                <div class="title">
                                    <h3><a href="${h.url}" class="_j_hotel_info_link" target="_blank"
                                           title="${h.id}" data-cs-p="标题">${h.name}</a></h3>
                                    <br>
                                    <span>Hotel Midtown Richardson</span>
                                </div>
                            </div>
                            <div class="hotel-info ">
                                <ul class="nums clearfix">
                                    <li class="rating rating2">
                                        <em>${h.score}</em>分
                                    </li>
                                    <li class="split"></li>
                                </ul>

                                <div class="location">
                            <span><i class="icon-location"></i>详细地址: <a href="javascript:;" data-id="1762"
                                                                        data-type="area">${h.address}</a></span>
                                </div>

                            </div>
                        </div>
                    </#list>
                </#if>
                </div>-->
                <div style="margin-bottom: 24px;padding-left: 12px;">
                    <a style="color: #666;font-weight: bold;text-decoration: none;cursor: default;" target="_blank"
                       href="/hotel/license">骡窝窝酒店平台合作伙伴</a>
                </div>

                <#if hotelList??>
                    <#list hotelList as hotel>
                        <div class="hotel-item clearfix _j_hotel_item" data-id="9031088" data-is-merge_room="0"
                             data-name="台北德立庄酒店" data-lat="25.042788" data-lng="121.50931" data-is-airbnb="0"
                             data-cs-t="酒店list页点击入口分布">
                            <div class="hotel-pic">
                                <a href="/hotel/9031088.html?iMddid=10819" class="_j_hotel_info_link" target="_blank"
                                   data-cs-p="图片">
                                    <img src=${hotel.coverUrl!}
                                         alt="台北德立庄酒店预订" style="width: 330px;">
                                </a>
                            </div>
                            <div class="hotel-title">
                                <div class="title">
                                    <h3><a href="/hotel/9031088.html?iMddid=10819" class="_j_hotel_info_link" target="_blank"
                                           title="台北德立庄酒店 Hotel Midtown Richardson" data-cs-p="标题">${hotel.name}</a></h3>
                                    <a href="/hotel/activity/pigeon/index" target="_blank"><i class="icon-bird"></i></a>
                                    <br>
                                    <span>${hotel.englishName!}</span>
                                </div>
                            </div>
                            <div class="hotel-info ">
                                <ul class="nums clearfix">
                                    <li class="rating rating2">
                                        <em>${hotel.score!}</em>分
                                        <br><strong>${hotel.mark!}</strong>
                                    </li>
                                    <li><a href="/hotel/9031088.html?iMddid=10819#anchor=comment" class="_j_hotel_info_link"
                                           target="_blank"><em>1633条(模拟数据)</em><br>螺窝窝评价</a></li>
                                    <li class="split"></li>
                                    <li><a href="/hotel/9031088.html?iMddid=10819#anchor=comment" class="_j_hotel_info_link"
                                           target="_blank"><em>38篇(模拟数据)</em><br>游记提及</a></li>
                                </ul>
                                <p class="summary" title="">
                                    ${hotel.summary!}</p>
                                <div class="location">
                                    <span title="秀山街4号"><i class="icon-location"></i>${hotel.address}</span>
                                </div>

                            </div>
                            <div class="hotel-btns">
                                <a class="btn-booking _j_booking_btn" href="javascript:" rel="nofollow" style=""
                                   data-ota="booking"
                                   data-url="/hotel/booking/go2booking.php?from=hotel_list_new&amp;mddid=10819&amp;poi_id=9031088&amp;to=booking&amp;j=https%3A%2F%2Fwww.booking.com%2Fhotel%2Ftw%2Frichardson.zh-cn.html%3Fno_rooms%3D1%26group_adults%3D2%26group_children%3D0%26label%3Dhotel-1324124_nrm-01_gstadt-02_gstkid-00_logid-%7Bbooking_log_id%7D"
                                   data-price="762" data-is-cache-price="1" data-is-sold-out="0" data-pay-type="">
                                    <div class="ota">
                                        <div class="name">
                                            <strong><img
                                                    src=${hotel.coverUrl!}
                                                    height="20" width="100"></strong>


                                        </div>
                                        <p class="tips" style="display:none;"></p>
                                    </div>
                                    <div class="price _j_booking_price">
                                       <#-- <strong>￥</strong><strong>762</strong><strong
                                            style="font-size: 12px;color: #666;padding-left: 2px;vertical-align: 1px;">起</strong>-->
                                           <strong>价格:￥</strong>
                                           <span class="hotel_price" data-peoplenum="${qo.num!}" data-priceforone="${hotel.priceForOne!}">
                                           ${(qo.num * hotel.priceForOne * qo.days)}
                                        </span>
                                        <i class="arrow"></i>
                                    </div>
                                    <div class="price _j_booking_sold_out" style="display:none;">
                                        <span>已售罄</span>
                                    </div>

                                </a>
                                <#--<a class="btn-booking _j_booking_btn" href="javascript:" rel="nofollow" style=""
                                   data-ota="youyu_pkg"
                                   data-url="/hotel/booking/go2booking.php?from=hotel_list_new&amp;mddid=10819&amp;poi_id=9031088&amp;to=youyu_pkg&amp;j=http%3A%2F%2Fwww.mafengwo.cn%2Fhotel_zx%2Fhotel%2Findex.php%3FiId%3D1259624%26sRoom%3D2"
                                   data-price="707" data-is-cache-price="0" data-is-sold-out="0" data-pay-type="0"
                                   data-ota-id="14">
                                    <div class="ota">
                                        <div class="name">
                                            <strong><img
                                                    src="http://images.mafengwo.net/images/hotel/newlogo/mafengwo_2018@2x.png"
                                                    height="20" width="100"></strong>

                                            <i class="icon-alipay" style=""></i>
                                            <i class="icon-wxpay" style=""></i>


                                        </div>
                                        <p class="tips" style="display:none;"></p>
                                    </div>
                                    <div class="price _j_booking_price">
                                        <strong>价格:￥</strong>
                                        <span class="hotel_price" data-peoplenum="${qo.num!}" data-priceforone="${hotel.priceForOne!}">
                                            ${(qo.num * hotel.priceForOne)}
                                        </span>

                                        <strong style="font-size: 12px;color: #666;padding-left: 2px;vertical-align: 1px;">起</strong>
                                        <i class="arrow"></i>
                                    </div>
                                    <div class="price _j_booking_sold_out" style="display:none;">
                                        <span>已售罄</span>
                                    </div>

                                </a>-->
                                <a class="btn-booking _j_booking_btn" href="javascript:" rel="nofollow" style=""
                                   data-ota="hotels"
                                   data-url="/hotel/booking/go2booking.php?from=hotel_list_new&amp;mddid=10819&amp;poi_id=9031088&amp;to=hotels&amp;j=http%3A%2F%2Fwww.hotels.cn%2FPPCHotelDetails%3Fpos%3DHCOM_CN%26locale%3Dzh_CN%26hotelid%3D537274%26numberOfRooms%3D1%26cur%3DCNY%26childrenPerRoom%3D0%26childAgesPerRoom%3D0%26adultsPerRoom%3D2"
                                   data-price="988" data-is-cache-price="1" data-is-sold-out="0" data-pay-type="">
                                    <div class="ota">
                                        <div class="name">
                                            <strong>Hotels.com</strong>
                                        </div>
                                        <p class="tips" style="display:none;"></p>
                                    </div>
                                    <div class="price _j_booking_price">
                                        <#--<strong>￥</strong><strong>988</strong><strong
                                            style="font-size: 12px;color: #666;padding-left: 2px;vertical-align: 1px;">起</strong>-->
                                            <strong>价格:￥</strong>
                                            <span class="hotel_price" data-peoplenum="${qo.num!}" data-priceforone="${hotel.priceForOne!}">
                                            ${(qo.num * hotel.priceForOne * qo.days * 0.9)}
                                        </span>
                                        <i class="arrow"></i>
                                    </div>
                                    <div class="price _j_booking_sold_out" style="display:none;">
                                        <span>已售罄</span>
                                    </div>
                                </a>
                                <a class="btn-booking _j_booking_btn" href="javascript:" rel="nofollow" style=""
                                   data-ota="agoda"
                                   data-url="/hotel/booking/go2booking.php?adults_num=2&amp;children_num=0&amp;children_age=0&amp;from=hotel_list_new&amp;mddid=10819&amp;poi_id=9031088&amp;to=agoda&amp;j=http%3A%2F%2Fwww.agoda.com%2Fzh-cn%2Fasia%2Ftaiwan%2Ftaipei%2Fhotel_midtown_richardson.html"
                                   data-price="748" data-is-cache-price="1" data-is-sold-out="0" data-pay-type="">
                                    <div class="ota">
                                        <div class="name">
                                            <strong>agoda</strong>
                                        </div>
                                        <p class="tips" style="display:none;"></p>
                                    </div>
                                    <div class="price _j_booking_price">
                                        <#--<strong>￥</strong><strong>748</strong><strong
                                            style="font-size: 12px;color: #666;padding-left: 2px;vertical-align: 1px;">起</strong>-->
                                            <strong>价格:￥</strong>
                                            <span class="hotel_price" data-peoplenum="${qo.num!}" data-priceforone="${hotel.priceForOne!}">
                                            ${(qo.num * hotel.priceForOne * qo.days * 0.8)}
                                        </span>
                                        <i class="arrow"></i>
                                    </div>
                                    <div class="price _j_booking_sold_out" style="display:none;">
                                        <span>已售罄</span>
                                    </div>
                                </a>

                            </div>
                            <div class="hotel-other">
                                <div class="collect">
                                    <a class="btn-addCollect _j_add_fav" href="javascript:;" data-id="9031088" ><i></i>
                                        预订
                                    </a>
                                </div>
                            </div>
                        </div>
                    </#list>
                </#if>

            </div>
            <script language="javascript" src="http://js.mafengwo.net/js/hotel/sign/index.js?1552035728"
                    type="text/javascript"
                    crossorigin="anonymous"></script>


            <link href="http://css.mafengwo.net/css/hotel/captcha.css?1552035728" rel="stylesheet" type="text/css">

            <link href="http://css.mafengwo.net/css/hotel/dialog.css?1552035728" rel="stylesheet" type="text/css">


            <script language="javascript" src="http://js.mafengwo.net/js/corelib/underscore-1.6.0.js?1537192880"
                    type="text/javascript" crossorigin="anonymous"></script>

            <script language="javascript"
                    src="http://js.mafengwo.net/js/cv/js+underscore1.3.3:js+corelib+backbone-1.1.2^Z1E^1537192880.js"
                    type="text/javascript" crossorigin="anonymous"></script>

            <script language="javascript" src="http://js.mafengwo.net/js/jquery-ui-1.11.0.min.js?1537192880"
                    type="text/javascript" crossorigin="anonymous"></script>

            <script language="javascript" src="http://js.mafengwo.net/js/MouseTip.js?1537192880" type="text/javascript"
                    crossorigin="anonymous"></script>
            <link href="http://css.mafengwo.net/css/mfw-footer.css?1558532347" rel="stylesheet" type="text/css">
            <style>
                #banner-con-gloable {
                    display: block;
                    position: fixed;
                    bottom: 0;
                    left: -100%;
                    z-index: 110;
                    width: 100%;
                    height: 179px;
                    overflow-x: hidden;
                }

                #banner-con-gloable .banner-btn-con {
                    width: 100%;
                    height: 162px;
                    background: rgba(30, 15, 8, 0.95);
                    position: absolute;
                    bottom: 0;
                }

                #banner-con-gloable .banner-btn-con .close-btn {
                    position: absolute;
                    right: 35px;
                    top: 24px;
                    z-index: 120;
                    height: 24px;
                    width: 24px;
                    cursor: pointer;
                }

                #banner-con-gloable .banner-image-con {
                    position: absolute;
                    right: 50%;
                    bottom: 0;
                    width: 1000px;
                    margin-right: -500px;
                }

                #float-pannel-gloable {
                    padding-left: 28px;
                    padding-bottom: 20px;
                    display: block;
                    position: fixed;
                    bottom: 0;
                    z-index: 110;
                    left: -230px;
                }

                #float-pannel-gloable .float-btn {
                    width: 24 pxpx;
                    height: 24px;
                    position: absolute;
                    right: 0;
                    top: 0;
                    z-index: 100;
                }

                #closed {
                    height: 24px;
                    width: 24px;
                    vertical-algin: top;
                    border: none;
                    cursor: pointer;
                }
            </style>

            <div id="float-pannel-gloable">
                <img class="float-image"
                     src="https://p4-q.mafengwo.net/s14/M00/BB/8C/wKgE2l0r2W-ALHaeAACAt6lqXyA464.png"
                     style="width:178px;">
                <div class="float-btn">
                    <img id="closed" src="https://n4-q.mafengwo.net/s13/M00/46/AC/wKgEaVy2xHeAZJhRAAADGY-wozY871.png">
                </div>
            </div>
            <script>
                $(function () {

                    var flag_page = location.href.match(/^(https|http):\/\/www\.mafengwo\.cn\/?$/g) ||
                            location.href.match(/^(https|http):\/\/www\.mafengwo\.cn(\/\?\S*)/g) ||
                            location.href.match(/(cn\/gonglve|cn\/yj\/|cn\/i\/|cn\/jd\/|cn\/xc\/|cn\/schedule\/|cn\/baike\/|cn\/poi\/|mdd|travel-scenic-spot)/g)

                    if (!!flag_page && getCookie('ad_hide') != '1' && getCookie('ad_close_num') < 2) {
                        handleBannerShow();
                    }

                });

                // 浮标关闭按钮点击
                $("#float-pannel-gloable .float-btn").click(function () {
                    $("#float-pannel-gloable").animate({left: -230,}, 800, 'swing');
                    add_cookie('ad_hide', '1', 1);
                    if (!getCookie('ad_close_num')) {
                        add_cookie('ad_close_num', 1, 9999)
                    } else if (!!getCookie('ad_close_num')) {
                        add_cookie('ad_close_num', getCookie('ad_close_num') * 1 + 1, 9999)
                    }
                });

                function add_cookie(name, value, n) {
                    var exp = new Date();
                    exp.setTime(exp.getTime() + 24 * 60 * 1000 * 60 * n);
                    document.cookie = name + '=' + value + ';expires=' + exp.toGMTString() + ';path=/';
                }

                function getCookie(cname) {
                    var name = cname + "=";
                    var ca = document.cookie.split(';');

                    for (var i = 0; i < ca.length; i++) {
                        var c = ca[i];
                        while (c.charAt(0) == ' ') c = c.substring(1);
                        if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
                    }

                    return "";
                }

                function delCookie(name) {
                    var exp = new Date();
                    exp.setTime(exp.getTime() - 1);
                    var cval = getCookie(name);
                    if (cval != null)
                        document.cookie = name + "=" + cval + ";expires=" + exp.toGMTString();
                }

                function handleBannerShow() {
                    $("#float-pannel-gloable").animate({left: 0,}, 500, 'swing');
                }


                $(".sales-sort").click(function (data) {

                })


                })

            </script>
            <link href="http://css.mafengwo.net/css/mfw-toolbar.css?1537192876" rel="stylesheet" type="text/css">
        </div>
</body>
</html>