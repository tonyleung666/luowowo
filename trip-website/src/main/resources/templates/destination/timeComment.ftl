<div class="tiles">
    <#if vos?? >
    <#list vos as vo>

            <div class="item col3">
                <a href="/destination/guide?id=${vo.destId}" data-id=${vo.destId} target="_blank"><img src=${vo.coverUrl!} width="323" height="220">
                    <div class="title">${vo.destName!}</div>
                </a>
            </div>

    </#list>
    </#if>
</div>
