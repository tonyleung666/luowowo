<div class="tiles smallTitleList">
<#if smallTitleList?? >
    <#list smallTitleList as s>
        <div class="item col4">
            <a href="/destinationscreen/destscreen?themeId=${s.id!}" target="_blank"><img src=${s.coverUrl!} width="238" height="220">
                <div class="title">${s.smallTitle!}</div>
            </a>
        </div>
    </#list>
</#if>
</div>


