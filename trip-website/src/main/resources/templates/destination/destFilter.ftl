<!DOCTYPE html>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>

    <link href="/styles/base.css" rel="stylesheet" type="text/css">
    <link href="/styles/destination.css" rel="stylesheet" type="text/css">
    <link href="/styles/travelspot.css" rel="stylesheet" type="text/css">
    <link href="/styles/select.css" rel="stylesheet" type="text/css">
    <link href="/js/plugins/jqPaginator/jqPagination.css" rel="stylesheet" type="text/css">
    <link href="/styles/destfilter.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="/js/jquery/jquery.js"></script>

    <script type="text/javascript" src="/js/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/js/plugins/jqPaginator/jq-paginator.min.js"></script>>
    <script type="text/javascript" src="/js/plugins/jquery-form/jquery.form.js"></script>
    <script type="text/javascript" src="/js/system/guide.js"></script>
    <script type="text/javascript" src="/js/destscreenjs/common.js"></script>
    <script type="text/javascript" src="/js/system/destionation.js"></script>
</head>
<script>
    $(function () {
        var timeId = -1;
        var themeId = -1;
        var dayId = -1;
        var monthNum = -1;
        $('.timemonth a').click(function () {
            $('.timemonth a').removeClass('on');
            $('.time a').removeClass('on');
            $(this).addClass("on");
            monthNum = $(this).data("id");
            timeId = -1;
            /*if(timeId!=-1){
                $('.time a:first-child').removeClass('on');
            }*/
            $.get('/destinationscreen/filterCommend',{themeId:themeId,dayId:dayId,monthNum:monthNum},function (data) {
                $('.row-list').empty();
                $('.row-list').html(data);
            })
        })
        $('.time a').click(function () {
            $('.timemonth a').removeClass('on');
            $('.time a').removeClass('on');
            $(this).addClass("on");
            timeId = $(this).data("id");
            /*if(timeId!=-1){
                $('.time a:first-child').removeClass('on');
            }*/
            $.get('/destinationscreen/filterCommend',{timeId:timeId,themeId:themeId,dayId:dayId},function (data) {
                $('.row-list').empty();
                $('.row-list').html(data);
            })
        })
        $('.theme a').click(function () {
            themeId = $(this).data("id");
            /*if(themeId!=-1){
                $('.theme a:first-child').removeClass('on');
            }*/
            //$('.themes a').removeClass('on');
            $('.theme a').removeClass('on');
            $(this).addClass("on");
            $.get('/destinationscreen/filterCommend',{themeId:themeId,timeId:timeId,dayId:dayId,monthNum:monthNum},function (data) {
                $('.row-list').empty();
                $('.row-list').html(data);
            })
        })
        $('.day a').click(function () {
            $(this).siblings().removeClass("on");
            $(this).addClass("on");
            dayId = $(this).data("id");
            console.log(themeId);
            /*if(dayId!=-1){
                $('.day a:first-child').removeClass('on');
            }*/
            $.get('/destinationscreen/filterCommend',{timeId:timeId,themeId:themeId,dayId:dayId,monthNum:monthNum},function (data) {
                $('.row-list').empty();
                $('.row-list').html(data);
            })
        })
        var destName = '${qo.destName!}'
        console.log(destName);
        if (destName == '1') {
            console.log(2);
            $.get('/destinationscreen/filterCommend',{timeId:${qo.timeId},themeId:${qo.themeId}},function (data) {
                $('.row-list').empty();
                if(${qo.timeId}!=-1){
                    $('.time a').removeClass('on');
                    timeId = ${qo.timeId};
                    $('.time [data-id=${qo.timeId}]').addClass('on');
                    console.log(${qo.timeId})
                }
                if(${qo.themeId}!=-1){
                    $('.theme a').removeClass('on');
                    $('.timemonth [data-id="-1"]').addClass('on');
                    //themeId = $(this).data("id");
                    themeId = ${qo.themeId};
                    $('.theme [data-id="${qo.themeId}"]').addClass('on');
                    if($('.theme_time [data-id="${qo.themeId}"]').data('id') != undefined ){
                        //console.log($('.theme_time [data-id="${qo.themeId}"]'))
                        $('.timemonth [data-id="-1"]').removeClass('on');
                        $('.theme [data-id="-1"]').addClass('on');
                        $('.theme_time [data-id="${qo.themeId}"]').addClass('on');
                        timeId = ${qo.themeId};
                        themeId = -1;
                    }
                }
                $('.row-list').html(data);
            })
        }else {
            console.log(2);
            $.get('/destinationscreen/filterCommend',{destName:'${qo.destName}'},function (data) {
                $('.row-list').empty();

                $('.row-list').html(data);
            })
        }

    })

</script>
<body>
<div class="lww_header">
    <div class="header_wrap">
        <div class="header_logo">
            <a href="javascript:;" class="lww_logo"></a>
        </div>
        <ul class="header_nav">
            <li><a href="./index.html">首页</a></li>
            <li class="header_nav_active"><a href="./destination.html">目的地</a></li>
            <li><a href="./gonglve.html">旅游攻略</a></li>
            <li><a href="javascript:;">去旅行<i class="icon_caret_down"></i></a></li>
            <li><a href="javascript:;">机票</a></li>
            <li><a href="/checkhotel">酒店</a></li>
            <li><a href="javascript:;">社区<i class="icon_caret_down"></i></a></li>
        </ul>
        <div class="header_search">
            <input type="text" />
            <a class="icon_search"></a>
        </div>
        <div class="login_info">
            <div class="head_user">
                <a href="javascript:;">

                    <i class="icon_caret_down"></i>
                </a>
            </div>
            <div class="header_msg">
                消息<i class="icon_caret_down"></i>
            </div>
            <div class="header_daka">
                <a href="javascript:;">打卡</a>
            </div>
        </div>
    </div>
    <div class="shadow"></div>
</div>
<div class="container">
    <div class="row-top">
        <div class="wrapper">
            <div class="top-bar">
                <div class="crumb">
                    <div class="item"><a href="javascript:;" target="_blank">目的地</a><em>&gt;</em></div>
                    <div class="item cur">目的地筛选</div>
                </div>
            </div>
            <div class="filter-title">目的地筛选</div>
            <div class="filter-nav ">
                <dl class="clearfix ">
                    <dt>时间</dt>
                    <dd class="J_dd ">
                        <div class="month clearfix timemonth">
                            <a class="on" href="javascript:void(0)" data-id="-1">不限</a>
                            <a href="javascript:void(0)" data-id="1">1月</a>
                            <a href="javascript:void(0)" data-id="2">2月</a>
                            <a href="javascript:void(0)" data-id="3">3月</a>
                            <a href="javascript:void(0)" data-id="4">4月</a>
                            <a href="javascript:void(0)" data-id="5">5月</a>
                            <a href="javascript:void(0)" data-id="6">6月</a>
                            <a href="javascript:void(0)" data-id="7">7月</a>
                            <a href="javascript:void(0)" data-id="8">8月</a>
                            <a href="javascript:void(0)" data-id="9">9月</a>
                            <a href="javascript:void(0)" data-id="10">10月</a>
                            <a href="javascript:void(0)" data-id="11">11月</a>
                            <a href="javascript:void(0)" data-id="12">12月</a>
                        </div>
                        <div class="festival clearfix time theme_time">
                                <#list dateList as date>
                                    <a href="javascript:void(0)" data-id=${date.id}>${date.smallTitle}</a>
                                </#list>
                        </div>
                    </dd>
                </dl>
                <dl class="clearfix theme">
                    <dt>主题</dt>
                    <dd class="J_dd ">
                        <a class="on" class="" href="javascript:void(0)" data-id="-1">不限</a>
                        <div class="sub-nav themes">
                            <dl class="clearfix">
                                <dt>全年适宜：</dt>
                                <dd>
                                <#--<a href="javascript:void(0)" data-id="167" class="on">免签</a>-->
                                        <#list yearList as year>
                                            <a href="javascript:void(0)" data-id=${year.id}>${year.smallTitle}</a>
                                        </#list>
                                </dd>
                            </dl>
                            <dl class="clearfix">
                                <dt>季节：</dt>
                                <dd>
                                    <#list seasonList as season>
                                        <a href="javascript:void(0)" data-id=${season.id}>${season.smallTitle}</a>
                                    </#list>
                                </dd>
                            </dl>
                            <dl class="clearfix">
                                <dt>出行方式：</dt>
                                <dd>

                                    <#list goList as go>
                                        <a href="javascript:void(0)" data-id=${go.id}>${go.smallTitle}</a>
                                    </#list>
                                </dd>
                            </dl>
                        </div>
                    </dd>
                </dl>
                <dl class="clearfix day">
                    <dt>天数</dt>
                    <dd class="J_dd ">
                        <a class="on" href="javascript:void(0)" data-id="-1">不限</a>
                        <a href="javascript:void(0)" data-id="31">2-3天</a>
                        <a href="javascript:void(0)" data-id="32">4-5天</a>
                        <a href="javascript:void(0)" data-id="33">6-9天</a>
                        <a href="javascript:void(0)" data-id="34">10天及以上</a>
                    </dd>
                </dl>
            </div>
        </div>
    </div>
    <div class="row-list">

    </div>
</div>
<div id="footer">
    <div class="ft-content" style="width: 1105px">
        <div class="ft-info clearfix">
            <dl class="ft-info-col ft-info-intro">
                <dt>马蜂窝旅游网</dt>
                <dd>叩丁狼是一家专注于培养高级IT技术人才，为学员提供定制化IT职业规划方案及</dd>
                <dd>意见咨询服务的教育科技公司，为您提供海量优质课程，以及创新的线上线下学</dd>
                <dd>习体验，帮助您获得全新的个人发展和能力提升。</dd>
            </dl>
            <dl class="ft-info-col ft-info-qrcode">
                <dd>
                    <span class="ft-qrcode-tejia"></span>
                </dd>
                <dd>
                    <span class="ft-qrcode-weixin"></span>
                </dd>
                <dd>
                    <span class="ft-qrcode-weixin" style="background-image: url('https://p3-q.mafengwo.net/s10/M00/48/A9/wKgBZ1t_4sSAVJ6uAAAlzJ0PZgU881.png?imageMogr2%2Fthumbnail%2F%2194x90r%2Fgravity%2FCenter%2Fcrop%2F%2194x90%2Fquality%2F90')"></span>
                </dd>
            </dl>
            <dl class="ft-info-social">
                <dt>向崇尚自由的加勒比海盗致敬！</dt>
                <dd>
                    <a class="ft-social-weibo" target="_blank" href="javascript:;" rel="nofollow"><i
                            class="ft-social-icon"></i></a>
                    <a class="ft-social-qqt" target="_blank" href="javascript:;" rel="nofollow"><i
                            class="ft-social-icon"></i></a>
                    <a class="ft-social-qzone" target="_blank" href="javascript:;" rel="nofollow"><i
                            class="ft-social-icon"></i></a>
                </dd>
            </dl>
        </div>

        <div class="ft-links">
            <a target="_blank" href="http://china.makepolo.com/">马可波罗</a><a target="_blank" href="http://www.onlylady.com/">Onlylady女人志</a><a target="_blank" href="http://trip.elong.com/">艺龙旅游指南</a><a target="_blank" href="http://www.cncn.com">欣欣旅游网</a>
            <a target="_blank" href="http://www.8264.com/">户外运动</a><a target="_blank" href="http://www.yue365.com/">365音乐网</a><a target="_blank" href="http://ishare.iask.sina.com.cn/">爱问共享资料</a><a target="_blank" href="http://www.uzai.com/">旅游网</a>
            <a target="_blank" href="http://www.zongheng.com/">小说网</a>
            <a target="_blank" href="http://www.xuexila.com/">学习啦</a><a target="_blank" href="http://www.yododo.com">游多多自助游</a><a target="_blank" href="http://www.zhcpic.com/">问答</a><a target="_blank" href="http://huoche.mafengwo.cn/">火车时刻表</a>
            <a target="_blank" href="http://www.lvmama.com">驴妈妈旅游网</a>
            <a target="_blank" href="http://www.haodou.com/">好豆美食网</a><a target="_blank" href="http://www.taoche.com/">二手车</a><a target="_blank" href="http://www.lvye.cn">绿野户外</a><a target="_blank" href="http://www.tuniu.com/">途牛旅游网</a>
            <a target="_blank" href="http://www.mapbar.com/">图吧</a>
            <a target="_blank" href="http://www.chnsuv.com">SUV联合越野</a><a target="_blank" href="http://www.uc.cn/">手机浏览器</a><a target="_blank" href="http://sh.city8.com/">上海地图</a><a target="_blank" href="http://www.tianqi.com/">天气预报查询</a>
            <a target="_blank" href="http://www.ly.com/">同程旅游</a>
            <a target="_blank" href="http://www.tieyou.com/">火车票</a><a target="_blank" href="http://www.yunos.com/">YunOS</a><a target="_blank" href="http://you.ctrip.com/">携程旅游</a><a target="_blank" href="http://www.jinjiang.com">锦江旅游</a>
            <a target="_blank" href="http://www.huoche.net/">火车时刻表</a>
            <a target="_blank" href="http://www.tripadvisor.cn/">TripAdvisor</a><a target="_blank" href="http://www.tianxun.com/">天巡网</a><a target="_blank" href="http://www.mayi.com/">短租房</a><a target="_blank" href="http://www.zuzuche.com">租租车</a>
            <a target="_blank" href="http://www.5fen.com/">五分旅游网</a>
            <a target="_blank" href="http://www.zhuna.cn/">酒店预订</a><a target="_blank" href="http://www.ailvxing.com">爱旅行网</a><a target="_blank" href="http://360.mafengwo.cn/all.php">旅游</a><a target="_blank" href="http://vacations.ctrip.com/">旅游网</a>
            <a target="_blank" href="http://www.wed114.cn">wed114结婚网</a>
            <a target="_blank" href="http://www.chexun.com/">车讯网</a><a target="_blank" href="http://www.aoyou.com/">遨游旅游网</a><a target="_blank" href="http://www.91.com/">手机</a>
            <a href="javascript:;" target="_blank">更多友情链接&gt;&gt;</a>
        </div>
    </div>
</div>
<div class="mfw-toolbar" id="_j_mfwtoolbar" style="display: block;">
    <div class="toolbar-item-top" style="display: none;">
        <a role="button" class="btn _j_gotop">
            <i class="icon_top"></i>
            <em>返回顶部</em>
        </a>
    </div>
    <div class="toolbar-item-feedback">
        <a role="button" data-japp="feedback" class="btn">
            <i class="icon_feedback"></i>
            <em>意见反馈</em>
        </a>
    </div>
    <div class="toolbar-item-code">
        <a role="button" class="btn">
            <i class="icon_code"></i>
        </a>
        <a role="button" class="mfw-code _j_code">


            <img src="https://p1-q.mafengwo.net/s1/M00/6C/51/wKgIC1t_6TuASybrAADGUPUHjr021.jpeg?imageMogr2%2Fthumbnail%2F%21450x192r%2Fgravity%2FCenter%2Fcrop%2F%21450x192%2Fquality%2F90"
                 width="450" height="192">
        </a>
        <!--<div class="wx-official-pop"><img src="http://images.mafengwo.net/images/qrcode-weixin.gif"><i class="_j_closeqrcode"></i></div>-->
    </div>

</div>
</body>

</html>