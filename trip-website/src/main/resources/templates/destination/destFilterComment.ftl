<div class="wrapper tiles" >
    <#--<ul class="clearfix">-->
        <#--<li class="item">-->
        <#if destList??>
            <#list destList as dest>
                <div class=" col1">
                    <div class="img ">
                    <a href="/destination/guide?id=${dest.destId!}" target="_blank"><img height="300" width="1000" src=${dest.coverUrl!} style="display: inline;">
                        <div class="title">${dest.destName}</div>
                    </a>
                    </div>
                </div>
                <tr>
                </tr>
                <div class="info">
                    <p class="detail">${dest.info!}</p>
                    <div class="hot">
                        <span class="label">TOP3</span>
                        <#if dest.poiList??>
                            <#list dest.poiList as poi >
                                <a href="/poi/detail/${poi.id}" target="_blank">${poi.name}</a><span class="divide"></span>
                            </#list>
                        </#if>
                    </div>
                    <div class="line"><a href="javascript:;" target="_blank"><em>1</em>${dest.title!}</a></div>
                </div>
            </#list>
        </#if>
    <#--   </li>-->
   </ul>
</div>