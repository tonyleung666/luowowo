<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>骡窝窝国内机票预订 - 骡窝窝</title>
        <meta name="renderer" content="webkit">
        <link href="/styles/base.css" rel="stylesheet" type="text/css">
        <link href="/styles/index.css" rel="stylesheet" type="text/css">
        <link href="/js/plugins/jqPaginator/jqPagination.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/js/jquery/jquery.js"></script>
        <script type="text/javascript" src="/js/plugins/jquery-form/jquery.form.js"></script>
        <script type="text/javascript" src="/js/plugins/jqPaginator/jq-paginator.min.js"></script>
        <script type="text/javascript" src="/js/system/index.js"></script>
        <script type="text/javascript" src="/js/system/common.js"></script>
        <link href="http://css.mafengwo.net/flight/css/flightpc/index.ad132c05.css?1574230409" rel="stylesheet" type="text/css">

        <script>
            $(function () {

                /*订票机票点击事件*/
                $("#btn_search").on("click", function () {

                });
                $('#orderForm').ajaxForm(function (data) {
                    if (data.success){
                        popup(data.msg);
//                        var r = confirm("订购成功!");
//                        if (r == true) {
                        window.location.href='/flight/index';
//                        }
                    } else {
                        popup(data.msg);
                    }
                });

            })

        </script>

    </head>
    <body style="position: relative;">

        <#assign currentNav="">
        <#include "../common/navbar.ftl">
        <link rel="stylesheet" href="https://wpstatic.mafengwo.net/mtraffic/static/css/swiper-4.4.2.min.css">
        <div id="app">
            <div class="v-flightpc-container v-book clearfix">
                <div class="fl main">
                    <form action="/flight/deal" method="post" id="orderForm">
                        <div class="cont">
                            <div class="sec-title">
                                <h3 class="text">乘机人</h3>
                            </div>
                            <div class="psger-add">
                                <ul class="list">
                                    <li class="item">
                                        <div class="wrap none">
                                            <div class="people-inputbox">
                                                <input type="text" id="passenger" name="passenger" placeholder="请输入乘机人姓名"
                                                       class="input" style="width: 452px;">
                                                <span class="type-name">成人票</span>
                                                <div class="info">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="wrap none">
                                            <div class="people-inputbox">
                                                <input type="text" id="idcardNo" name="idcardNo" placeholder="请输入乘机人身份证"
                                                       class="input" style="width: 452px;">
                                                <div class="info">
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <#--<span class="btnbox">-->
                                    <#--<i class="plus">&nbsp;</i>-->
                                    <#--<span class="add">添加乘机人</span>-->
                                <#--</span>-->
                            </div>
                        </div>

                        <div class="cont">
                            <div class="sec-title">
                                <h3 class="text">联系人</h3>
                            </div>
                            <div class="psger-add">
                                <ul class="list">
                                    <li class="item none">
                                        <div class="wrap none">
                                            <div class="inputbox">
                                                <input type="text" name="linkman" id="linkman"
                                                       placeholder="请填写联系人姓名" class="input buyer-name">
                                            </div>
                                        </div>
                                        <div class="wrap">
                                            <div class="inputbox">
                                                <input type="text" name="linktel" id="linktel"
                                                       placeholder="请填写联系人电话" class="input buyer-email">
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <input type="hidden" name="flightInfo.id" value="${info.id}">
                        <button type="submit" class="submit">提交订单</button>
                    </form>
                </div>
                <div class="fr aside">
                    <div class="trips">
                        <div class="trip-item none">
                            <div class="trip-title none">
                                <span class="trip-name">${info.depPort.dest.name} - ${info.arrPort.dest.name}</span>
                                <span class="trip-date">${info.depTime?string('MM月dd日')}</span>
                            </div>
                            <div class="trip-detail">
                                <div class="trip-left">
                                    <p class="trip-time">${info.depTime?string('HH:mm')}</p>
                                    <p class="trip-airport">${info.depPort.name} </p>
                                </div>
                                <div class="trip-mid">
                                    <p class="trip-hour">${interval}</p>
                                    <span class="trip-line"><i class="trip-arrow"></i></span>
                                    <p class="trip-info">${info.airline!} ${info.flightNo!} <span>| 经济舱</span></p>
                                </div>
                                <div class="trip-right">
                                    <p class="trip-time">${info.arrTime?string('HH:mm')}</p>
                                    <p class="trip-airport">${info.arrPort.name} </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="prices">
                        <div class="price-item">
                            <div class="price-wrap clearfix">
                                <span class="price-type">机票 成人</span>
                                <span class="price-info">
                                    <span class="price-num">¥${info.price-50}</span> x 1</span>
                            </div>
                            <div class="price-wrap clearfix">
                                <span class="price-type">机建燃油 成人</span>
                                <span class="price-info"><span class="price-num">¥50</span> x 1</span>
                            </div>
                        </div>
                    </div>
                    <div class="total-price">
                        <span class="total-title">应付总额</span>
                        <span class="total-num">${info.price!}</span>
                        <span class="total-type">¥</span>
                    </div>
                    <div class="tgq">退改签行李额规则及供应商信息</div>
                </div>
                <div class="v-dialog" style="display: none;">
                    <div class="v-mask-loading">
                        <div class="v-mask-loading-icon"></div>
                    </div>
                </div>
            </div>
        </div>

        <#include "../common/footer.ftl">
    </body>
</html>