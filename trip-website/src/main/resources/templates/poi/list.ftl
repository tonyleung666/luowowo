<!DOCTYPE html>
<html>

<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link href="/styles/base.css" rel="stylesheet" type="text/css">
        <link href="/styles/travelguide.css" rel="stylesheet" type="text/css">
        <link href="/styles/reply.css" rel="stylesheet" type="text/css">
        <link href="/js/plugins/jqPaginator/jqPagination.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/js/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="/js/system/travelguide.js"></script>
        <script type="text/javascript" src="/js/plugins/jqPaginator/jq-paginator.min.js"></script>
        <script type="text/javascript" src="/js/travelfilter.js"></script>
    </head>

<body>
    <#assign currentNav="destination">
    <#include "../common/navbar.ftl">

	<div class="row row-placeTop" data-cs-p="面包屑">
    <div class="wrapper">
        <link href="http://css.mafengwo.net/css/cv/css+mdd+place-crumb:css+mdd+place-navbar^Z1U^1559788120.css"
              rel="stylesheet" type="text/css">
        <script language="javascript" src="http://js.mafengwo.net/js/hotel/sign/index.js?1552035728"
                type="text/javascript" crossorigin="anonymous"></script>
        <link href="http://css.mafengwo.net/css/mdd/place-crumb.css?1530619858" rel="stylesheet" type="text/css">

        <div class="crumb">
            <div class="item"><a href="/destination">目的地</a><em>&gt;</em></div>
        <#list toasts as t>
            <div class="item">
                <div class="drop">
                    <span class="hd"><a href="javascript:;" target="_blank">${(t.name)!}<i></i></a></span>
                    <div class="bd">
                        <i class="arrow"><b></b></i>
                        <div class="col">
                            <h3>热门地区</h3>
                            <ul class="clearfix">
                                <#list t.children as tc>
                                    <#if tc_index lt 5 >
                                        <li><a href="/destination/guide?id=${tc.id!}" target="_blank">${tc.name}</a> </li>
                                    </#if>
                                </#list>

                            </ul>
                        </div>
                        <div class="more"><a href="/destination/guide?id=${t.id}" target="_blank">&gt;&gt;更多地区</a></div>
                    </div>
                </div>
                <em>&gt;</em>
            </div>
        </#list>
            <div class="item">
                <div class="drop">
                    <span class="hd"><a href="javascript:;">${dest.name}<i></i></a></span>
                    <div class="bd">
                        <i class="arrow"><b></b></i>
                        <div class="col">
                            <!--h3>热门国家</h3-->
                            <ul class="clearfix">
                                <li><a href="/destination/surveyPage?destId=${dest.id}" target="_blank">${dest.name}概括</a></li>
                                <li><a href="#travel" target="_blank">${dest.name}游记</a></li>
                                <li><a href="#strategy" target="_blank">${dest.name}攻略</a></li>
                                <li><a href="javascript:;" target="_blank">${dest.name}跟团游</a></li>
                            </ul>
                        </div>
                        <!--div class="more"><a href="#">&gt;&gt;更多国家</a></div-->
                    </div>
                </div>
                <em>&gt;</em>
            </div>
            <div class="item cur"><strong>${dest.name}景点</strong></div>
        </div>

        <div class="place-navbar" id="_j_mdd_place_nav_bar_warper" style="border-top: 0;" data-cs-t="目的地导航">
            <div class="navbar-con">
                <ul class="navbar clearfix navbar-first-level-warper">
                    <li class="navbar-overview">
                        <a class="navbar-btn" href="/" data-cs-p="首页">
                            <i class="navbar-icon"></i><span>首页</span>

                        </a>
                    </li>
                    <li class="navbar-line">
                        <a class="navbar-btn" href="#" data-cs-p="行程线路">
                            <i class="navbar-icon"></i><span>行程线路</span>

                        </a>
                    </li>
                    <li class="navbar-scenic">
                        <a class="navbar-btn" href="#" data-cs-p="景点">
                            <i class="navbar-icon"></i><span>景点</span>

                        </a>
                    </li>
                    <li class="navbar-hotels">
                        <a class="navbar-btn" href="#" data-cs-p="酒店">
                            <i class="navbar-icon"></i><span>酒店</span>

                        </a>
                    </li>
                    <li class="navbar-flight">
                        <a class="navbar-btn" href="#" data-cs-p="机票">
                            <i class="navbar-icon"></i><span>机票</span>

                        </a>
                    </li>
                    
                </ul>
            </div>
        </div>
        <div id="fill_area" style="height: 75px; display: none;"></div>

    </div>
</div>



<div class="row row-summary row-bg">
    <div class="wrapper">
        <h2 class="title">景点概况</h2>
        <div>
            <p style="">
                <span>${(poi.info)!}</span>
            </p>
        </div>
    </div>
	
</div>
<br>
<br>







<div class="row row-top5" data-cs-p="必游景点">
    <div class="wrapper">
        <h2 class="title">必游景点TOP5</h2>
        <#if tops ??>
        <#list tops as p>
            <div class="item clearfix">
                <div class="info">
                        <div class="middle">
                            <h3>
                                <span class="num">${p_index + 1}</span>
                                <a href="/poi/detail?id=${p.id!}" target="_blank" title="${p.name!}">${p.name!}
                                </a><a href="/poi/detail?id=${p.id}" target="_blank" title="${p.name!}">
                                <span class="rev-total"><em>${p.replynum!}</em> 条点评</span>
                            </a>
                            </h3>
                            <p>${p.summary}</p>
                            <div class="links">这里还包含景点：
                                <#if p.children??>
                                    <#list p.children as child>
                                        <a href="/poi/detail?id=${child.id!}" target="_blank">${child.name}</a>
                                    </#list>
                                </#if>
                            </div>
                        </div>

                </div>
                <div class="pic">
                    <a href="/poi/detail?id=${p.id!}" target="_blank" title="${p.name}">
                        <div class="large">
                            <img src="${p.imgUrls[0]}"
                                 width="380" height="270">
                        </div>
                        <div>
                            <img src="${p.imgUrls[1]}" width="185" height="130">
                        </div>
                        <div>
                            <img src="${p.imgUrls[2]}" width="185" height="130">
                        </div>
                    </a>
                </div>
            </div>
        </#list>
        </#if>
    </div>
</div>
<Br>
<Br>



<div class="row row-hotScenic row-bg" data-cs-p="热门景点">
    <div class="wrapper">
        <h2 class="title">热门景点</h2>
        <div class="bd">
            <div class="grid grid-two">
                <div class="figure">
                    <a href="/poi/5178221.html" target="_blank" title="中山大学">
                        <img src="http://p3-q.mafengwo.net/s10/M00/73/65/wKgBZ1omodCAIS75ABc0yMLtG8E31.jpeg?imageMogr2%2Fthumbnail%2F%21485x320r%2Fgravity%2FCenter%2Fcrop%2F%21485x320%2Fquality%2F100"
                             width="485" height="320">
                        <h3 class="title">中山大学</h3>
                        <div class="mask-container">
                            <div class="mask">
                                <div class="middle">
                                    <h3>中山大学</h3>
                                    <p>中国南方科学研究与人才培养的重地</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
            <div class="grid">
                <div class="figure">
                    <a href="/poi/6047483.html" target="_blank" title="上下九步行街">
                        <img src="http://p1-q.mafengwo.net/s9/M00/3D/A3/wKgBs1eDNyCALt6LABceb7zfpl447.jpeg?imageMogr2%2Fthumbnail%2F%21242x155r%2Fgravity%2FCenter%2Fcrop%2F%21242x155%2Fquality%2F100"
                             width="242" height="155">
                        <h3 class="title">上下九步行街</h3>
                        <div class="mask-container">
                            <div class="mask">
                                <div class="middle">
                                    <h3>上下九步行街</h3>
                                    <p>中西合璧的西关风情特色，在此感受独特的岭南商业文化</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="figure">
                    <a href="/poi/1049.html" target="_blank" title="西汉南越王博物馆">
                        <img src="http://b3-q.mafengwo.net/s5/M00/0A/C4/wKgB3FCrTPqAOEwmAAMLfSUbUIE57.jpeg?imageMogr2%2Fthumbnail%2F%21242x155r%2Fgravity%2FCenter%2Fcrop%2F%21242x155%2Fquality%2F100"
                             width="242" height="155">
                        <h3 class="title">西汉南越王博物馆</h3>
                        <div class="mask-container">
                            <div class="mask">
                                <div class="middle">
                                    <h3>西汉南越王博物馆</h3>
                                    <p>岭南规模最大汉代彩绘墓室，各式出土文物</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
            <div class="grid">
                <div class="figure">
                    <a href="/poi/26336.html" target="_blank" title="岭南印象园">
                        <img src="http://b3-q.mafengwo.net/s5/M00/B5/37/wKgB3FEsZt6AaeAdAARfi3uw2CU10.jpeg?imageMogr2%2Fthumbnail%2F%21242x155r%2Fgravity%2FCenter%2Fcrop%2F%21242x155%2Fquality%2F100"
                             width="242" height="155">
                        <h3 class="title">岭南印象园</h3>
                        <div class="mask-container">
                            <div class="mask">
                                <div class="middle">
                                    <h3>岭南印象园</h3>
                                    <p>体验岭南乡土风情和民俗文化的好去处</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="figure">
                    <a href="/poi/928.html" target="_blank" title="红专厂创意园">
                        <img src="http://n3-q.mafengwo.net/s7/M00/C0/B2/wKgB6lR5l6mABglmAD5ONUIG7c885.jpeg?imageMogr2%2Fthumbnail%2F%21242x155r%2Fgravity%2FCenter%2Fcrop%2F%21242x155%2Fquality%2F100"
                             width="242" height="155">
                        <h3 class="title">红专厂创意园</h3>
                        <div class="mask-container">
                            <div class="mask">
                                <div class="middle">
                                    <h3>红专厂创意园</h3>
                                    <p>浓浓的怀旧风，文艺范十足</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
            <div class="grid">
                <div class="figure">
                    <a href="/poi/5428004.html" target="_blank" title="广州长隆旅游度假区">
                        <img src="http://p3-q.mafengwo.net/s12/M00/FA/86/wKgED1whybCAFHfjABn7foL1A6I47.jpeg?imageMogr2%2Fthumbnail%2F%21242x155r%2Fgravity%2FCenter%2Fcrop%2F%21242x155%2Fquality%2F100"
                             width="242" height="155">
                        <h3 class="title">广州长隆旅游度假区</h3>
                        <div class="mask-container">
                            <div class="mask">
                                <div class="middle">
                                    <h3>广州长隆旅游度假区</h3>
                                    <p>整个度假区很适合带孩子游玩，家庭周末出游的上佳选择</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="figure">
                    <a href="/poi/5423346.html" target="_blank" title="小洲村">
                        <img src="http://p1-q.mafengwo.net/s6/M00/FC/D6/wKgB4lLXeNGAB1d_AAPlw-QqOPM76.jpeg?imageMogr2%2Fthumbnail%2F%21242x155r%2Fgravity%2FCenter%2Fcrop%2F%21242x155%2Fquality%2F100"
                             width="242" height="155">
                        <h3 class="title">小洲村</h3>
                        <div class="mask-container">
                            <div class="mask">
                                <div class="middle">
                                    <h3>小洲村</h3>
                                    <p>保留岭南水乡小桥流水人家的生活情态。</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
            <div class="grid grid-two">
                <div class="figure">
                    <a href="/poi/28270.html" target="_blank" title="大夫山森林公园">
                        <img src="http://b4-q.mafengwo.net/s5/M00/61/64/wKgB3FHl6kKAG7AXAAWR6CvoUaY37.jpeg?imageMogr2%2Fthumbnail%2F%21485x320r%2Fgravity%2FCenter%2Fcrop%2F%21485x320%2Fquality%2F100"
                             width="485" height="320">
                        <h3 class="title">大夫山森林公园</h3>
                        <div class="mask-container">
                            <div class="mask">
                                <div class="middle">
                                    <h3>大夫山森林公园</h3>
                                    <p>广州地区生态型森林公园，山清水秀，空气清新</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
            <div class="grid">
                <div class="figure">
                    <a href="/poi/25146.html" target="_blank" title="华南植物园">
                        <img src="http://b3-q.mafengwo.net/s6/M00/0D/DD/wKgB4lMtPNGAN1n0AAv_YXyOQLs29.jpeg?imageMogr2%2Fthumbnail%2F%21242x155r%2Fgravity%2FCenter%2Fcrop%2F%21242x155%2Fquality%2F100"
                             width="242" height="155">
                        <h3 class="title">华南植物园</h3>
                        <div class="mask-container">
                            <div class="mask">
                                <div class="middle">
                                    <h3>华南植物园</h3>
                                    <p>全国三大植物园之一，深秋的时候别有一番风味</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="figure">
                    <a href="/poi/28340.html" target="_blank" title="太古仓码头">
                        <img src="http://n1-q.mafengwo.net/s6/M00/DC/D7/wKgB4lOJsrKAejs0AAPBfY9TKxU00.jpeg?imageMogr2%2Fthumbnail%2F%21242x155r%2Fgravity%2FCenter%2Fcrop%2F%21242x155%2Fquality%2F100"
                             width="242" height="155">
                        <h3 class="title">太古仓码头</h3>
                        <div class="mask-container">
                            <div class="mask">
                                <div class="middle">
                                    <h3>太古仓码头</h3>
                                    <p>广州对外通商缩影，红砖房创意园区</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
        </div>
    </div>
</div>





<div class="row row-allScenic" data-cs-p="全部景点">
    <div class="wrapper">
        <h2 class="title">
            广州全部景点
            <a class="btn-add" href="/poi/add.php?iId=10088" target="_blank" title="推荐新的景点"><i>+</i>推荐新的景点</a>
        </h2>
        <ul class="nav clearfix">
            <li class="on"><a title="全部景点">全部景点</a></li>
            <li data-tagid="26750"><a title="周边古村落">周边古村落</a></li>
            <li data-tagid="26447"><a title="羊城八景">羊城八景</a></li>
            <li data-tagid="27407"><a title="赏花佳地">赏花佳地</a></li>
            <li data-tagid="27410"><a title="老建筑">老建筑</a></li>
            <li data-tagid="27413"><a title="小清新地儿">小清新地儿</a></li>
        </ul>
        <div class="bd">
            <ul class="scenic-list clearfix">
                <li>
                    <a href="/poi/449.html" target="_blank" title="广州长隆野生动物世界">
                        <div class="img"><img
                                src="http://n1-q.mafengwo.net/s9/M00/2D/99/wKgBs1dAO4KAZS5gAA9In_luGxU53.jpeg?imageMogr2%2Fthumbnail%2F%21192x130r%2Fgravity%2FCenter%2Fcrop%2F%21192x130%2Fquality%2F100"
                                width="192" height="130"></div>
                        <h3>广州长隆野生动物世界</h3>
                    </a>

                </li>
            </ul>
        </div>
        <div class="_j_tn_pagination">
            <div align="right" class="m-pagination">
                <span class="count">共<span>20</span>页 / <span>2540</span>条</span>

                <span class="pg-current">1</span>
                <a class="pi" data-page="2" rel="nofollow" title="第2页">2</a>
                <a class="pi" data-page="3" rel="nofollow" title="第3页">3</a>
                <a class="pi" data-page="4" rel="nofollow" title="第4页">4</a>
                <a class="pi" data-page="5" rel="nofollow" title="第5页">5</a>
                <a class="pi" data-page="6" rel="nofollow" title="第6页">6</a>

                <a class="pi pg-next" data-page="2" rel="nofollow" title="后一页">后一页</a>
                <a class="pi pg-last" data-page="20" rel="nofollow" title="末页">末页</a>
            </div>
        </div>
    </div>
</div>


    </div>
    <div id="footer">
        <div class="ft-content" style="width: 1105px">
            <div class="ft-info clearfix">
                <dl class="ft-info-col ft-info-intro">
                    <dt>骡窝窝旅游网</dt>
                    <dd>叩丁狼是一家专注于培养高级IT技术人才，为学员提供定制化IT职业规划方案及</dd>
                    <dd>意见咨询服务的教育科技公司，为您提供海量优质课程，以及创新的线上线下学</dd>
                    <dd>习体验，帮助您获得全新的个人发展和能力提升。</dd>
                </dl>
                <dl class="ft-info-col ft-info-qrcode">
                    <dd>
                        <span class="ft-qrcode-tejia"></span>
                    </dd>
                    <dd>
                        <span class="ft-qrcode-weixin"></span>
                    </dd>
                    <dd>
                        <span class="ft-qrcode-weixin" style="background-image: url('https://p3-q.mafengwo.net/s10/M00/48/A9/wKgBZ1t_4sSAVJ6uAAAlzJ0PZgU881.png?imageMogr2%2Fthumbnail%2F%2194x90r%2Fgravity%2FCenter%2Fcrop%2F%2194x90%2Fquality%2F90')"></span>
                    </dd>
                </dl>
                <dl class="ft-info-social">
                    <dt>向崇尚自由的加勒比海盗致敬！</dt>
                    <dd>
                        <a class="ft-social-weibo" target="_blank" href="javascript:;" rel="nofollow"><i
                                class="ft-social-icon"></i></a>
                        <a class="ft-social-qqt" target="_blank" href="javascript:;" rel="nofollow"><i
                                class="ft-social-icon"></i></a>
                        <a class="ft-social-qzone" target="_blank" href="javascript:;" rel="nofollow"><i
                                class="ft-social-icon"></i></a>
                    </dd>
                </dl>
            </div>

            <div class="ft-links">
                <a target="_blank" href="http://china.makepolo.com/">马可波罗</a><a target="_blank" href="http://www.onlylady.com/">Onlylady女人志</a><a target="_blank" href="http://trip.elong.com/">艺龙旅游指南</a><a target="_blank" href="http://www.cncn.com">欣欣旅游网</a>
                <a target="_blank" href="http://www.8264.com/">户外运动</a><a target="_blank" href="http://www.yue365.com/">365音乐网</a><a target="_blank" href="http://ishare.iask.sina.com.cn/">爱问共享资料</a><a target="_blank" href="http://www.uzai.com/">旅游网</a>
                <a target="_blank" href="http://www.zongheng.com/">小说网</a>
                <a target="_blank" href="http://www.xuexila.com/">学习啦</a><a target="_blank" href="http://www.yododo.com">游多多自助游</a><a target="_blank" href="http://www.zhcpic.com/">问答</a><a target="_blank" href="http://huoche.mafengwo.cn/">火车时刻表</a>
                <a target="_blank" href="http://www.lvmama.com">驴妈妈旅游网</a>
                <a target="_blank" href="http://www.haodou.com/">好豆美食网</a><a target="_blank" href="http://www.taoche.com/">二手车</a><a target="_blank" href="http://www.lvye.cn">绿野户外</a><a target="_blank" href="http://www.tuniu.com/">途牛旅游网</a>
                <a target="_blank" href="http://www.mapbar.com/">图吧</a>
                <a target="_blank" href="http://www.chnsuv.com">SUV联合越野</a><a target="_blank" href="http://www.uc.cn/">手机浏览器</a><a target="_blank" href="http://sh.city8.com/">上海地图</a><a target="_blank" href="http://www.tianqi.com/">天气预报查询</a>
                <a target="_blank" href="http://www.ly.com/">同程旅游</a>
                <a target="_blank" href="http://www.tieyou.com/">火车票</a><a target="_blank" href="http://www.yunos.com/">YunOS</a><a target="_blank" href="http://you.ctrip.com/">携程旅游</a><a target="_blank" href="http://www.jinjiang.com">锦江旅游</a>
                <a target="_blank" href="http://www.huoche.net/">火车时刻表</a>
                <a target="_blank" href="http://www.tripadvisor.cn/">TripAdvisor</a><a target="_blank" href="http://www.tianxun.com/">天巡网</a><a target="_blank" href="http://www.mayi.com/">短租房</a><a target="_blank" href="http://www.zuzuche.com">租租车</a>
                <a target="_blank" href="http://www.5fen.com/">五分旅游网</a>
                <a target="_blank" href="http://www.zhuna.cn/">酒店预订</a><a target="_blank" href="http://www.ailvxing.com">爱旅行网</a><a target="_blank" href="http://360.mafengwo.cn/all.php">旅游</a><a target="_blank" href="http://vacations.ctrip.com/">旅游网</a>
                <a target="_blank" href="http://www.wed114.cn">wed114结婚网</a>
                <a target="_blank" href="http://www.chexun.com/">车讯网</a><a target="_blank" href="http://www.aoyou.com/">遨游旅游网</a><a target="_blank" href="http://www.91.com/">手机</a>
                <a href="javascript:;" target="_blank">更多友情链接&gt;&gt;</a>
            </div>
        </div>
    </div>
    <div class="mfw-toolbar" id="_j_mfwtoolbar" style="display: block;">
        <div class="toolbar-item-top" style="display: none;">
            <a role="button" class="btn _j_gotop">
                <i class="icon_top"></i>
                <em>返回顶部</em>
            </a>
        </div>
        <div class="toolbar-item-feedback">
            <a role="button" data-japp="feedback" class="btn">
                <i class="icon_feedback"></i>
                <em>意见反馈</em>
            </a>
        </div>
        <div class="toolbar-item-code">
            <a role="button" class="btn">
                <i class="icon_code"></i>
            </a>
            <a role="button" class="mfw-code _j_code">


                <img src="https://p1-q.mafengwo.net/s1/M00/6C/51/wKgIC1t_6TuASybrAADGUPUHjr021.jpeg?imageMogr2%2Fthumbnail%2F%21450x192r%2Fgravity%2FCenter%2Fcrop%2F%21450x192%2Fquality%2F90"
                    width="450" height="192">
            </a>
            <!--<div class="wx-official-pop"><img src="http://images.mafengwo.net/images/qrcode-weixin.gif"><i class="_j_closeqrcode"></i></div>-->
        </div>

    </div>
</body>

</html>