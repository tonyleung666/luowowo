<#if reply.type == 0>
  <li>
      <a href="/#">
          <img src="${reply.headUrl!}"
               width="16" height="16">${reply.username!}
      </a>
      ： ${reply.content!}
      <a class="_j_reply re_reply" data-replyid="${reply.id!}"
         data-username="${reply.username!}" title="添加回复">回复</a>
      <br><span class="time">${reply.createTime?string('yyyy-MM-dd HH:mm:ss')}</span>
  </li>
<#else>
  <li>
      <a href="/#">
          <img src="${reply.headUrl!}"
               width="16" height="16">${reply.username!}
      </a>
      回复${reply.refReply.username!}：${reply.content!}
      <a class="_j_reply re_reply" data-replyid="${reply.id}" data-contentarea="replyContent${reply.poiCommentId!}"
         data-username="${reply.username!}" title="添加回复">回复</a>
      <br><span class="time">${reply.createTime?string('yyyy-MM-dd HH:mm:ss')}</span>
  </li>
</#if>