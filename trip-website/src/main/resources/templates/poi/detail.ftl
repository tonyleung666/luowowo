<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <link href="/styles/base.css" rel="stylesheet" type="text/css">
    <link href="/styles/replyDetail.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/js/jquery.js"></script>
    <script type="text/javascript" src="/js/plugins/jquery-form/jquery.form.js"></script>
    <link href="/js/plugins/jqPaginator/jqPagination.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/js/system/common.js"></script>
    <script src="/js/jquery-upload/jquery.ui.widget.js"></script>
    <script src="/js/jquery-upload/jquery.iframe-transport.js"></script>
    <script src="/js/jquery-upload/jquery.fileupload.js"></script>
    <#--<link href="/styles/strategyDetail.css" rel="stylesheet" type="text/css">-->
    <#--<style type="text/css">-->
        <#--.bar {-->
            <#--margin-top:10px;-->
            <#--height:10px;-->
            <#--max-width: 370px;-->
            <#--background: green;-->
        <#--}-->
    <#--</style>-->
    <script>
        $(function () {

            $('.useful').click(function () {
                var thumb_btn = $(this);
                var cmtId = thumb_btn.data('id');
                $.post('/poi/thumbUp', {cmtId:cmtId}, function (data) {
                    if(data.success){
                        thumb_btn.addClass("on");
                        $("#thumb" + cmtId).html(data.data.thumbupnum);
                    }else{
                        if(data.code == 102){
                            popup(data.msg);
                        }else{
                            thumb_btn.removeClass("on");
                            $("#thumb" + cmtId).html(data.data.thumbupnum);
                        }
                    }
                })
            });

            $('#favor').click(function () {
                var favor_btn = $(this);
                $.post('/poi/favor', {pid:${poi.id}}, function (data) {
                    if(data.success){
                        favor_btn.addClass("on");
                        popup("收藏成功!")
                    }else{
                        if(data.code == 102){
                            popup(data.msg);
                        }else{
                            favor_btn.removeClass("on");
                            popup("取消收藏!");
                        }
                    }
                })
            });

            $('.btn-reviews').click(function () {
                $('._j_commentarea').focus();
            });
            
            //回复
            $(".comment_list").on("click", ".re_reply", function () {
                var cmtid = $(this).data("cmtid");
                var username = $(this).data("username");
                var replyid = $(this).data("replyid");
                $('#replyContent'+ cmtid).attr("placeholder", "回复：" + username);
                $('#replyContent'+ cmtid).focus();

                $('#type' + cmtid).val(1);
                $('#refReplyId' + cmtid).val(replyid);
            });

            $('._j_comment').click(function () {
                var cmtid = $(this).data("cmtid");
                $("#replyContent"+ cmtid).attr("placeholder", "");

                $('#type' + cmtid).val(0);
                $('#refReplyId' + cmtid).val(null);

                $("#replyContent"+ cmtid).focus();
            });

            //发表回复
            $(".btn_submit_reply").click(function () {
                var cmtid = $(this).data("cmtid");
                if (!$("#replyContent" + cmtid).val()) {
                    alert("评论不能为空");
                    return;
                }
                $("#replyForm" + cmtid).ajaxSubmit(function (data) {
                    $("#commentContent").val("");
                    $("#commentContent").attr("placeholder", "");
                    $("#reply_list"+cmtid).append(data);
                    window.location.reload();
                })
            })
        })
    </script>
</head>

<body>
    <#assign currentNav="destination">
    <#include "../common/navbar.ftl">
<!----------------------------------------->

    <div class="container" data-cs-t="景点详情页">

    <div class="row row-top">
        <div class="wrapper">
            <div class="extra">
                <!-- 天气 S-->
                <div class="weather" data-cs-p="天气">
                    <a href="/weather/10088.html" target="_blank">
                        <img src="http://images.mafengwo.net/images/mdd_weather/icon/icon34.png" width="25" height="25">
                        <span>中雨 27℃~34℃</span>
                    </a>
                </div>
                <!-- 天气 E-->
                <!-- 收藏去过 S-->
                <div class="action _j_rside want-been">
                    <div class="been-box">
                        <a class="_j_beenpoi btn-been _j_hovergo" href="/path/" target="_blank" title="添加至我的足迹"
                           data-cs-p="足迹">
                            <i class="icon"></i>
                            <span class="txt">去过</span>
                        </a>
                        <div class="rate-pop" style="display:none;">
                            <div class="rank-star">
                                <span class="s-star s-star0"></span>
                                <div class="click_star">
                                    <a title="1星" rel="nofollow" data-num="1"></a>
                                    <a title="2星" rel="nofollow" data-num="2"></a>
                                    <a title="3星" rel="nofollow" data-num="3"></a>
                                    <a title="4星" rel="nofollow" data-num="4"></a>
                                    <a title="5星" rel="nofollow" data-num="5"></a>
                                </div>
                            </div>
                            <span class="rank-hint">必去推荐</span>
                        </div>
                    </div>
                    <a class="_j_favpoi btn-collect <#if pids??>${pids?seq_contains(poi.id)?string("on", "")}</#if>"
                       id="favor" href="javascript:;" title="添加收藏"
                       data-cs-p="收藏">
                        <i class="icon"></i>
                        <span class="txt">收藏</span>
                    </a>
                </div>
                <!-- 收藏去过 E-->
            </div>
            <!-- 面包屑 S-->
           <div class="crumb" data-cs-p="面包屑">
                <div class="item"><a href="/mdd/" target="_blank">目的地</a><em>&gt;</em></div>
                <div class="item">
                    <div class="drop">
                        <span class="hd"><a href="/travel-scenic-spot/mafengwo/10088.html">广州<i></i></a></span>
                        <div class="bd">
                            <i class="arrow"><b></b></i>
                            <div class="col">
                                <ul class="clearfix">
                                    <li><a href="/jd/10088/gonglve.html" target="_blank">广州景点</a></li>
                                    <li><a href="/hotel/10088/" target="_blank">广州酒店</a></li>
                                    <li><a href="/cy/10088/gonglve.html" target="_blank">广州美食</a></li>
                                    <li><a href="/gw/10088/gonglve.html" target="_blank">广州购物</a></li>
                                    <li><a href="/yl/10088/gonglve.html" target="_blank">广州娱乐</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <em>&gt;</em>
                </div>
                <div class="item cur">广州景点</div>
            </div>
            <!-- 面包屑 E-->


            <!-- POI名称 S-->
            <div class="title">
                <h1>${poi.name!}</h1>
                <div class="en">${poi.english!}</div>
            </div>
            <!-- POI名称 E-->

            <!-- 快捷导航 S-->
            <div style="height: 60px;">
                <div class="r-nav" id="poi-navbar" data-cs-p="快捷导航">
                    <ul class="clearfix">
                        <li data-scroll="overview" class="on">
                            <a title="概况">概况</a>
                        </li>
                        <li data-scroll="attractions" style="display: none">
                            <a title="景点亮点">景点亮点</a>
                        </li>
                        <li data-scroll="commentlist">
                            <a title="蜂蜂点评" href="#commentlist">蜂蜂点评<span>${poi.replynum!}</span></a>
                        </li>


                        <li data-scroll="comment" class="nav-right">
                            <a class="btn-reviews" title="我要点评">我要点评</a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- 快捷导航 E-->

        </div>
    </div>

    <div data-anchor="overview">
        <div class="row row-picture row-bg">
            <div class="wrapper">
                <a class="photo" data-cs-p="相册" href="/photo/poi/25091.html" target="_blank">
                    <div class="bd">
                        <div class="pic-big"><img
                                src="${(poi.imgUrls[0])!}"
                                width="690" height="370"></div>
                        <div class="pic-small"><img
                                src="${(poi.imgUrls[1])!}"
                                width="305" height="183"></div>
                        <div class="pic-small"><img
                                src="${(poi.imgUrls[2])!}"
                                width="305" height="183"></div>
                        <span>${poi.imgnum!}张图片</span></div>
                </a></div>
        </div>

        <!-- 简介 S -->
        <div class="mod mod-detail" data-cs-p="概况">
            ${poi.info!}

            <ul class="baseinfo clearfix">
                <li class="tel">
                    <div class="label">电话</div>
                    <div class="content">${poi.tel!}</div>
                </li>
                <li class="item-site">
                    <div class="label">网址</div>
                    <div class="content"><a href="${poi.web!}" target="_blank" rel="nofollow">${poi.web!}</a>
                    </div>
                </li>
                <li class="item-time">
                    <div class="label">用时参考</div>
                    <div class="content">${poi.visitTime!}</div>
                </li>
            </ul>

            <dl>
                <dt>交通</dt>
                ${poi.traffics!}
            </dl>
            <dl>
                <dt>门票</dt>
                ${poi.ticket!}
            </dl>
            <dl>
                <dt>开放时间</dt>
                ${poi.openHour!}
            </dl>

            <div style="color:#999;font-size:12px;" data-cs-p="概况-感谢蜂蜂">
                *信息更新时间：2019-07-14&nbsp;&nbsp;&nbsp;&nbsp;
                感谢蜂蜂 <a href="/u/79650791.html" target="_blank">一点点的爱</a>
                参与了编辑
            </div>
        </div>
        <!-- 简介 E -->

        <!-- 内部景点 S -->
        <div data-anchor="subPoilist">
            <div id="pagelet-block-bb456ee5b764682811223f9b8d30739e" class="pagelet-block"
                 data-api=":poi:pagelet:poiSubPoiApi" data-params="{&quot;poi_id&quot;:&quot;25091&quot;}" data-async="1"
                 data-controller="/js/poi/ControllerPoiSubPoi">
                <div class="mod mod-innerScenic" data-cs-p="内部景点">
                    <div class="mhd">内部景点</div>
                    <div class="mbd">
                        <ul class="clearfix">
                            <#if poi.children??>
                            <#list poi.children as child>
                                <li>
                                    <a href="/poi/detail?id=${child.id!}" target="_blank" title="${child.name!}">
                                        <img src="${child.imgUrls[0]}"
                                             width="235" height="150">
                                        <span class="num num-top">${child_index+1}</span>
                                        <div class="info">
                                            <h3>${child.name!}</h3>
                                            <span><em>${child.replynum}</em>条点评</span>
                                        </div>
                                    </a>
                                </li>
                            </#list>
                            </#if>
                        </ul>
                    </div>
                    <div class="more more-subpoi">
                        <a class="btn-subpoi" data-page="1">查看更多</a>
                    </div>
                </div>
                <style>
                    .mod-innerScenic .more {
                        margin-top: 20px;
                        text-align: center;
                    }

                    .mod-innerScenic .more a {
                        display: inline-block;
                        width: 160px;
                        height: 50px;
                        background-color: #fff;
                        border: 1px solid #fc9c27;
                        line-height: 50px;
                        color: #ff9d00;
                        font-size: 14px;
                        border-radius: 4px;
                        text-align: center;
                    }

                    .mod-innerScenic .num {
                        width: 40px;
                    }
                </style>
            </div>
        </div>
        <!-- 内部景点 E -->

    </div>

    <!--评论-->
    <div data-anchor="commentlist">

        <div id="pagelet-block-15f9d6d9ad9f6c363d2d27120e8a6198" class="pagelet-block"
             data-api=":poi:pagelet:poiCommentListApi" data-params="{&quot;poi_id&quot;:&quot;25091&quot;}" data-async="1"
             data-controller="/js/poi/ControllerPoiComment">
            <div class="mod mod-reviews" data-cs-p="评论列表">
                <div class="mhd mhd-large">蜂蜂点评<span>（共有<em>${poi.replynum!}</em>条真实评价）</span></div>
                <div class="review-nav">
                    <ul class="clearfix">
                        <li data-type="0" data-category="0" class="on"><span class="divide"></span><a
                                href="javascript:void(0);"><span>全部</span></a></li>
                    </ul>
                </div>
                <div class="loading-img" style="display: none;"><img
                        src="http://images.mafengwo.net/images/weng/loading3.gif"> Loading...
                </div>
                <div class="_j_commentlist">
                    <div class="rev-list" >
                        <ul>
                            <#list cmtPage.content as cmt>
                                <li class="rev-item comment-item clearfix">
                                    <div class="user">
                                        <a class="avatar" href="/#" ><img src="${cmt.headUrl!}" width="48" height="48"></a>
                                        <span class="level">LV.${cmt.level!}</span></div>
                                    <a class="useful <#if userInfo??>${cmt.thumbuplist?seq_contains(userInfo.id)?string("on", "")}</#if>" data-id="${cmt.id!}" title="点赞">
                                        <i></i><span class="useful-num" id="thumb${cmt.id}">${cmt.thumbupnum!}</span>
                                    </a>
                                    <a class="name" href="/u/52068941.html" target="_blank">${cmt.username!}</a>
                                    <span class="s-star s-star${cmt.overallStar}"></span>
                                    <p class="rev-txt">${cmt.content!}
                                    </p>


                                    <div class="rev-img">
                                        <#list cmt.imgUrls as url>
                                            <a href="/#" target="_blank">
                                                <img src="${url}" width="200" height="120"></a>
                                        </#list>
                                    </div>

                                    <div class="info clearfix">
                                        <a class="btn-comment _j_comment" title="添加评论"
                                           data-cmtid="${cmt.id}">评论</a>
                                        <span class="time">${cmt.createTime?string('yyyy-MM-dd HH:mm:ss')}</span>
                                    </span>
                                    </div>

                                    <div class="comment add-reply-${cmt.id}">
                                    <#if cmt.replies ??>
                                        <ul class="more_reply_box comment_list" id="reply_list${cmt.id}">
                                            <#list cmt.replies as reply>
                                                <#if reply.type == 0>
                                                    <li>
                                                        <a href="/#">
                                                            <img src="${reply.headUrl!}"
                                                                 width="16" height="16">${reply.username!}
                                                        </a>
                                                        ： ${reply.content!}
                                                        <a class="_j_reply re_reply" data-replyid="${reply.id}" data-cmtid="${cmt.id}"
                                                           data-username="${reply.username!}" title="添加回复">回复</a>
                                                        <br><span class="time">${reply.createTime?string('yyyy-MM-dd HH:mm:ss')}</span>
                                                    </li>
                                                <#else>
                                                    <li>
                                                        <a href="/#">
                                                            <img src="${reply.headUrl!}"
                                                                 width="16" height="16">${reply.username!}
                                                        </a>
                                                        回复${reply.refReply.username!}：${reply.content!}
                                                        <a class="_j_reply re_reply" data-replyid="${reply.id}" data-cmtid="${cmt.id}"
                                                           data-username="${reply.username!}" title="添加回复">回复</a>
                                                        <br><span class="time">${reply.createTime?string('yyyy-MM-dd HH:mm:ss')}</span>
                                                    </li>
                                                </#if>
                                            </#list>
                                        </ul>
                                    </#if>

                                        <div class="add-comment hide reply-form">
                                            <form id="replyForm${cmt.id}" action="/poi/addReply" method="post">
                                                <input type="hidden" name="poiCommentId" value="${cmt.id!}">
                                                <input type="hidden" name="type" value="0" id="type${cmt.id}">
                                                <input type="hidden" name="refReply.id" id="refReplyId${cmt.id}">
                                                <textarea class="comment_reply" name="content" id="replyContent${cmt.id}"
                                                          style="overflow: hidden; color: rgb(204, 204, 204);"></textarea>
                                                <a class="btn btn_submit_reply" data-cmtid="${cmt.id}">回复</a>
                                            </form>
                                        </div>
                                    </div>
                                </li>
                            </#list>
                        </ul>
                    </div>

                    <div align="right" class="m-pagination">
                        <span class="count">共<span>${cmtPage.totalPages!}</span>页 / <span>${cmtPage.totalElements!}</span>条</span>
                        <#list 1..cmtPage.totalPages as i>
                            <a class="pi" data-page="${i}" rel="nofollow" title="第${i}页">${i}</a>
                        </#list>
                        <#--<span class="pg-current">1</span>-->
                        <#--<a class="pi" data-page="5" rel="nofollow" title="第5页">5</a>-->

                        <a class="pi pg-next" data-page="2" rel="nofollow" title="后一页">后一页</a>
                        <a class="pi pg-last" data-page="5" rel="nofollow" title="末页">末页</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div data-anchor="comment">
        <div class="row row-reviewForm" id="comment_20190714202243" data-cs-p="点评">
            <div class="wrapper">

                <div class="mfw-reviews">
                    <div id="_j_commentform_cnt">
                        <h2>
                            <strong>${poi.name}</strong>
                            <em>*</em>为必填选项
                        </h2>
                        <form id="commentForm" action="/poi/addCommnet" method="post" class="_j_commentdialogform" data-typeid="3">
                            <input type="hidden" name="poiId" value="${poi.id}">
                            <input type="hidden" name="poiName" value="${poi.name}">
                            <input type="hidden" name="imgUrls" id="imgUrls">
                            <div class="review-item item-star">
                                <div class="label"><em>*</em>总体评价</div>
                                <div class="review-star _j_rankblock" data-star="" name="rank">
                                    <input type="hidden" name="overallStar" value="" essential="1" data-inputname="总体评价">
                                    <span class="_j_starcount star0"></span>
                                    <div class="click-star _j_starlist">
                                        <a role="button" title="千万别去" rel="nofollow"></a>
                                        <a role="button" title="不推荐" rel="nofollow"></a>
                                        <a role="button" title="一般般" rel="nofollow"></a>
                                        <a role="button" title="值得一去" rel="nofollow"></a>
                                        <a role="button" title="必须推荐" rel="nofollow"></a>
                                    </div>

                                </div>
                                <span class="txt-tips _j_startip">点击星星打分</span>
                            </div>
                            <div class="review-group">
                                <div class="review-item item-rating"
                                     data-conf="{&quot;type&quot;:&quot;select&quot;,&quot;notnull&quot;:&quot;1&quot;,&quot;name&quot;:&quot;\u98ce\u5149&quot;,&quot;conf&quot;:{&quot;options&quot;:[&quot;1&quot;,&quot;2&quot;,&quot;3&quot;,&quot;4&quot;,&quot;5&quot;]},&quot;show&quot;:{&quot;style&quot;:&quot;starRank&quot;}}">
                                    <div class="label"><em>*</em>风光</div>
                                    <div class="review-score _j_rankblock" data-star="" name="scene_rank">
                                        <input type="hidden" name="sceneStar" value="" essential="1"
                                               data-inputname="给风光的评分">
                                        <span class="_j_starcount star0"></span>
                                        <div class="click-star _j_starlist">
                                            <a role="button" title="特别差" rel="nofollow"></a>
                                            <a role="button" title="不太好" rel="nofollow"></a>
                                            <a role="button" title="一般般" rel="nofollow"></a>
                                            <a role="button" title="很棒" rel="nofollow"></a>
                                            <a role="button" title="超出预期" rel="nofollow"></a>
                                        </div>
                                    </div>
                                    <span class="txt-tips _j_startip">给风光打分</span>
                                </div>
                                <div class="review-item item-rating"
                                     data-conf="{&quot;type&quot;:&quot;select&quot;,&quot;notnull&quot;:&quot;1&quot;,&quot;name&quot;:&quot;\u7279\u8272&quot;,&quot;conf&quot;:{&quot;options&quot;:[&quot;1&quot;,&quot;2&quot;,&quot;3&quot;,&quot;4&quot;,&quot;5&quot;]},&quot;show&quot;:{&quot;style&quot;:&quot;starRank&quot;}}">
                                    <div class="label"><em>*</em>特色</div>
                                    <div class="review-score _j_rankblock" data-star="" name="feature_rank">
                                        <input type="hidden" name="featuresStar" value="" essential="1"
                                               data-inputname="给特色的评分">
                                        <span class="_j_starcount star0"></span>
                                        <div class="click-star _j_starlist">
                                            <a role="button" title="特别差" rel="nofollow"></a>
                                            <a role="button" title="不太好" rel="nofollow"></a>
                                            <a role="button" title="一般般" rel="nofollow"></a>
                                            <a role="button" title="很棒" rel="nofollow"></a>
                                            <a role="button" title="超出预期" rel="nofollow"></a>
                                        </div>

                                    </div>
                                    <span class="txt-tips _j_startip">给特色打分</span>
                                </div>
                                <div class="review-item item-rating"
                                     data-conf="{&quot;type&quot;:&quot;select&quot;,&quot;notnull&quot;:&quot;1&quot;,&quot;name&quot;:&quot;\u670d\u52a1&quot;,&quot;conf&quot;:{&quot;options&quot;:[&quot;1&quot;,&quot;2&quot;,&quot;3&quot;,&quot;4&quot;,&quot;5&quot;]},&quot;show&quot;:{&quot;style&quot;:&quot;starRank&quot;}}">
                                    <div class="label"><em>*</em>服务</div>
                                    <div class="review-score _j_rankblock" data-star="" name="service_rank">
                                        <input type="hidden" name="serviceStar" value="" essential="1"
                                               data-inputname="给服务的评分">
                                        <span class="_j_starcount star0"></span>
                                        <div class="click-star _j_starlist">
                                            <a role="button" title="特别差" rel="nofollow"></a>
                                            <a role="button" title="不太好" rel="nofollow"></a>
                                            <a role="button" title="一般般" rel="nofollow"></a>
                                            <a role="button" title="很棒" rel="nofollow"></a>
                                            <a role="button" title="超出预期" rel="nofollow"></a>
                                        </div>

                                    </div>
                                    <span class="txt-tips _j_startip">给服务打分</span>
                                </div>
                            </div>
                            <div class="review-item item-comment">
                                <div class="label"><em>*</em>评价</div>
                                <div class="content">
                                    <textarea class="_j_commentarea" name="content" essential="1" data-inputname="点评内容"
                                              placeholder="详细、客观、真实，130字以上为佳！上传图片会加分哦！" data-minlen="15"
                                              data-maxlen="1000"></textarea>
                                    <p class="_j_commentcounttip">15-1000个字</p>
                                </div>
                            </div>

                            <div class="review-item item-photo">
                                <div class="label">上传照片</div>
                                <div class="content" >
                                    <dl class="upload-box _j_piclist">
                                        <dd data-commentid="" id="_j_addpicbtns" ids="0" style="position: relative;">
                                            <a class="add-place"><i></i></a>
                                            <div id="html5_1dfo6usgd1maqh5812ivtkf1n223_container"
                                                 class="moxie-shim moxie-shim-html5"
                                                 style="position: absolute; top: 0px; left: 0px; width: 120px; height: 120px; overflow: hidden; z-index: -1;">
                                                <input id="html5_1dfo6usgd1maqh5812ivtkf1n223" type="file" name="pic"
                                                       style="font-size: 999px; opacity: 0; position: absolute; top: 0px; left: 0px; width: 100%; height: 100%;"
                                                       multiple="" accept="image/jpeg,image/gif,image/png,.JPEG"></div>
                                        </dd>
                                    </dl>
                                </div>
                            </div>
                            <script>
                                $('.add-place').click(function(){
                                    $('#html5_1dfo6usgd1maqh5812ivtkf1n223').click();
                                });
                                $('#html5_1dfo6usgd1maqh5812ivtkf1n223').fileupload(
                                        {
                                            url: 'imgUpload',
                                            dataType: "json",
                                            multipart:true,
                                            done:function(e,data){
                                                //done方法就是上传完毕的回调函数，其他回调函数可以自行查看api
                                                //注意data要和jquery的ajax的data参数区分，这个对象包含了整个请求信息
                                                //返回的数据在data.result中，这里dataType中设置的返回的数据类型为json
                                                if(data.result.sta) {
                                                    console.log(data.result);
                                                    // 上传成功：
                                                    var url = $("#imgUrls").val();
                                                    if (url) {
                                                        url = url + ";";
                                                    }
                                                    var src = data.result.previewSrc;
                                                    $("#imgUrls").val(url + src);
                                                    $(".upload-box").append("<dd class='_j_picitem' data-picid='0'>" +
                                                            "<div class='place'><div class='img'><img class='_j_edit_src' src="+src+" style='width:120px;height:120px'></div><div class='title'><h4 class='_j_edit_title'></h4></div><div class='mask-operate'><a class='btn-remove _j_remove_dd'></a></div></div></dd>");
                                                } else {
                                                    // 上传失败：
                                                    $(".upload-box").append("<div style='color:red;'>"+data.result.msg+"</div>");
                                                }
                                            },
                                            progressall: function (e, data) {//上传进度
                                                var progress = parseInt(data.loaded / data.total * 100, 10);
                                                $(".progress .bar").css("width", progress + "%");
                                                $(".proportion").html("上传总进度："+progress+"%");
                                            }
                                        }
                                );
                            </script>
                            <div class="review-item item-action">
                                <a class="btn-large _j_submit" role="button" title="提交点评">提交点评</a>
                            </div>
                        </form>
                    </div>
                </div>
                 <script type="text/javascript">
                     $('._j_submit').click(function () {
                        $('#commentForm').ajaxSubmit(function (data) {
                            window.location.reload();
                        });
                     });

                   $(function () {
                       $("._j_starlist a").mouseover(function () {
                            var index = $(this).index()+1;
                            var text = $(this).attr("title");

                           $(this).closest("div").prev().addClass("star"+index);
                           $(this).closest("div").parent().next().html(text);
                       }).mouseout(function () {
                           var index = $(this).index()+1;
                           var text = $(this).attr("title");
                           $(this).closest("div").prev().removeClass("star"+index);
                           $(this).closest("div").parent().next().html(text);

                           var x = $(this).closest("div").prev().prev().val();
                           if(x == index){
                               $(this).closest("div").prev().addClass("star"+x);
                           }


                       }).click(function () {
                           var index = $(this).index()+1;
                           var text = $(this).attr("title");
                           $(this).closest("div").prev().addClass("star"+index);
                           $(this).closest("div").parent().next().html(text);

                           $(this).closest("div").prev().prev().val(index);

                       })
                   })

                </script>



                <div class="mfw-reviews have-reviews" style="display: none">
                    <h2>
                        <strong>广州塔</strong>
                    </h2>
                    <div class="review-item item-star">
                        <div class="label">你已评价为</div>
                        <div class="review-star">
                            <span class="star0"></span>
                        </div>
                        <a class="edit-reviews" data-commentid="" title="修改评论"><i></i>我要修改</a>
                    </div>
                </div>
            </div>
        </div>

    </div>



    </div>








    <!----------------------------------------->
    <div id="footer">
        <div class="ft-content" style="width: 1105px">
            <div class="ft-info clearfix">
                <dl class="ft-info-col ft-info-intro">
                    <dt>马蜂窝旅游网</dt>
                    <dd>叩丁狼是一家专注于培养高级IT技术人才，为学员提供定制化IT职业规划方案及</dd>
                    <dd>意见咨询服务的教育科技公司，为您提供海量优质课程，以及创新的线上线下学</dd>
                    <dd>习体验，帮助您获得全新的个人发展和能力提升。</dd>
                </dl>
                <dl class="ft-info-col ft-info-qrcode">
                    <dd>
                        <span class="ft-qrcode-tejia"></span>
                    </dd>
                    <dd>
                        <span class="ft-qrcode-weixin"></span>
                    </dd>
                    <dd>
                        <span class="ft-qrcode-weixin" style="background-image: url('https://p3-q.mafengwo.net/s10/M00/48/A9/wKgBZ1t_4sSAVJ6uAAAlzJ0PZgU881.png?imageMogr2%2Fthumbnail%2F%2194x90r%2Fgravity%2FCenter%2Fcrop%2F%2194x90%2Fquality%2F90')"></span>
                    </dd>
                </dl>
                <dl class="ft-info-social">
                    <dt>向崇尚自由的加勒比海盗致敬！</dt>
                    <dd>
                        <a class="ft-social-weibo" target="_blank" href="javascript:;" rel="nofollow"><i
                                class="ft-social-icon"></i></a>
                        <a class="ft-social-qqt" target="_blank" href="javascript:;" rel="nofollow"><i
                                class="ft-social-icon"></i></a>
                        <a class="ft-social-qzone" target="_blank" href="javascript:;" rel="nofollow"><i
                                class="ft-social-icon"></i></a>
                    </dd>
                </dl>
            </div>

            <div class="ft-links">
                <a target="_blank" href="http://china.makepolo.com/">马可波罗</a><a target="_blank" href="http://www.onlylady.com/">Onlylady女人志</a><a target="_blank" href="http://trip.elong.com/">艺龙旅游指南</a><a target="_blank" href="http://www.cncn.com">欣欣旅游网</a>
                <a target="_blank" href="http://www.8264.com/">户外运动</a><a target="_blank" href="http://www.yue365.com/">365音乐网</a><a target="_blank" href="http://ishare.iask.sina.com.cn/">爱问共享资料</a><a target="_blank" href="http://www.uzai.com/">旅游网</a>
                <a target="_blank" href="http://www.zongheng.com/">小说网</a>
                <a target="_blank" href="http://www.xuexila.com/">学习啦</a><a target="_blank" href="http://www.yododo.com">游多多自助游</a><a target="_blank" href="http://www.zhcpic.com/">问答</a><a target="_blank" href="http://huoche.mafengwo.cn/">火车时刻表</a>
                <a target="_blank" href="http://www.lvmama.com">驴妈妈旅游网</a>
                <a target="_blank" href="http://www.haodou.com/">好豆美食网</a><a target="_blank" href="http://www.taoche.com/">二手车</a><a target="_blank" href="http://www.lvye.cn">绿野户外</a><a target="_blank" href="http://www.tuniu.com/">途牛旅游网</a>
                <a target="_blank" href="http://www.mapbar.com/">图吧</a>
                <a target="_blank" href="http://www.chnsuv.com">SUV联合越野</a><a target="_blank" href="http://www.uc.cn/">手机浏览器</a><a target="_blank" href="http://sh.city8.com/">上海地图</a><a target="_blank" href="http://www.tianqi.com/">天气预报查询</a>
                <a target="_blank" href="http://www.ly.com/">同程旅游</a>
                <a target="_blank" href="http://www.tieyou.com/">火车票</a><a target="_blank" href="http://www.yunos.com/">YunOS</a><a target="_blank" href="http://you.ctrip.com/">携程旅游</a><a target="_blank" href="http://www.jinjiang.com">锦江旅游</a>
                <a target="_blank" href="http://www.huoche.net/">火车时刻表</a>
                <a target="_blank" href="http://www.tripadvisor.cn/">TripAdvisor</a><a target="_blank" href="http://www.tianxun.com/">天巡网</a><a target="_blank" href="http://www.mayi.com/">短租房</a><a target="_blank" href="http://www.zuzuche.com">租租车</a>
                <a target="_blank" href="http://www.5fen.com/">五分旅游网</a>
                <a target="_blank" href="http://www.zhuna.cn/">酒店预订</a><a target="_blank" href="http://www.ailvxing.com">爱旅行网</a><a target="_blank" href="http://360.mafengwo.cn/all.php">旅游</a><a target="_blank" href="http://vacations.ctrip.com/">旅游网</a>
                <a target="_blank" href="http://www.wed114.cn">wed114结婚网</a>
                <a target="_blank" href="http://www.chexun.com/">车讯网</a><a target="_blank" href="http://www.aoyou.com/">遨游旅游网</a><a target="_blank" href="http://www.91.com/">手机</a>
                <a href="javascript:;" target="_blank">更多友情链接&gt;&gt;</a>
            </div>
        </div>
    </div>
    <div class="mfw-toolbar" id="_j_mfwtoolbar" style="display: block;">
        <div class="toolbar-item-top" style="display: none;">
            <a role="button" class="btn _j_gotop">
                <i class="icon_top"></i>
                <em>返回顶部</em>
            </a>
        </div>
        <div class="toolbar-item-feedback">
            <a role="button" data-japp="feedback" class="btn">
                <i class="icon_feedback"></i>
                <em>意见反馈</em>
            </a>
        </div>
        <div class="toolbar-item-code">
            <a role="button" class="btn">
                <i class="icon_code"></i>
            </a>
            <a role="button" class="mfw-code _j_code">


                <img src="https://p1-q.mafengwo.net/s1/M00/6C/51/wKgIC1t_6TuASybrAADGUPUHjr021.jpeg?imageMogr2%2Fthumbnail%2F%21450x192r%2Fgravity%2FCenter%2Fcrop%2F%21450x192%2Fquality%2F90"
                    width="450" height="192">
            </a>
            <!--<div class="wx-official-pop"><img src="http://images.mafengwo.net/images/qrcode-weixin.gif"><i class="_j_closeqrcode"></i></div>-->
        </div>

    </div>
</body>

</html>