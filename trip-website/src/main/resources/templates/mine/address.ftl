<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">

    <link href="/styles/base.css" rel="stylesheet" type="text/css">
    <link href="/js/datepicker/datepicker.css" rel="stylesheet">
    <link href="/styles/setting.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/js/jquery.js"></script>
    <script type="text/javascript" src="/js/jquery-upload/jquery.ui.widget.js"></script>
    <script type="text/javascript" src="/js/jquery-upload/jquery.iframe-transport.js"></script>
    <script type="text/javascript" src="/js/jquery-upload/jquery.fileupload.js"></script>
    <script src="/js/datepicker/datepicker.js"></script>
    <script type="text/javascript" src="/js/plugins/jquery-form/jquery.form.js"></script>

    <script type="text/javascript" src="/js/main.js"></script>
    <script type="text/javascript" src="/js/distpicker.data.js"></script>
    <script type="text/javascript" src="/js/distpicker.js"></script>
    <script type="text/javascript" src="/dist/distpicker.min.js"></script>
    <script type="text/javascript" src="/dist/distpicker.data.min.js"></script>
    <script>
        $(function(){
            $('#inputBtn').click(function () {
                $("#inputModal").modal('show');
            });

            $(".submitBtn").click(function () {
                $("#editForm").ajaxSubmit(function (data) {
                    handleMessage(data);
                })
            })
        });
    </script>
  <title>地址</title>
</head>
<body>

<div class="modal fade" id="inputModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div data-toggle="distpicker">
                <select data-province="---- 选择省 ----"></select>
                <select data-city="---- 选择市 ----"></select>
                <select data-district="---- 选择区 ----"></select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <span class="glyphicon glyphicon-off"></span> 关闭
                </button>
                <button type="button" class="btn btn-primary submitBtn">
                    <span class="glyphicon glyphicon-saved"></span> 保存
                </button>
            </div>
        </div>
    </div>
</div>


</body>

</html>