<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <link href="/styles/base.css" rel="stylesheet" type="text/css">
    <link href="/styles/homepage.css" rel="stylesheet" type="text/css">
    <link href="/styles/popup.css" rel="stylesheet" type="text/css">
    <link href="/styles/replyDetail.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/js/jquery/jquery.js"></script>
    <script type="text/javascript" src="/js/system/common.js"></script>
    <script type="text/javascript" src="/js/system/homepage.js"></script>


</head>

<body style="position: relative;">
<#include "topBar.ftl">
<div class="main">
    <div class="banner">
        <div class="tags_bar">
            <div class="center clearfix">
                <ul class="flt2">
                    <li class="on"><a class="tags_link" href="/mine/home" title="我的窝">我的窝</a></li>
                    <li><a class="tags_link" href="/mine/myTravelNotes" title="我的游记">我的游记</a></li>
                    <li><a class="tags_link" href="/mine/review" title="我的点评">我的点评</a></li>
                    <li id="_j_pathnav"><a class="tags_link" href="/mine/travelcollection" title="我的收藏">我的收藏</a></li>
                    <li><a class="tags_link" href="/mine/orders" title="我的订单">我的订单</a></li>
                    <li><a class="tags_link" href="/mine/setting" title="设置">设置</a></li>
                </ul>
            </div>
        </div>
    </div>
        <div class="center clearfix">
            <div class="side_bar flt1">
                <div class="MAvatar">
                    <div class="MAvaImg hasAva">
                        <img src="${(userInfo.headImgUrl)!}" height="120" width="120" alt="xxxx">
                    </div>
                    <div class="MAvaName">${(userInfo.nickname)!}
                        <i class="MGenderMale"></i>
                    </div>

                    <div class="its_tags">
                        <a href="javascript:;" target="_blank" title="VIP"><i class="vip"></i></a>
                        <a href="javascript:;" target="_blank" title="分舵"><i class="duo"></i></a>
                        <a href="javascript:;" target="_blank" title="指路人"><i class="zhiluren"></i></a>
                    </div>
                    <div class="MAvaInfo clearfix MAvaMyInfo">
                        <span class="MAvaLevel flt1">等级：<a href="javascript:;"
                                title="Lv.等级" target="_blank">LV.${(userInfo.level)!}</a></span>
                        <span class="MAvaPlace flt1" title="广州">现居：${(userInfo.city)!}</span> <span class="MAvaSet">
                        <a title="设置" href="/mine/setting" ></a></span>
                    </div>
                    <div id="_j_profilearea" class="MAvaProfile">
                        <div class="MProfile _j_showintrobox" style="display: block;">
                            <pre>${(userInfo.info)!}</pre>
                        </div>
                        <div class="MAvaInput _j_inputarea hide">
                            <textarea id="_j_introarea" placeholder="例：摄影师/旅居澳洲/潜水爱好者" maxlength="100"></textarea>
                            <a role="button" id="_j_introsaver" class="MAvaBtn" title="保存">保存</a>
                        </div>

                    </div>
                    <div class="MAvaMore clearfix">
                        <div class="MAvaNums">
                            <strong><a href="javascript:;" target="_blank">${attentions?size}</a></strong>
                            <p>关注</p>
                        </div>
                        <div class="MAvaNums">
                            <strong><a href="javascript:;" target="_blank">${fansNum!}</a></strong>
                            <p>粉丝</p>
                        </div>
                        <div class="MAvaNums last">
                            <strong><a href="javascript:;" target="_blank">${(userInfo.integral)!}</a></strong>
                            <p>积分</p>
                        </div>
                    </div>
                </div>
            <div class="MUsers">
                <div class="MUsersTitle">我的关注</div>
                <div class="MUsersDetail" id="_j_followcnt">
                    <div class="MUsersAtom">
                        <ul class="clearfix _j_followlist">
                            <#list attentions as attention>
                                <li>
                                    <a href="javascript:;" target="_blank">
                                        <img src="${(attention.headImgUrl)!}"
                                             height="48" width="48" alt="桃小桃和她的喵" title="用户头像">
                                    </a>
                                    <p><a href="javascript:;" target="_blank"
                                          title="用户昵称">${(attention.nickname)!}</a></p>
                                </li>
                            </#list>
                        </ul>
                    </div>
                    <!-- <div class="MSimplePages _j_follow_page_action" data-total="13"><span
                            class="MPrev _j_prev disabled" title="上一页"></span><span class="MNext _j_next"
                            title="下一页"></span></div> -->
                </div>
            </div>
        </div>
        <div class="content flt2">
            <div class="common_block my_notes">
                <div class="notes_list">
                    <ul>
                        <#list pageInfo.list as t>
                            <li data-order="1" data-top="0">
                                <dl>
                                    <dt>
                                        <a href="/travel/detail?id=${t.id}" target="_blank" id="_j_coverlink_12894894">
                                            <img src="${t.coverUrl!}" height="400" width="680" alt="封面"></a>
                                        <div class="hover_item">
                                            <div class="thumb_description">
                                                <strong>${(t.dest.name)!}</strong>
                                            </div>
                                        </div>

                                    </dt>
                                    <dd>
                                        <div class="note_title clearfix">
                                            <div class="MDing">
                                                <span id="topvote12894894">${t.thumbsupnum!}</span>
                                                <a role="button" data-japp="articleding" rel="nofollow"
                                                   data-iid="12894894" data-vote="0" title="顶一下">顶</a>
                                            </div>
                                            <div class="note_info">
                                                <h3>
                                                    <a href="/travel/detail?id=${t.id}" target="_blank" title="游记">${t.title!}</a>
                                                </h3>
                                                <div class="note_more">
                                                    <span class="MInfoNum">
                                                        <i class="MIcoView"></i><em>${t.viewnum!}</em>
                                                    </span>
                                                    <span class="MInfoNum"><i class="MIcoStar"></i><em>${t.replynum!}</em></span>
                                                    <span class="time">${(t.createTime?string("yyyy-MM-dd"))!}</span>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="note_word">
                                            ${t.summary!}
                                        </div>
                                    </dd>
                                </dl>
                            </li>
                        </#list>
                        <div class="more_notes">
                            <a href="javascript:;">共<strong>${(pageInfo.list?size)}</strong>篇游记 </a>
                        </div>
                    </ul>
                </div>
                <#--poiComments-->
                <div class="common_block my_ask my_dp" id="_j_commentwrap">
                    <div class="dp_list">
                        <ul>
                         <#list poiComments as poiComment>
                            <li>
                                <dl>
                                    <dt>
                                        <div class="dp_handles flt2">
                                            <div class="dp_ding flt2">
                                                <a role="button" title="顶一下" class="disabled" data-id="191713354"></a>
                                                <strong class="_j_dingnum">${poiComment.thumbupnum!}</strong>
                                            </div>
                                        </div>
                                        <div class="dp_title"><a href="javascript:;"
                                                                 target="_blank"><span>${poiComment.poiName!}</span><br></a>
                                        </div>
                                        <div><span class="MStarTips">${poiComment.grade!}</span></div>
                                        <div class="MStar">
                                            <span class="s-star s-star${poiComment.overallStar}"></span>
                                            <#--<div class="MStarIco MStarL3"><span></span></div>-->
                                        </div>
                                    </dt>
                                    <dd>
                                        <div class="dp_detail">
                                            <div class="dp_word">${poiComment.content!}</div>
                                            <div class="dp_pics">
                                                <ul class="clearfix">
                                                </ul>
                                            </div>
                                            <div class="dp_comment MInfoNum"><i
                                                    class="MIcoComment"></i><span><strong>${poiComment.replies?size}</strong>条回复</span>
                                            </div>
                                        </div>
                                    </dd>
                                </dl>
                            </li>
                         </#list>
                        </ul>
                    </div>
                    <div class="more_notes">
                        <a href="javascript:;">共<strong>${poiComments?size}</strong>篇景点点评</a>
                    </div>
                </div>
    </div>
</div>
<div id="footer">
    <div class="ft-content" style="width: 1105px">
        <div class="ft-info clearfix">
            <dl class="ft-info-col ft-info-intro">
                <dt>马蜂窝旅游网</dt>
                <dd>叩丁狼是一家专注于培养高级IT技术人才，为学员提供定制化IT职业规划方案及</dd>
                <dd>意见咨询服务的教育科技公司，为您提供海量优质课程，以及创新的线上线下学</dd>
                <dd>习体验，帮助您获得全新的个人发展和能力提升。</dd>
            </dl>
            <dl class="ft-info-col ft-info-qrcode">
                <dd>
                    <span class="ft-qrcode-tejia"></span>
                </dd>
                <dd>
                    <span class="ft-qrcode-weixin"></span>
                </dd>
                <dd>
                        <span class="ft-qrcode-weixin"
                              style="background-image: url('https://p3-q.mafengwo.net/s10/M00/48/A9/wKgBZ1t_4sSAVJ6uAAAlzJ0PZgU881.png?imageMogr2%2Fthumbnail%2F%2194x90r%2Fgravity%2FCenter%2Fcrop%2F%2194x90%2Fquality%2F90')"></span>
                </dd>
            </dl>
            <dl class="ft-info-social">
                <dt>向崇尚自由的加勒比海盗致敬！</dt>
                <dd>
                    <a class="ft-social-weibo" target="_blank" href="javascript:;" rel="nofollow"><i
                            class="ft-social-icon"></i></a>
                    <a class="ft-social-qqt" target="_blank" href="javascript:;" rel="nofollow"><i
                            class="ft-social-icon"></i></a>
                    <a class="ft-social-qzone" target="_blank" href="javascript:;" rel="nofollow"><i
                            class="ft-social-icon"></i></a>
                </dd>
            </dl>
        </div>

        <div class="ft-links">
            <a target="_blank" href="http://china.makepolo.com/">马可波罗</a><a target="_blank"
                                                                            href="http://www.onlylady.com/">Onlylady女人志</a><a
                target="_blank"
                href="http://trip.elong.com/">艺龙旅游指南</a><a target="_blank" href="http://www.cncn.com">欣欣旅游网</a>
            <a target="_blank" href="http://www.8264.com/">户外运动</a><a target="_blank"
                                                                      href="http://www.yue365.com/">365音乐网</a><a
                target="_blank"
                href="http://ishare.iask.sina.com.cn/">爱问共享资料</a><a target="_blank"
                                                                    href="http://www.uzai.com/">旅游网</a>
            <a target="_blank" href="http://www.zongheng.com/">小说网</a>
            <a target="_blank" href="http://www.xuexila.com/">学习啦</a><a target="_blank"
                                                                        href="http://www.yododo.com">游多多自助游</a><a
                target="_blank" href="http://www.zhcpic.com/">问答</a><a
                target="_blank" href="http://huoche.mafengwo.cn/">火车时刻表</a>
            <a target="_blank" href="http://www.lvmama.com">驴妈妈旅游网</a>
            <a target="_blank" href="http://www.haodou.com/">好豆美食网</a><a target="_blank"
                                                                         href="http://www.taoche.com/">二手车</a><a
                target="_blank" href="http://www.lvye.cn">绿野户外</a><a
                target="_blank" href="http://www.tuniu.com/">途牛旅游网</a>
            <a target="_blank" href="http://www.mapbar.com/">图吧</a>
            <a target="_blank" href="http://www.chnsuv.com">SUV联合越野</a><a target="_blank"
                                                                          href="http://www.uc.cn/">手机浏览器</a><a
                target="_blank" href="http://sh.city8.com/">上海地图</a><a
                target="_blank" href="http://www.tianqi.com/">天气预报查询</a>
            <a target="_blank" href="http://www.ly.com/">同程旅游</a>
            <a target="_blank" href="http://www.tieyou.com/">火车票</a><a target="_blank"
                                                                       href="http://www.yunos.com/">YunOS</a><a
                target="_blank" href="http://you.ctrip.com/">携程旅游</a><a
                target="_blank" href="http://www.jinjiang.com">锦江旅游</a>
            <a target="_blank" href="http://www.huoche.net/">火车时刻表</a>
            <a target="_blank" href="http://www.tripadvisor.cn/">TripAdvisor</a><a target="_blank"
                                                                                   href="http://www.tianxun.com/">天巡网</a><a
                target="_blank" href="http://www.mayi.com/">短租房</a><a
                target="_blank" href="http://www.zuzuche.com">租租车</a>
            <a target="_blank" href="http://www.5fen.com/">五分旅游网</a>
            <a target="_blank" href="http://www.zhuna.cn/">酒店预订</a><a target="_blank"
                                                                      href="http://www.ailvxing.com">爱旅行网</a><a
                target="_blank"
                href="http://360.mafengwo.cn/all.php">旅游</a><a target="_blank"
                                                               href="http://vacations.ctrip.com/">旅游网</a>
            <a target="_blank" href="http://www.wed114.cn">wed114结婚网</a>
            <a target="_blank" href="http://www.chexun.com/">车讯网</a><a target="_blank"
                                                                       href="http://www.aoyou.com/">遨游旅游网</a><a
                target="_blank" href="http://www.91.com/">手机</a>
            <a href="javascript:;" target="_blank">更多友情链接&gt;&gt;</a>
        </div>
    </div>
</div>
<div class="mfw-toolbar" id="_j_mfwtoolbar" style="display: block;">
    <div class="toolbar-item-top" style="display: none;">
        <a role="button" class="btn _j_gotop">
            <i class="icon_top"></i>
            <em>返回顶部</em>
        </a>
    </div>
    <div class="toolbar-item-feedback">
        <a role="button" data-japp="feedback" class="btn">
            <i class="icon_feedback"></i>
            <em>意见反馈</em>
        </a>
    </div>
    <div class="toolbar-item-code">
        <a role="button" class="btn">
            <i class="icon_code"></i>
        </a>
        <a role="button" class="mfw-code _j_code">


            <img src="https://p1-q.mafengwo.net/s1/M00/6C/51/wKgIC1t_6TuASybrAADGUPUHjr021.jpeg?imageMogr2%2Fthumbnail%2F%21450x192r%2Fgravity%2FCenter%2Fcrop%2F%21450x192%2Fquality%2F90"
                 width="450" height="192">
        </a>
        <!--<div class="wx-official-pop"><img src="http://images.mafengwo.net/images/qrcode-weixin.gif"><i class="_j_closeqrcode"></i></div>-->
    </div>

</div>
</body>

</html>