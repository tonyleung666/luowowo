<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>道具商店</title>
    <link href="http://css.mafengwo.net/css/cv/css+base:css+jquery.suggest:css+plugins:css+plugins+jquery.jgrowl:css+other+popup:css+app+topbar_v2^alw^1542357401.css" rel="stylesheet" type="text/css"/>
    <link href="/styles/strategyDetail.css" rel="stylesheet" type="text/css">
    <link href="http://css.mafengwo.net/css/daoju.css?1537192876" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="/js/jquery/jquery.js"></script>
    <script type="text/javascript" src="/js/plugins/jquery-form/jquery.form.js"></script>
    <script type="text/javascript" src="/js/plugins/jqPaginator/jq-paginator.min.js"></script>
    <link href="/js/plugins/jqPaginator/jqPagination.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/js/plugins/jqPaginator/jq-paginator.min.js"></script>
    <script type="text/javascript" src="/js/system/index.js"></script>
    <style>
        .img{
            border-top-color: initial;
            border-top-style: initial;
            border-top-width: 0px;
            border-right-color: initial;
            border-right-style: initial;
            border-right-width: 0px;
            border-bottom-color: initial;
            border-bottom-style: initial;
            border-bottom-width: 0px;
            border-left-color: initial;
            border-left-style: initial;
            border-left-width: 0px;
            border-image-source: initial;
            border-image-slice: initial;
            border-image-width: initial;
            border-image-outset: initial;
            border-image-repeat: initial;
            vertical-align: middle;
        }
    </style>
    <script>

    </script>

</head>
<body>
<form class="form-horizontal" action="/prop/buy" method="post" id="buyForm">
    <input id="itemId" type="hidden" value="0" name="id">
</form>
<div class="topBar">
    <div class="topBarC">
        <div class="logo"><a title="自由行" href="/">自由行</a></div>
        <div class="t_nav">
            <ul id="pnl_nav" data-cs-t="headnav_wo">
                <li data-cs-p="index">
                    <strong class="t"><a href="/">首页</a></strong>
                </li>
                <li data-cs-t="wenda" data-cs-p="wenda">
                    <strong class="t"><a data-cs-p="from_wo_nav" href="/question">问答</a></strong>
                </li>
                <li data-cs-t="things" data-cs-p="things">
                    <strong class="t"><a data-cs-p="from_wo_nav" href="/mall/things.php">周边</a></strong>
                </li>
                <li data-cs-p="together">
                    <strong class="t"><a href="/together/">结伴</a></strong>
                </li>
                <li data-cs-p="group">
                    <strong class="t"><a href="/group/">小组</a></strong>
                </li>
                <li data-cs-p="mall">
                    <strong class="t"><a href="/club/">俱乐部</a></strong>
                </li>
                <li class="drop" data-cs-p="other">
                    <strong class="t"><a href="/app">更多<b></b></a></strong>
                </li>
            </ul>
        </div>
        <div class="t_info">
            <div data-pagelet id="pagelet-block-2d7ace2c07b64c50aca930ff4a543d39" class="" data-api="apps:user:pagelet:pageViewHeadInfo" data-params="{&quot;type&quot;:2}" data-async="1" data-controller="/js/pageletcommon/pageHeadUserInfoWWWDark"></div>
        </div>
    </div>
</div>

<div class="container">
    <div class="mt-nav clearfix">
    </div>
    <!-- banner -->
    <div class="banner">
        <img src="http://images.mafengwo.net/images/daoju/banner.jpg" alt="">
    </div>
    <div class="row clearfix">
        <div class="col-main">
            <div class="daoju_tab" id="chenge_btn">
                <a id="shop" class="active _j_goods_tab" data-index="0">道具商店</a>
                <a class="_j_goods_tab " data-index="1" >我的道具</a>
                <strong style="left: 0px"></strong>
            </div>
            <script>
                $(function () {
                    $("._j_goods_tab").click(function () {
                        $('#chenge_btn a').removeClass("on");
                        $(this).addClass("on")
                    })
                })
            </script>
            <form class="form-horizontal" action="/prop/p" method="post" id="itemForm">
                <input id="queryType" type="hidden" value="0" name="queryType">
            </form>
            <form class="form-horizontal" action="/prop/comment" method="post" id="commentForm">
            </form>
            <div id="itemBox" class="daoju_list  _j_goods_list" data-flag="0" data-total="2">

            </div>
            <div class="pagin" id="smallpager"></div>

        </div>
        <div class="col-side">

            <div class="side_my">
                <p>你的金币</p>
                <div id="gold" class="gold"><i></i>${(userProp.integral)!}</div>
            </div>
            <form class="form-horizontal" action="/prop/comment" method="post" id="commentForm">
                <input id="queryType" type="hidden" value="0" name="queryType">
            </form>
            <div class="side_cmt">
                <div class="tit">留言板</div>
                <div class="con">
                    <div id="_j_comment_cnt">

                        <div class="addComment clearfix">
                            <div class="pub">
                                <textarea id="textarea" class="_j_comment_pubarea"></textarea>
                                <input type="button"  class="_j_comment_pubbtn" id="btn" value="发表留言">
                            </div>
                        </div>
                        <div class="commentList _j_comment_list"></div>
                        <div class="page _j_comment_pagination"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {

        $("#commentForm").ajaxForm(function (data) {
            $(".commentList").html(data);
        });
        $("#commentForm").submit();


        $("#itemForm").ajaxForm(function (data) {
            $("#itemBox").html(data);
        });
        $("#itemForm").submit();
        $("._j_goods_tab").click(function () {
            var queryType = $(this).data("index");
            $.get("/prop/p", {queryType:queryType}, function (data) {
                $("#itemBox").html(data);
            });
        });
        //回复
        $("._j_reply").on("click", ".re_reply", function () {
            var username = $(this).data("name");
            $('#replyContent'+ cmtid).attr("placeholder", "回复：" + username);
            $('#replyContent'+ cmtid).focus();

            $('#type' + cmtid).val(1);
            $('#refComment' + cmtid).val(replyid);
        });

        $('._j_reply').click(function () {
            var id = $(this).data("id");
            var username = $(this).data("name");
            $('#textarea').attr("placeholder", "回复：" + username);
            $('#textarea').focus();;
        });







    });

    $(function () {
        $(".btn_buy").click(function () {
            var id=$(this).data("id");
            var gold=$(this).data("gold");
            var total=$(this).data("total");
            console.log(1);
            $.post("/prop/buy", {id: id,gold:gold,total:total}, function (data) {
                if(data.success){
                    alert("购买成功");
                }else{
                    alert(data.msg);
                }
            });
        })
    })
</script>
<link href="http://css.mafengwo.net/css/mfw-footer.css?1558532347" rel="stylesheet" type="text/css"/>
<div id="footer">
    <div class="ft-content" style="width: 1105px">
        <div class="ft-copyright">
            <a href="http://www.mafengwo.cn"><i class="ft-mfw-logo"></i></a>
            <p>© 2019 Mafengwo.cn <a href="http://www.miibeian.gov.cn/" target="_blank" rel="nofollow">京ICP备11015476号</a> <a href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=11010502013401" target="_blank"><img src="http://www.wolfcode.cn/themes/simplebootx/Public/self/img/index/school-environment5.png" width="12" style="margin:0 2px 4px 0;">京公网安备11010502013401号</a> <a href="http://images.mafengwo.net/images/about/icp2.jpg" target="_blank" rel="nofollow">京ICP证110318号</a><span class="m_l_10">违法和不良信息举报电话: 010-59222790 举报邮箱: mfwjubao@mafengwo.com</span></p>
            <p>网络出版服务许可证：(总)网出证(京)字第161号<span class="m_l_10">增值电信业务经营许可证：京B2-20180228</span> <a href="https://n1-q.mafengwo.net/s12/M00/A5/45/wKgED1xJi3uAA7KLAAf_CkKLHRQ87.jpeg" target="_blank" rel="nofollow" class="m_l_10">营业执照</a><a href="/sales/uhelp/doc" target="_blank" rel="nofollow" class="m_l_10">帮助中心</a><span class="m_l_10">马蜂窝客服：国内</span><span class="highlight">4006-345-678</span><span class="m_l_10">海外</span> <span class="highlight">+86-10-5922-2799</span></p>
        </div>
        <div class="ft-safety">
            <a class="s-a" target="_blank" href="https://search.szfw.org/cert/l/CX20140627008255008321" id="___szfw_logo___"></a>
            <a class="s-b" href="https://ss.knet.cn/verifyseal.dll?sn=e130816110100420286o93000000&ct=df&a=1&pa=787189" target="_blank" rel="nofollow"></a>
            <a class="s-c" href="http://www.itrust.org.cn/Home/Index/itrust_certifi/wm/1669928206.html" target="_blank" rel="nofollow"></a>
            <a class="s-d" href="http://www.itrust.org.cn/Home/Index/satification_certificate/wm/MY2019051501.html" target="_blank" rel="nofollow"></a>
        </div>

    </div>
</div>
<link href="http://css.mafengwo.net/css/mfw-toolbar.css?1537192876" rel="stylesheet" type="text/css"/>
<div class="mfw-toolbar" id="_j_mfwtoolbar">
    <div class="toolbar-item-top">
        <a role="button" class="btn _j_gotop">
            <i class="icon_top"></i>
            <em>返回顶部</em>
        </a>
    </div>
    <div class="toolbar-item-feedback">
        <a role="button" data-japp="feedback" class="btn">
            <i class="icon_feedback"></i>
            <em>意见反馈</em>
        </a>
    </div>
    <div class="toolbar-item-code">
        <a role="button" class="btn">
            <i class="icon_code"></i>
        </a>
        <a role="button" class="mfw-code _j_code">
            <img src="http://www.wolfcode.cn/themes/simplebootx/Public/self/img/index/school-environment5.png" width="450" height="192" >
        </a>
    </div>
    <div class="toolbar-item-down">
        <a role="button" class="btn _j_gobottom">
            <i class="icon_down"></i>
            <em>页面底部</em>
        </a>
    </div>
</div>
</body>
</html>