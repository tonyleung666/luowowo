
<#list page.content as comment>
    <li class="_j_citem" data-uid="${comment.userId}" data-username="${comment.username}">
        <a href="/userCenter/homepage?uid=${comment.userId}" class="mcmt-photo img photo"><img src="${comment.headUrl}" width="30px" height="30px" style="border-radius: 50%"></a>
        <a href="" class="user">${comment.username}</a>
        <a href="" class="level">LV.${comment.level}</a>
        <a href="#" class="_j_reply" data-id="${comment.id}" data-name="${comment.username}" data-="">回复</a><br>
        <span class="date" style="padding-left: 50px">${comment.createTime?string("yyyy-MM-dd HH:mm:ss")}</span>
            <#if comment.type==0>
                <p style="margin-left: 50px">${comment.content}</p>
            </#if>
            <#if comment.type==1>
                <p style="margin-left: 50px">回复<a href="/userCenter/homepage?uid=${comment.userId}">@${comment.username}</a>:${comment.content}</p
            </#if>
    </li>
</#list>
<div style="float: right">
    <div style="float: left;" ><span style="line-height:30px"> 共${page.totalPages!}页 / ${page.totalElements}条&nbsp;&nbsp;&nbsp;</span></div>
    <div id="pagination" class="jq-pagination" style="display: inline;"></div>
</div>
<script>
    $("#pagination").jqPaginator({
                totalPages: ${page.totalPages!0},
                visiblePages: 5,
                currentPage: ${page.number+1}||1,
            prev: '<a class="prev" href="javascript:void(0);">上一页<\/a>',
            next: '<a class="next" href="javascript:void(0);">下一页<\/a>',
            page: '<a href="javascript:void(0);">{{page}}<\/a>',
            last: '<a class="last" href="javascript:void(0);" >尾页<\/a>',
            onPageChange: function(page, type) {
        if(type == 'change'){
            $("#currentPage").val(page);
            $("#commentForm").submit();
        }

        }
    })
    $(function(){
        var flag=true;
        var username=null;
        $('._j_reply').click(function () {
            var id = $(this).data("id");
            username = $(this).data("name");
            $('#textarea').attr("placeholder", "回复：" + username);
            $('#textarea').focus();


        });
        $("._j_comment_pubbtn").click(function () {
            if (!$("._j_comment_pubarea").val()) {
                alert("评论不能为空");
                return;
            }
            if(username!=null){
                username="回复：" + username;
            }

            var comment=$("._j_comment_pubarea").val();
            $.post("/prop/AddComment", {comment: comment,username:username}, function (data) {
                if(data.success){
                }else{
                    alert(data.msg)
                }
                window.location.reload();
            });
        })
        /*$("#btn").click(function () {
                if (!$("._j_comment_pubarea").val()) {
                    alert("评论不能为空");
                    return;
                }
                var comment=$("._j_comment_pubarea").val();
                $.post("/prop/AddComment", {comment: comment}, function (data) {
                    if(data.success){
                    }else{
                        alert(data.msg)
                    }
                });

            });
        }*/


    })
</script>
</ul>