<script>
    $(function () {
        $('.recommend_tab_l .subject_js:first').addClass('active');
    })
</script>
<div class="main_content_l ticket_icon">
    <ul class="recommend_tab_l">
        <#list tags as t>
            <li class="subject_js" data="${t.id}">${t.tag}<i class="ticket_icon"></i></li>
        </#list>
    </ul>
</div>
<ul class="promotion_list clearfix" style="display: block;" id="listUL">
<#if ts??>
    <#list ts as t>
    <#--推荐门票-->
        <li>
            <a href="/ticket/detail?id=${t.id}" target="_blank" onclick="cmcTag('门票频道页-PC-站点-P4-景点推荐-广州主题乐园-001-广州塔','PC门票频道页景点推荐');">
                <div class="promotion_img_box">
                    <img src="${(t.iandParkImg)!}" width="222" height="150" alt="">

                </div>
                <div class="promotion_footer">
                    <h5 title="${(t.landPark)!}">${(t.landPark)!}</h5>
                    <span class="promotion_comment_b">${t.reputably}% 好评</span>
                    <p><span>¥<dfn>${(t.childPrice)!}</dfn></span><samp>起</samp></p>
                </div>
            </a>
        </li>
    </#list>
</#if>
</ul>