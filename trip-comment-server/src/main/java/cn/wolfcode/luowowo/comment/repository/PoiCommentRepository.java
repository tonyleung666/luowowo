package cn.wolfcode.luowowo.comment.repository;

import cn.wolfcode.luowowo.comment.domain.PoiComment;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PoiCommentRepository extends MongoRepository<PoiComment, String> {

}
