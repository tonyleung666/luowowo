package cn.wolfcode.luowowo.comment.service.impl;

import cn.wolfcode.luowowo.comment.domain.PoiCommentReply;
import cn.wolfcode.luowowo.comment.query.PoiCommentReplyQuery;
import cn.wolfcode.luowowo.comment.repository.PoiCommentReplyRepository;
import cn.wolfcode.luowowo.comment.service.IPoiCommentReplyService;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class PoiCommentReplyServiceImpl implements IPoiCommentReplyService {
    @Autowired
    private PoiCommentReplyRepository repository;

    @Autowired
    private MongoTemplate template;

    public void save(PoiCommentReply comment) {
        comment.setCreateTime(new Date());
        repository.save(comment);
    }

    public Page query(PoiCommentReplyQuery qo) {
        Query query = new Query();
        if (qo.getPoiCommentId() != -1) {
            query.addCriteria(Criteria.where("poiCommentId").is(qo.getPoiCommentId()));
        }

        PageRequest pageable = PageRequest.of(qo.getCurrentPage() - 1, qo.getPageSize(), Sort.Direction.DESC, "createTime");
        //total
        long total = template.count(query, PoiCommentReply.class);


        query.with(pageable);
        //list
        List<PoiCommentReply> list = template.find(query, PoiCommentReply.class);
        List commentList = new ArrayList();
        for (PoiCommentReply comment : list) {
            commentList.add(0,comment);
        }
        return new PageImpl(commentList, pageable, total);
    }

    public PoiCommentReply replyAdd(PoiCommentReply reply) {
        reply.setCreateTime(new Date());
        //如果是评论的评论
        if (reply.getType() == PoiCommentReply.POI_COMMENT_REPLY_REF){
            String refId = reply.getRefReply().getId();
            if (StringUtils.hasLength(refId)){
                PoiCommentReply refReply = repository.findById(refId).get();
                if (refReply !=null){
                    reply.setRefReply(refReply);
                }
            }
        }
        repository.save(reply);
        return reply;
    }

    public List<PoiCommentReply> findByPoiCommentId(String poiCommentId) {
        return repository.findByPoiCommentId(poiCommentId);
    }

    public PoiCommentReply addReply(PoiCommentReply reply) {
        reply.setCreateTime(new Date());
        //如果是评论的评论
        if (reply.getType() == PoiCommentReply.POI_COMMENT_REPLY_REF){
            String refId = reply.getRefReply().getId();
            if (StringUtils.hasLength(refId)){
                PoiCommentReply refReply = repository.findById(refId).get();
                if (refReply !=null){
                    reply.setRefReply(refReply);
                }
            }
        }
        repository.save(reply);
        return reply;
    }

}
