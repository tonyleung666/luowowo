package cn.wolfcode.luowowo.comment.repository;

import cn.wolfcode.luowowo.comment.domain.Answer;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface AnswerRepository extends MongoRepository<Answer,String>{
    List<Answer> findByQuestionId(Long id);
}
