package cn.wolfcode.luowowo.comment.repository;

import cn.wolfcode.luowowo.comment.domain.PoiCommentReply;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PoiCommentReplyRepository extends MongoRepository<PoiCommentReply, String> {

    List<PoiCommentReply> findByPoiCommentId(String poiCommentId);
}
