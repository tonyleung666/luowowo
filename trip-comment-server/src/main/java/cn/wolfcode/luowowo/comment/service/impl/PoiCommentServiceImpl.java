package cn.wolfcode.luowowo.comment.service.impl;

import cn.wolfcode.luowowo.article.service.IPoiService;
import cn.wolfcode.luowowo.comment.domain.PoiComment;
import cn.wolfcode.luowowo.comment.domain.PoiCommentReply;
import cn.wolfcode.luowowo.comment.query.PoiCommentQuery;
import cn.wolfcode.luowowo.comment.repository.PoiCommentRepository;
import cn.wolfcode.luowowo.comment.service.IPoiCommentReplyService;
import cn.wolfcode.luowowo.comment.service.IPoiCommentService;
import cn.wolfcode.luowowo.common.exception.LogicException;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.*;

@Service
public class PoiCommentServiceImpl implements IPoiCommentService {
    @Autowired
    private PoiCommentRepository repository;

    @Autowired
    private MongoTemplate template;

    @Reference
    private IPoiService poiService;

    @Autowired
    private IPoiCommentReplyService replyService;

    public void save(PoiComment comment) {
        comment.setCreateTime(new Date());
        repository.save(comment);
        poiService.updateReplynum(comment.getPoiId(), 1);
    }

    public Page query(PoiCommentQuery qo) {
        Query query = new Query();
        query.addCriteria(Criteria.where("poiId").is(qo.getPoiId()));

        PageRequest pageable = PageRequest.of(qo.getCurrentPage() - 1, qo.getPageSize(), Sort.Direction.DESC, "createTime");
        //total
        long total = template.count(query, PoiComment.class);


        query.with(pageable);
        //list
        List<PoiComment> list = template.find(query, PoiComment.class);
        if (list.size() > 0) {
            for (PoiComment poiComment : list) {
                List<PoiCommentReply> replies = replyService.findByPoiCommentId(poiComment.getId());
                if (replies.size() > 0) {
                    poiComment.setReplies(replies);
                }
            }
        }

        PageImpl page = new PageImpl(list, pageable, total);

        return page;
    }

    public boolean commentThumbUp(String commentId, Long userId) {
        //判断当前操作是点赞还是取消
        //  1.1 判断当前用户是否在集合中
        //  1.2 如果在集合中:取消点赞, 否则执行点赞
        Optional<PoiComment> commentOpt = repository.findById(commentId);
        if (!commentOpt.isPresent()) {
            throw new LogicException("参数异常!");
        }
        PoiComment comment = commentOpt.get();
        List<Long> list = comment.getThumbuplist();
        if (list.contains(userId)) {
            //取消点赞: 点赞数-1, list移除userId
            comment.setThumbupnum(comment.getThumbupnum() - 1);
            list.remove(userId);
        } else {
            //执行点赞: 点赞数+1, list添加userId
            comment.setThumbupnum(comment.getThumbupnum() + 1);
            list.add(userId);
        }
        // 更新数据
        repository.save(comment);
        return list.contains(userId);
    }

    @Override
    public List<PoiComment> getByUserId(Long uid) {
        if(uid==null){
            return Collections.emptyList();
        }
        Query query = new Query();
        query.with(Sort.by(Sort.Direction.ASC,"createTime"));
        query.addCriteria(Criteria.where("userId").is(uid));
        List<PoiComment> poiCommentList = template.find(query, PoiComment.class);
        for (PoiComment cmt : poiCommentList) {
            cmt.setReplies(replyService.findByPoiCommentId(cmt.getId()));
        }
        Collections.sort(poiCommentList, new Comparator<PoiComment>(){
            public int compare(PoiComment cmt1, PoiComment cmt2) {
                return cmt2.getReplies().size() - cmt1.getReplies().size();
            }
        });
        return poiCommentList;
    }

    public PoiComment get(String cmtId) {
        return repository.findById(cmtId).get();
    }

}
