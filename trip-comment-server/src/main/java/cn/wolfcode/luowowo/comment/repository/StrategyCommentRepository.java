package cn.wolfcode.luowowo.comment.repository;

import cn.wolfcode.luowowo.comment.domain.StrategyComment;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StrategyCommentRepository extends MongoRepository<StrategyComment, String> {

}
