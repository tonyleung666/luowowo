package cn.wolfcode.luowowo.comment.repository;

import cn.wolfcode.luowowo.comment.domain.AnswerComment;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface AnswerCommentRepository extends MongoRepository<AnswerComment,String> {
    List<AnswerComment> findByAnswerId(String id);
}
