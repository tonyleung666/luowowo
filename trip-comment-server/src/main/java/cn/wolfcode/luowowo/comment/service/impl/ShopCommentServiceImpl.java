package cn.wolfcode.luowowo.comment.service.impl;

import cn.wolfcode.luowowo.comment.domain.ShopComment;
import cn.wolfcode.luowowo.comment.repository.ShopCommentRepository;
import cn.wolfcode.luowowo.comment.service.IShopCommentService;
import cn.wolfcode.luowowo.common.query.QueryObject;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

@Service
public class ShopCommentServiceImpl implements IShopCommentService {

    @Autowired
    private ShopCommentRepository repository;

    @Autowired
    private MongoTemplate template;

    public void save(ShopComment shopComment) {
        repository.save(shopComment);
    }

    public Page query() {
        Query query = new Query();
        QueryObject qo=new QueryObject();
        PageRequest pageable = PageRequest.of(qo.getCurrentPage() - 1, qo.getPageSize(), Sort.Direction.DESC, "createTime");
        //total
        long total = template.count(query, ShopComment.class);


        query.with(pageable);
        //list
        List<ShopComment> list = template.find(query, ShopComment.class);


        return new PageImpl(list, pageable, total);
    }
}
