package cn.wolfcode.luowowo.comment.service.impl;

import cn.wolfcode.luowowo.article.service.IStrategyService;
import cn.wolfcode.luowowo.comment.domain.StrategyComment;
import cn.wolfcode.luowowo.comment.query.StrategyCommentQuery;
import cn.wolfcode.luowowo.comment.repository.StrategyCommentRepository;
import cn.wolfcode.luowowo.comment.service.IStrategyCommentService;
import cn.wolfcode.luowowo.common.exception.LogicException;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class StrategyCommentServiceImpl implements IStrategyCommentService {
    @Autowired
    private StrategyCommentRepository repository;

    @Autowired
    private MongoTemplate template;
    @Reference
    private IStrategyService strategyService;

    public void save(StrategyComment comment) {
        comment.setCreateTime(new Date());
        repository.save(comment);
    }

    public Page query(StrategyCommentQuery qo) {
        Query query = new Query();
        if (qo.getDetailId() != -1) {
            query.addCriteria(Criteria.where("detailId").is(qo.getDetailId()));
        }

        PageRequest pageable = PageRequest.of(qo.getCurrentPage() - 1, qo.getPageSize(), Sort.Direction.DESC, "createTime");
        //total
        long total = template.count(query, StrategyComment.class);


        query.with(pageable);
        //list
        List<StrategyComment> list = template.find(query, StrategyComment.class);


        return new PageImpl(list, pageable, total);
    }

    public void commentThumbUp(String commentId, Long userId) {
        //判断当前操作是点赞还是取消
        //  1.1 判断当前用户是否在集合中
        //  1.2 如果在集合中:取消点赞, 否则执行点赞
        Optional<StrategyComment> commentOpt = repository.findById(commentId);
        if (!commentOpt.isPresent()) {
            throw new LogicException("参数异常!");
        }
        StrategyComment comment = commentOpt.get();
        List<Long> list = comment.getThumbuplist();
        if (list.contains(userId)) {
            //取消点赞: 点赞数-1, list移除userId
            comment.setThumbupnum(comment.getThumbupnum() - 1);
            list.remove(userId);
        } else {
            //执行点赞: 点赞数+1, list添加userId
            comment.setThumbupnum(comment.getThumbupnum() + 1);
            list.add(userId);
        }
        // 更新数据
        repository.save(comment);

    }

    @Override
    public Page queryByUser(Long userId) {
        StrategyCommentQuery strategyCommentQuery = new StrategyCommentQuery();
        Query query = new Query();
        if(userId!=null){
            query.addCriteria(Criteria.where("userId").is(userId));
        }
        //查询该用户的评论游记的数量
        Long total = template.count(query, StrategyComment.class);//游记数
        PageRequest pageRequest = PageRequest.of(strategyCommentQuery.getCurrentPage() - 1, strategyCommentQuery.getPageSize(),
                Sort.Direction.DESC, "createTime");
        query.with(pageRequest);
        //查询当前显示数据
        List<StrategyComment> list = template.find(query, StrategyComment.class);
        List<Object> objects = new ArrayList<>();
            //Strategy strategy=strategyService.getStrategy(userId);
        for (StrategyComment strategyComment : list) {
            //strategyComment.setStrategyCoverUrl(strategy.getCoverUrl());
            objects.add(strategyComment);
        }
        return new PageImpl(objects ,pageRequest, total);
    }

}
