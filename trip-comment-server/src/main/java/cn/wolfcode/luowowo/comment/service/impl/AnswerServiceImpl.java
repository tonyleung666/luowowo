package cn.wolfcode.luowowo.comment.service.impl;

import cn.wolfcode.luowowo.comment.domain.Answer;
import cn.wolfcode.luowowo.comment.domain.AnswerComment;
import cn.wolfcode.luowowo.comment.query.AnswerQuery;
import cn.wolfcode.luowowo.comment.repository.AnswerRepository;
import cn.wolfcode.luowowo.comment.service.IAnswerCommentService;
import cn.wolfcode.luowowo.comment.service.IAnswerService;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.Date;
import java.util.List;

@Service
public class AnswerServiceImpl implements IAnswerService {

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private MongoTemplate template;

    @Autowired
    private IAnswerCommentService commentService;

    public void save(Answer answer) {
        answer.setAnswerTime(new Date());
        answerRepository.save(answer);
    }

    public List<Answer> findByQuestionId(Long id) {
        return answerRepository.findByQuestionId(id);

    }

    public Page query(AnswerQuery qo) {
        Query query = new Query();
        query.addCriteria(Criteria.where("questionId").is(qo.getQuestionId()));

        PageRequest pageable = PageRequest.of(qo.getCurrentPage() - 1, qo.getPageSize(), Sort.Direction.DESC, "createTime");
        //total
        long total = template.count(query, Answer.class);


        query.with(pageable);
        //list
        List<Answer> list = template.find(query, Answer.class);
        if (list.size() > 0) {
            for (Answer answer : list) {
                List<AnswerComment> replies = commentService.findByAnswerId(answer.getId());
                if (replies.size() > 0) {
                    answer.setReply(replies);
                }
            }
        }

        PageImpl page = new PageImpl(list, pageable, total);

        return page;
    }

    public List<Answer> findViewtop() {
        Query query = new Query();
        // 设置分页信息
        query.skip(1).limit(8);
        // 设置排序规则
        query.with(new Sort(Sort.Direction.DESC,"viewnum"));

        List<Answer> list = template.find(query, Answer.class);
        return list;
    }

    public List<Answer> findthumbuptop() {
        Query query = new Query();
        // 设置分页信息
        query.skip(1).limit(8);
        // 设置排序规则
        query.with(new Sort(Sort.Direction.DESC,"thumbupnum"));

        List<Answer> list = template.find(query, Answer.class);
        return list;
    }

    public List<Answer> findShareTop() {
        Query query = new Query();
        // 设置分页信息
        query.skip(1).limit(8);
        // 设置排序规则
        query.with(new Sort(Sort.Direction.DESC,"sharenum"));

        List<Answer> list = template.find(query, Answer.class);
        return list;
    }

}
