package cn.wolfcode.luowowo.comment.repository;


import cn.wolfcode.luowowo.comment.domain.ShopComment;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ShopCommentRepository extends MongoRepository<ShopComment,String>{
}
