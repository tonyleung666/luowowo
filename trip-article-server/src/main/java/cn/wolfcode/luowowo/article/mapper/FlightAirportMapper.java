package cn.wolfcode.luowowo.article.mapper;

import cn.wolfcode.luowowo.article.domain.FlightAirport;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FlightAirportMapper {

    List<FlightAirport> selectByInitial(@Param("beginInitial") String beginInitial, @Param("endInitial") String endInitial);

    List<FlightAirport> selectHots();

}