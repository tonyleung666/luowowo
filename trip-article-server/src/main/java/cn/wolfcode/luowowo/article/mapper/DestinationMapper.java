package cn.wolfcode.luowowo.article.mapper;

import cn.wolfcode.luowowo.article.domain.Destination;
import cn.wolfcode.luowowo.article.query.DestinationQuery;
import java.util.List;

public interface DestinationMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Destination record);

    Destination selectByPrimaryKey(Long id);

    List<Destination> selectAll();

    int updateByPrimaryKey(Destination record);

    List<Destination> selectDestsByDeep(int deep);


    List<Destination> selectDestByIds(Long[] destIds);

    List<Destination> selectForList(DestinationQuery qo);


    List<Destination> selectSubDestParentId(Long parentId);

    Destination selectDestById(Long destId);

    Long queryCountOfTravelByDest(Long id);

    List<Destination> selectCityOfChina(Long parentId);

    List<Destination> queryDestByParentId(Long destId);
}