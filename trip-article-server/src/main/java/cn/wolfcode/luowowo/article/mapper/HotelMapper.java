package cn.wolfcode.luowowo.article.mapper;

import cn.wolfcode.luowowo.article.domain.Hotel;

import java.util.List;

public interface HotelMapper {


    List<Hotel> queryHotelByDestId(Long destId);
}