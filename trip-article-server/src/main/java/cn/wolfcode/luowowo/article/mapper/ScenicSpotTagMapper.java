package cn.wolfcode.luowowo.article.mapper;

import cn.wolfcode.luowowo.article.domain.ScenicSpotTag;
import java.util.List;

public interface ScenicSpotTagMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ScenicSpotTag record);

    ScenicSpotTag selectByPrimaryKey(Integer id);

    List<ScenicSpotTag> selectAll();

    int updateByPrimaryKey(ScenicSpotTag record);
}