package cn.wolfcode.luowowo.article.service.impl;

import cn.wolfcode.luowowo.article.domain.TicketIndent;
import cn.wolfcode.luowowo.article.mapper.TicketIndentMapper;
import cn.wolfcode.luowowo.article.service.ITicketIndentService;
import cn.wolfcode.luowowo.common.exception.LogicException;
import cn.wolfcode.luowowo.common.util.AssertUtil;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class TicketIndentServiceImpl implements ITicketIndentService {

    @Autowired
    private TicketIndentMapper ticketIndentMapper;

    public TicketIndent save(TicketIndent ticketIndent) {
        AssertUtil.hasLength(ticketIndent.getBuyname(), "昵称不能为空!");
        AssertUtil.hasLength(ticketIndent.getPhone(), "手机号不能为空!");
        String regex = "^((13[0-9])|(14[5,7,9])|(15([0-3]|[5-9]))|(166)|(17[0,1,3,5,6,7,8])|(18[0-9])|(19[8|9]))\\d{8}$";
        if (ticketIndent.getPhone().length() != 11) {
            throw new LogicException("手机号码长度不正确");
        } else {
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(ticketIndent.getPhone());
            if(!m.matches()){
                throw new LogicException("手机号码格式不正确");
            }
        }
        if(ticketIndent.getUserIdcard().length()!=6){
            throw new LogicException("身份证号不在确");
        }

        if(ticketIndent.getUseTime().length()==0){
            throw new LogicException("请选择使用时间");
        }
        ticketIndentMapper.insert(ticketIndent);
        return ticketIndent;
    }

    public void updateByIndent(TicketIndent ticketIndent) {
        ticketIndentMapper.updateByIndent(ticketIndent);
    }

    public TicketIndent selectByIndentId(Long id) {
        return ticketIndentMapper.selectByPrimaryKey(id);
    }
}
