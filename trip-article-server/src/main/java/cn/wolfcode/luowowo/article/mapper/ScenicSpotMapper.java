package cn.wolfcode.luowowo.article.mapper;

import cn.wolfcode.luowowo.article.domain.ScenicSpot;
import cn.wolfcode.luowowo.article.query.ScenicSpotQuery;

import java.util.List;

public interface ScenicSpotMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ScenicSpot record);

    ScenicSpot selectByPrimaryKey(Long id);

    List<ScenicSpot> selectAll();

    int updateByPrimaryKey(ScenicSpot record);

    List<ScenicSpot> selectTicketTop4();

    List<ScenicSpot> query(ScenicSpotQuery qo);
}