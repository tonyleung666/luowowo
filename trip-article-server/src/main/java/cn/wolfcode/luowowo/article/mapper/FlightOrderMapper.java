package cn.wolfcode.luowowo.article.mapper;

import cn.wolfcode.luowowo.article.domain.FlightOrder;

import java.util.List;

public interface FlightOrderMapper {
    /**
     * 新增
     * @param order
     */
    void insert(FlightOrder order);

    /**
     * 根据用户id查航班订单
     * @param uid
     * @return
     */
    List<FlightOrder> selectByUserId(Long uid);
}