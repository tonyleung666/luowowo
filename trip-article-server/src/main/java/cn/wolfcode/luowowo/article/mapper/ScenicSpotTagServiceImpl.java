package cn.wolfcode.luowowo.article.mapper;

import cn.wolfcode.luowowo.article.domain.ScenicSpotTag;
import cn.wolfcode.luowowo.article.service.IScenicSpotTagService;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class ScenicSpotTagServiceImpl implements IScenicSpotTagService {

    @Autowired
    private ScenicSpotTagMapper scenicSpotTagMapper;

    public List<ScenicSpotTag> list() {
        return scenicSpotTagMapper.selectAll();
    }
}
