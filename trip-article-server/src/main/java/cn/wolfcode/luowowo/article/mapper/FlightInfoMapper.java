package cn.wolfcode.luowowo.article.mapper;

import cn.wolfcode.luowowo.article.domain.FlightInfo;
import cn.wolfcode.luowowo.article.query.FlightInfoQuery;

import java.util.List;

public interface FlightInfoMapper {


    /**
     * 查询航班信息
     * @param qo
     * @return
     */
    List<FlightInfo> query(FlightInfoQuery qo);

    /**
     * 查单个
     * @param id
     * @return
     */
    FlightInfo selectByPrimaryKey(Long id);


}