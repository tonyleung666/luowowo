package cn.wolfcode.luowowo.article.mapper;

import cn.wolfcode.luowowo.article.domain.SmallTheme;

import java.util.List;

public interface SmallThemeMapper {

    List<SmallTheme> querySmallThemeByDestId(Long destId);

    List<SmallTheme> querySmallThemeByBigThemeId(Long bigThemeId);
}