package cn.wolfcode.luowowo.article.service.impl;

import cn.wolfcode.luowowo.article.domain.BigTheme;
import cn.wolfcode.luowowo.article.mapper.BigThemeMapper;
import cn.wolfcode.luowowo.article.service.IBigThemeService;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class BigThemeServiceImpl implements IBigThemeService {
    @Autowired
    private BigThemeMapper bigThemeMapper;

    public List<BigTheme> queryBigThemeByDestId(Long id) {
        return bigThemeMapper.queryBigThemeByDestId(id);
    }
}
