package cn.wolfcode.luowowo.article.service.impl;

import cn.wolfcode.luowowo.article.domain.BlackList;
import cn.wolfcode.luowowo.article.mapper.BlackListMapper;
import cn.wolfcode.luowowo.article.service.IBlackListService;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;

import java.util.List;
@Service
public class BlackListServiceImpl implements IBlackListService {
    @Reference
    private BlackListMapper blackListMapper;
    @Override
    public int deleteByPrimaryKey(Long id) {
        return blackListMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(BlackList record) {
        return blackListMapper.insert(record);
    }

    @Override
    public BlackList selectByPrimaryKey(Long id) {
        return blackListMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<BlackList> selectAll() {
        return blackListMapper.selectAll();
    }

    @Override
    public int updateByPrimaryKey(BlackList record) {
        return blackListMapper.updateByPrimaryKey(record);
    }

}
