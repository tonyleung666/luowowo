package cn.wolfcode.luowowo.article.mapper;

import cn.wolfcode.luowowo.article.domain.TicketIndent;

import java.util.List;

public interface TicketIndentMapper {
    int deleteByPrimaryKey(Integer id);

    void insert(TicketIndent record);

    TicketIndent selectByPrimaryKey(Long id);

    List<TicketIndent> selectAll();

    int updateByPrimaryKey(TicketIndent record);

    void updateByIndent(TicketIndent ticketIndent);

}