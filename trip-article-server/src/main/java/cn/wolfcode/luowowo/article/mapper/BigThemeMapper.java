package cn.wolfcode.luowowo.article.mapper;

import cn.wolfcode.luowowo.article.domain.BigTheme;

import java.util.List;

public interface BigThemeMapper {

    List<BigTheme> queryBigThemeByDestId(Long id);
}