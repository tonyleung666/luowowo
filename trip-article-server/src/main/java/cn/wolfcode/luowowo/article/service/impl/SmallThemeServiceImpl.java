package cn.wolfcode.luowowo.article.service.impl;

import cn.wolfcode.luowowo.article.domain.SmallTheme;
import cn.wolfcode.luowowo.article.mapper.SmallThemeMapper;
import cn.wolfcode.luowowo.article.service.ISmallThemeService;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class SmallThemeServiceImpl implements ISmallThemeService {
    @Autowired
    private SmallThemeMapper smallThemeMapper;

    public List<SmallTheme> querySmallThemeByDestId(Long destId) {
        return smallThemeMapper.querySmallThemeByDestId(destId);
    }

    public List<SmallTheme> querySmallThemeByBigThemeId(Long BigThemeId) {
        return smallThemeMapper.querySmallThemeByBigThemeId(BigThemeId);
    }
}
