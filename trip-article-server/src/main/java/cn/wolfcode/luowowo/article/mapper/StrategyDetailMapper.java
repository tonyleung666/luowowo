package cn.wolfcode.luowowo.article.mapper;

import cn.wolfcode.luowowo.article.domain.StrategyDetail;
import cn.wolfcode.luowowo.article.query.StrategyDetailQuery;
import cn.wolfcode.luowowo.cache.vo.StrategyStatisVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface StrategyDetailMapper {
    int deleteByPrimaryKey(Long id);

    int insert(StrategyDetail record);

    StrategyDetail selectByPrimaryKey(Long id);

    List<StrategyDetail> selectAll();

    int updateByPrimaryKey(StrategyDetail record);

    List<StrategyDetail> selectForList(StrategyDetailQuery qo);


    void insertRelation(@Param("detailId") Long detailId, @Param("tagId") Long tagId);

    void deleteRelation(Long detailId);

    List<StrategyDetail> selectViewNumTop3(Long id);

    void updateStatis(StrategyStatisVO vo);

    StrategyDetail queryViewNumTop1(Long destId);

    List<StrategyDetail> selectByIds(@Param("sids") List<Long> sids);
}