package cn.wolfcode.luowowo.article.service.impl;

import cn.wolfcode.luowowo.article.domain.Destination;
import cn.wolfcode.luowowo.article.domain.Region;
import cn.wolfcode.luowowo.article.mapper.DestinationMapper;
import cn.wolfcode.luowowo.article.query.DestinationQuery;
import cn.wolfcode.luowowo.article.service.IDestinationService;
import cn.wolfcode.luowowo.article.service.IRegionService;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class DestinationServiceImpl implements IDestinationService {

    @Autowired
    private DestinationMapper destinationMapper;

    @Autowired
    private IRegionService regionService;


    @Override
    public List<Destination> getDestsByDeep(int deep) {
        return destinationMapper.selectDestsByDeep(deep);
    }

    @Override
    public void saveOrUpdate(Destination destination) {

    }

    @Override
    public List<Destination> getDestByRegionId(Long rid) {

        //rid ==-1  表示查询国内所有省份

        if(rid  == -1){
            //id = 1; 表示中国id
            return destinationMapper.selectSubDestParentId(1L);
        }



        //区域
        Region region = regionService.get(rid);
        //关联的目的地id
        Long[] destIds = region.getRefIds();

        return destinationMapper.selectDestByIds(destIds);
    }


    @Override
    public PageInfo query(DestinationQuery qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        return new PageInfo(destinationMapper.selectForList(qo));
    }

    @Override
    public List<Destination> getToasts(Long parentId) {
        //parentId: 广州
        //: 根 >> 中国  >> 广东 >> 广州
        List<Destination> list = new ArrayList<>();

        createToast(parentId, list);
        Collections.reverse(list);  //列表反序
        return list;
    }

    //创建吐司
    private void createToast(Long parentId, List <Destination> list){
        //广州
        Destination destination = destinationMapper.selectByPrimaryKey(parentId);
        if(destination == null){
            return;
        }

        list.add(destination);

        if(destination.getParent() != null){
            createToast(destination.getParent().getId(), list);
        }

    }

    @Override
    public Destination getCountry(Long id) {
        List<Destination> list = this.getToasts(id);

        if(list != null && list.size() > 0){
            return list.get(0);
        }
        return null;
    }

    @Override
    public Destination getProvince(Long id) {

        //必须是国内的才设置省份

        List<Destination> list = this.getToasts(id);
        if(list != null && list.size() > 0){
            //国外
            if(list.get(0).getId() != 1){
                return null;
            }
        }

        if(list.size() > 1){
            return list.get(1);
        }

       /* Destination destination = destinationMapper.selectByPrimaryKey(id);

        //国家
        if(destination.getDeep() == 1){
            return null;
        }

        //省份
        if (destination.getDeep() == 2){
            return destination;
        }


        //城市
        if (destination.getDeep() == 3){
            return destination.getParent();
        }*/


        return null;
    }

    @Override
    public List<Destination> list() {
        return destinationMapper.selectAll();
    }

    public Destination findDestById(Long destId) {
        return destinationMapper.selectDestById(destId);
    }
    public Long queryCountOfTravelByDest(Long id) {
        return destinationMapper.queryCountOfTravelByDest(id);
    }

    public List<Destination> selectCityOfChina(Long parentId) {
        List<Destination> provinceList = destinationMapper.selectCityOfChina(parentId);
        List<Destination> cityList = new ArrayList<>();

        for (Destination destination : provinceList) {
            if (destination.getName().equals("北京") || destination.getName().equals("上海")
                    || destination.getName().equals("天津") || destination.getName().equals("重庆")
                    || destination.getName().equals("台湾") || destination.getName().equals("香港")
                    || destination.getName().equals("澳门")){
                cityList.add(destination);
            }else {
                List<Destination> cityOfProvinces = destinationMapper.selectCityOfChina(destination.getId());
                for (Destination cityOfProvince : cityOfProvinces) {
                    cityList.add(cityOfProvince);
                }
            }
        }
        if (cityList.size()>40){
            List<Destination> cityList100 = cityList.subList(0, 40);
            return cityList100;
        }
        return cityList;
    }

    public List<Destination> queryDestByParentId(Long destId) {
        return destinationMapper.queryDestByParentId(destId);
    }
}
