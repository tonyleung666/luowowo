package cn.wolfcode.luowowo.article.service.impl;

import cn.wolfcode.luowowo.article.domain.Hotel;
import cn.wolfcode.luowowo.article.mapper.HotelMapper;
import cn.wolfcode.luowowo.article.service.IHotelService;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class HotelServiceImpl  implements IHotelService{
    @Autowired
    private HotelMapper hotelMapper;
    public List<Hotel> queryHotelByDestId(Long destId) {
        return hotelMapper.queryHotelByDestId(destId);
    }
}
