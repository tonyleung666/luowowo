package cn.wolfcode.luowowo.article.service.impl;

import cn.wolfcode.luowowo.article.domain.ScenicSpot;
import cn.wolfcode.luowowo.article.mapper.ScenicSpotMapper;
import cn.wolfcode.luowowo.article.query.ScenicSpotQuery;
import cn.wolfcode.luowowo.article.service.IScenicSpotService;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class ScenicSpotServiceImpl implements IScenicSpotService {

    @Autowired
    private ScenicSpotMapper scenicSpotMapper;


    public List<ScenicSpot> list() {
        return scenicSpotMapper.selectAll();
    }

    public List<ScenicSpot> getTicketTop4() {
        return scenicSpotMapper.selectTicketTop4();
    }

    public List<ScenicSpot> query(ScenicSpotQuery qo) {
        return scenicSpotMapper.query(qo);
    }

    public ScenicSpot querySpotById(Long id) {
        return scenicSpotMapper.selectByPrimaryKey(id);
    }
}
