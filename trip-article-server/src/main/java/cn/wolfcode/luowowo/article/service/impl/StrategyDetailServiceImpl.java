package cn.wolfcode.luowowo.article.service.impl;

import cn.wolfcode.luowowo.article.domain.*;
import cn.wolfcode.luowowo.article.mapper.StrategyContentMapper;
import cn.wolfcode.luowowo.article.mapper.StrategyDetailMapper;
import cn.wolfcode.luowowo.article.query.StrategyDetailQuery;
import cn.wolfcode.luowowo.article.service.IDestinationService;
import cn.wolfcode.luowowo.article.service.IStrategyDetailService;
import cn.wolfcode.luowowo.article.service.IStrategyService;
import cn.wolfcode.luowowo.article.service.IStrategyTagService;
import cn.wolfcode.luowowo.cache.vo.StrategyStatisVO;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

@Service
public class StrategyDetailServiceImpl implements IStrategyDetailService {

    @Autowired
    private StrategyDetailMapper strategyDetailMapper;

    @Autowired
    private StrategyContentMapper strategyContentMapper;

    @Autowired
    private IStrategyService strategyService;

    @Autowired
    private IDestinationService destinationService;

    @Autowired
    private IStrategyTagService strategyTagService;

    @Override
    public PageInfo query(StrategyDetailQuery qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        return new PageInfo(strategyDetailMapper.selectForList(qo));
    }

    @Override
    public List<StrategyDetail> list() {
        return strategyDetailMapper.selectAll();
    }

    @Override
    public StrategyDetail get(Long id) {
        return strategyDetailMapper.selectByPrimaryKey(id);
    }

    public void saveOrUpdate(StrategyDetail detail, String tags) {

        //目的地
        Strategy strategy = strategyService.get(detail.getStrategy().getId());
        detail.setDest(strategy.getDest());

        //摘要
        StrategyContent strategyContent = detail.getStrategyContent();
        String content = strategyContent.getContent();
        if (content.length() > 200) {
            detail.setSummary(content.substring(0, 200));
        } else {
            detail.setSummary(content);
        }

        //是否国外
        List<Destination> toasts = destinationService.getToasts(strategy.getDest().getId());
        detail.setIsabroad(toasts.get(0).getId() != 1);

        if (detail.getId() == null) {
            detail.setCreateTime(new Date());
            strategyDetailMapper.insert(detail);
            strategyContent.setId(detail.getId());
            strategyContentMapper.insert(strategyContent);
        } else {
            strategyDetailMapper.updateByPrimaryKey(detail);
            strategyContent.setId(detail.getId());
            strategyContentMapper.updateByPrimaryKey(strategyContent);
            strategyDetailMapper.deleteRelation(detail.getId());
        }

        if (StringUtils.hasLength(tags)) {
            String[] tagArr = tags.split(",");
            if (tagArr.length > 0) {
                for (String s : tagArr) {
                    StrategyTag tag = new StrategyTag();
                    tag.setName(s);
                    strategyTagService.saveOrUpdate(tag);
                    //维护多对多的关系
                    strategyDetailMapper.insertRelation(detail.getId(), tag.getId());
                }
            }
        }


    }

    public StrategyContent getContent(Long id) {
        return strategyContentMapper.selectByPrimaryKey(id);
    }

    public List<StrategyDetail> queryViewNumTop3(Long id) {
        return strategyDetailMapper.selectViewNumTop3(id);
    }

    public void updateStatis(StrategyStatisVO vo) {
        strategyDetailMapper.updateStatis(vo);
    }

    public StrategyDetail queryViewNumTop1(Long destId) {
        return strategyDetailMapper.queryViewNumTop1(destId);
    }

    public List<StrategyDetail> queryStrategies(List<Long> sids) {
        if (sids == null || sids.size()<=0){
            return null;
        }
        return strategyDetailMapper.selectByIds(sids);
    }
}
