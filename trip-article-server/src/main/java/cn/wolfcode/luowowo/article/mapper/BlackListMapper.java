package cn.wolfcode.luowowo.article.mapper;

import cn.wolfcode.luowowo.article.domain.BlackList;

import java.util.List;

public interface BlackListMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BlackList record);

    BlackList selectByPrimaryKey(Long id);

    List<BlackList> selectAll();

    int updateByPrimaryKey(BlackList record);

}