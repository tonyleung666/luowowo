package cn.wolfcode.luowowo.article.mapper;

import cn.wolfcode.luowowo.article.domain.Poi;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PoiMapper {

    /**
     * 查单个
     * @param id
     * @return
     */
    Poi selectByPrimaryKey(Long id);

    /**
     * 根据上级id查点评数前五的列表
      * @param parentId
     * @return
     */
    List<Poi> selectTop5ByParent(Long parentId);

    /**
     * 修改评论数
     * @param poiId
     * @param num
     */
    void updateReplynum(@Param("poiId") Long poiId, @Param("num") int num);

    /**
     * 根据目的地id查询deep为1的景点
     * @param destId
     * @return
     */
    Poi selectByDestId(Long destId);

    List<Poi> queryTop3ByParentId(Long destId);

    /**
     * 查多个
     * @param pids
     * @return
     */
    List<Poi> selectByIds(@Param("pids") List<Long> pids);

    /**
     * 根据父Id查询
     * @param parentId
     * @return
     */
    List<Poi> selectByParent(Long parentId);
}