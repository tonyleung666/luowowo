package cn.wolfcode.luowowo.search.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class StatisVO implements Serializable {
    private Long id;        //统计的id
    private String name;    //统计的名字
    private Long count;     //统计个数

    public StatisVO(Long id, String name, Long count) {
        this.id = id;
        this.name = name;
        this.count = count;
    }

    public StatisVO(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}
