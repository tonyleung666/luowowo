package cn.wolfcode.luowowo.search.query;

import cn.wolfcode.luowowo.common.query.QueryObject;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DestinationScreenQuery extends QueryObject {

    private String destName = "1";
    private Integer monthNum = -1;

    //查找的类型
    private int type = -1;
    //日期类型
    private Long timeId = -1L;
    private Long themeId = -1L;
    private Long dayId = -1L;


   private Long bigThemeId = -1L;
}
