package cn.wolfcode.luowowo.search.service;

import cn.wolfcode.luowowo.search.domain.TravelTemplate;

import java.util.List;

public interface ITravelSearchService {
    void save(TravelTemplate template);


    /**
     * 根据目的地名称查询
     * @param destName
     * @return
     */
    List<TravelTemplate> findByDestName(String destName);
}
