package cn.wolfcode.luowowo.search.query;

import cn.wolfcode.luowowo.common.query.QueryObject;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@Getter
@Setter
public class StrategySearchQuery extends QueryObject {
    public static final int CONDITION_TYPE_ABROAD = 0;
    public static final int CONDITION_TYPE_CHINA = 1 ;
    public static final int CONDITION_TYPE_THEME = 2;


    private String orderBy = "viewnum";
    private int type = -1;
    private Long typeValue = -1L;

    public Pageable getPageable(){
        return PageRequest.of(super.getCurrentPage()-1, super.getPageSize());
    }

}
