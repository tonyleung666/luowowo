package cn.wolfcode.luowowo.search.service;

import cn.wolfcode.luowowo.search.domain.UserInfoTemplate;

import java.util.List;

public interface IUserInfoSearchService {
    void save(UserInfoTemplate template);

    /**
     * 根据目的地名称查询
     * @param destName
     * @return
     */
    List<UserInfoTemplate> findByDestName(String destName);
}
