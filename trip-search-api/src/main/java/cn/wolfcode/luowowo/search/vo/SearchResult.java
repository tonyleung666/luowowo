package cn.wolfcode.luowowo.search.vo;

import cn.wolfcode.luowowo.search.domain.DestinationTemplate;
import cn.wolfcode.luowowo.search.domain.StrategyTemplate;
import cn.wolfcode.luowowo.search.domain.TravelTemplate;
import cn.wolfcode.luowowo.search.domain.UserInfoTemplate;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class SearchResult implements Serializable {
    Long total = 0L;

    List<DestinationTemplate> dests = new ArrayList<>();
    List<StrategyTemplate> strategys = new ArrayList<>();
    List<TravelTemplate> travels = new ArrayList<>();
    List<UserInfoTemplate> users = new ArrayList<>();
}
