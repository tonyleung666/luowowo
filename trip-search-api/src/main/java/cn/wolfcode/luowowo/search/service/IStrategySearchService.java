package cn.wolfcode.luowowo.search.service;

import cn.wolfcode.luowowo.search.domain.StrategyTemplate;
import cn.wolfcode.luowowo.search.query.StrategySearchQuery;
import cn.wolfcode.luowowo.search.vo.StatisVO;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

public interface IStrategySearchService {
    /**
     * 保存
     * @param template
     */
    void save(StrategyTemplate template);

    /**
     * 主题推荐
     * @return
     */
    List<Map<String,Object>> queryThemeCommend();


    /**
     * 主题条件
     * @return
     */
    List<StatisVO> queryConditionThemes();


    /**
     * 国内条件
     * @return
     */
    List<StatisVO> queryConditionProvinces();

    /**
     * 国外条件
     * @return
     */
    List<StatisVO> queryConditionCountries();

    /**
     * 分页查询
     * @param qo
     * @return
     */
    Page query(StrategySearchQuery qo);

    /**
     * 根据目的地名称查询
     * @param destName
     * @return
     */
    List<StrategyTemplate> findByDestName(String destName);
}
