package cn.wolfcode.luowowo.search.service;


import cn.wolfcode.luowowo.search.domain.DestScreenVO;
import cn.wolfcode.luowowo.search.query.DestinationScreenQuery;

import java.util.List;

public interface IDestinationScreenService {
    /**
     * 通过月份查询目的地
     * @param monthNum
     * @return
     */
    List<DestScreenVO> queryDestScreenVO(Integer monthNum);

    /**
     * 保存DestScreenVO
     * @param vo
     */
    void save(DestScreenVO vo);

    /**
     * 通过qo去查vo
     * @param qo
     * @return
     */
    List<DestScreenVO> queryDestScreenVOByQo(Long timeId,Long themeId,Long dayId,Integer monthNum);
    //List<DestScreenVO> queryDestScreenVOByQo(DestinationScreenQuery qo);

    List<DestScreenVO> queryDestScreenVOByDestName(Long timeId, Long themeId, Long dayId, Integer monthNum, String destName);
    //List<DestScreenVO> queryDestScreenVOByQo(DestinationScreenQuery qo);

}
