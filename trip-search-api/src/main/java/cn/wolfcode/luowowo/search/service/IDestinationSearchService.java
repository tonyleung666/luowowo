package cn.wolfcode.luowowo.search.service;

import cn.wolfcode.luowowo.search.domain.DestinationTemplate;

public interface IDestinationSearchService {
    void save(DestinationTemplate template);

    /**
     * 根据目的地名称查询
     * @param name
     * @return
     */
    DestinationTemplate findByName(String name);
}
