package cn.wolfcode.luowowo.search.domain;

import cn.wolfcode.luowowo.article.domain.Poi;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.util.List;

/**
 * 攻略redis中统计数据
 * 运用模块：
 *  1：数据统计(回复，点赞，收藏，分享，查看)
 *  2：排行数据查询
 */
@Getter
@Setter
@Document(indexName = "luowowo_destscreen", type = "destscreen")
public class DestScreenVO implements Serializable {
    public static final String INDEX_NAME = "luowowo_destscreen";
    public static final String TYPE_NAME = "destscreen";
    //@Field 每个文档的字段配置（类型、是否分词、是否存储、分词器 ）
    @Id
    @Field(store=true, index = true,type = FieldType.Long)
    //@Field(store=true, index = true,type = FieldType.Long)
    private Long destId;//目的地id

    @Field(index = true, store = true, type = FieldType.Keyword)
    private String destName;//目的地名称
    @Field(index = true, store = true, type = FieldType.Keyword)
    private String coverUrl;//目的地背景
    @Field(index = true, analyzer = "ik_max_word", store = true, searchAnalyzer = "ik_max_word", type = FieldType.Keyword)
    //@Field(index = true, analyzer = "ik_max_word", store = true, searchAnalyzer = "ik_max_word", type = FieldType.Text)
    private String info;//目的地信息
    @Field(store = true, index = false, type = FieldType.Long)
    //@Field(store = true, index = true, type = FieldType.Long)
    private Long strategyDetailId;  //攻略id
    //攻略title
    //@Field(store = true, index = true, type = FieldType.Long)
    @Field(store = true, index = false, type = FieldType.Long)
    private Long count = 0L;//目的地在各个月份下的游记数量

    //private List<Travel> travels;//目的地的游记集合
    //月份
    @Field(index = true,  type = FieldType.Keyword)
    private List<Integer> monthNums;

    //小主题的id集合
    @Field(index = true,  type = FieldType.Keyword)
    private List<Long> smallThemeId;
    /*//景点id
    @Field(store = true, index = true, type = FieldType.Long)
    private Long sceneryId;
    //景点名称
    @Field(index = true, analyzer = "ik_max_word", store = true, searchAnalyzer = "ik_max_word", type = FieldType.Text)
    private String sceneryName;*/

    //游记id
   /* @Field(store = true, index = true, type = FieldType.Long)
    private Long travelId;*/
    //游记天数集合
    /*@Field(store = true, index = true, type = FieldType.Integer)
    private List<Integer> days;*/
    //@Field(index = true, analyzer = "ik_max_word", store = true, searchAnalyzer = "ik_max_word", type = FieldType.Keyword)
    @Field(index = false, type = FieldType.Keyword)
    private String title;//攻略title
    //景点对象集合
    @Field(index = false, type = FieldType.Keyword)
    private List<Poi> poiList;

}
