package cn.wolfcode.luowowo.comment.query;

import cn.wolfcode.luowowo.common.query.QueryObject;
import lombok.Getter;
import lombok.Setter;

@Setter@Getter
public class PoiCommentQuery extends QueryObject {
    private Long poiId = -1L;
}
