package cn.wolfcode.luowowo.comment.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 景点点评
 */
@Setter
@Getter
@Document("poi_comment")
public class PoiComment implements Serializable {
    @Id
    private String id;  //mongodb默认的是id
    private Long poiId;  //攻略(明细)id
    private String poiName; //游记标题
    private Long userId;    //用户id
    private String username;  //用户名
    private int level;          //用户等级
    private String headUrl;     //头像
    private Date createTime;    //创建时间
    private String content;      //评论内容
    private int overallStar;    //总体评分
    private int sceneStar;    //风光评分
    private int featuresStar;    //特色评分
    private int serviceStar;    //服务评分
    private String grade;    //评价

    private String[] imgUrls;      //点评的图片
    private int thumbupnum = 0;     //点赞数
    private List<Long> thumbuplist = new ArrayList<>(); //点赞用户的id集合

    private List<PoiCommentReply> replies;

    public void setImgUrls(String imgUrls){
        if (StringUtils.hasLength(imgUrls)) {
            this.imgUrls = imgUrls.split(";");
        }
    }

    public String getGrade(){
        if (overallStar<2){
            return "弱爆了!";
        }
        if (overallStar >= 2 && overallStar <=3){
            return "还行";
        }
        return "棒棒哒!";
    }
}