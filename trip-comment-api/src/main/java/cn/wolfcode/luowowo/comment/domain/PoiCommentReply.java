package cn.wolfcode.luowowo.comment.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

/**
 * 游记评论
 */
@Setter
@Getter
@Document("poi_comment_reply")
public class PoiCommentReply implements Serializable {
    public static final int POI_COMMENT_REPLY_NOMAL = 0; //普通评论
    public static final int POI_COMMENT_REPLY_REF = 1; //评论的评论
    @Id
    private String id;  //id
    private String poiCommentId;  //景点点评id
    private Long userId;    //用户id
    private String username; //用户名
    private String headUrl;   // 用户头像
    private int type = POI_COMMENT_REPLY_NOMAL; //评论类别
    private Date createTime; //创建时间
    private String content;  //评论内容
    private PoiCommentReply refReply;  //关联的评论
}
