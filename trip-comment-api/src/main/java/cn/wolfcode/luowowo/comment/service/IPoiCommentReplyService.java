package cn.wolfcode.luowowo.comment.service;

import cn.wolfcode.luowowo.comment.domain.PoiCommentReply;
import cn.wolfcode.luowowo.comment.query.PoiCommentReplyQuery;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * 景点点评评论服务
 */
public interface IPoiCommentReplyService {
    /**
     * 添加评论
     * @param reply
     */
    void save(PoiCommentReply reply);

    /**
     * 分页查询
     * @param qo
     * @return
     */
    Page query(PoiCommentReplyQuery qo);

    PoiCommentReply replyAdd(PoiCommentReply reply);

    /**
     * 根据景点点评id查评论
     * @param poiCommentId
     * @return
     */
    List<PoiCommentReply> findByPoiCommentId(String poiCommentId);

    /**
     * 新增景点点评评论
     * @param reply
     * @return
     */
    PoiCommentReply addReply(PoiCommentReply reply);
}
