package cn.wolfcode.luowowo.comment.service;

import cn.wolfcode.luowowo.comment.domain.TravelComment;
import cn.wolfcode.luowowo.comment.query.TravelCommentQuery;
import org.springframework.data.domain.Page;

/**
 * 游记评论服务
 */
public interface ITravelCommentService {
    /**
     * 添加评论
     * @param comment
     */
    void save(TravelComment comment);

    /**
     * 分页查询
     * @param qo
     * @return
     */
    Page query(TravelCommentQuery qo);

    TravelComment commentAdd(TravelComment comment);

    long getCount(Long id);
}
