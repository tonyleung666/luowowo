package cn.wolfcode.luowowo.comment.service;

import cn.wolfcode.luowowo.comment.domain.StrategyComment;
import cn.wolfcode.luowowo.comment.query.StrategyCommentQuery;
import org.springframework.data.domain.Page;

/**
 * 攻略评论服务
 */
public interface IStrategyCommentService {
    void save(StrategyComment comment);

    Page query(StrategyCommentQuery qo);

    /**
     * 攻略评论点赞
     * @param commentId
     * @param userId
     */
    void commentThumbUp(String commentId, Long userId);

    /**
     * 查询当前用户的评论
     * @param userId
     * @return
     */
    Page queryByUser(Long userId);
}
