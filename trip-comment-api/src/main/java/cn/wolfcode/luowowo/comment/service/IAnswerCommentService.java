package cn.wolfcode.luowowo.comment.service;


import cn.wolfcode.luowowo.comment.domain.AnswerComment;
import cn.wolfcode.luowowo.comment.query.AnswerCommentQuery;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IAnswerCommentService  {
    AnswerComment save(AnswerComment comment);

    Page query(AnswerCommentQuery qo);

    List<AnswerComment> findByAnswerId(String id);

}
