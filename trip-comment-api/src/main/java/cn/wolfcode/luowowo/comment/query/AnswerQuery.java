package cn.wolfcode.luowowo.comment.query;

import cn.wolfcode.luowowo.common.query.QueryObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AnswerQuery extends QueryObject {
    private Long questionId=-1L;
}
