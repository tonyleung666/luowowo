package cn.wolfcode.luowowo.comment.service;

import cn.wolfcode.luowowo.comment.domain.Answer;
import cn.wolfcode.luowowo.comment.query.AnswerQuery;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IAnswerService {
    void save(Answer answer);

    List<Answer> findByQuestionId(Long id);

    Page query(AnswerQuery qo);

    List<Answer> findViewtop();

    List<Answer>  findthumbuptop();

    List<Answer> findShareTop();
}
