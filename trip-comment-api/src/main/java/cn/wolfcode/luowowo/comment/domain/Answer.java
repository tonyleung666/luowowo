package cn.wolfcode.luowowo.comment.domain;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Document("answer")
@ToString
public class Answer implements Serializable {
    @Id
    private String id;
    private Long userId;
    private String username;
    private int userLavel;
    private String headUrl;
    private String content;
    private Date answerTime;//回复时间
    private Long questionId;
    private String coverUrl;//封面
    private int viewnum;//阅读数
    private List<Long> viewlist=new ArrayList<>();
    private int thumbupnum;//点赞数
    private List<Long> thumbuplist=new ArrayList<>();
    private int favornum;
    private List<Long> favorlist=new ArrayList<>();
    private int sharenum;//收藏数
    private List<Long> sharelist=new ArrayList<>();
    private Long tagId;//标签
    private Long destId;//目的地
    private int cummentnum;

    private List<AnswerComment> reply;
}
