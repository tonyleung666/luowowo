package cn.wolfcode.luowowo.comment.query;

import cn.wolfcode.luowowo.common.query.QueryObject;
import lombok.Getter;
import lombok.Setter;

@Setter@Getter
public class PoiCommentReplyQuery extends QueryObject {
    private Long poiCommentId = -1L;
}
