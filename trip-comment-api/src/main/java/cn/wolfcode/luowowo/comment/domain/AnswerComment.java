package cn.wolfcode.luowowo.comment.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Document("answer_comment")
public class AnswerComment implements Serializable{
    public static final int ANSWER_COMMENT_TYPE_COMMENT = 0; //普通评论
    public static final int ANSWER_COMMENT_TYPE = 1; //评论的评论
    @Id
    private String id;  //mongodb默认的是id
    private String answerId;  //回答id
    private Long userId;    //用户id
    private String username;  //用户名
    private int level;//等级
    private String headUrl;     //头像
    private Date createTime;    //创建时间
    private String content;//评论的内容
    private int type = ANSWER_COMMENT_TYPE_COMMENT;//评论内容
    private AnswerComment refComment;//关联的评论
    private int thumbupnum;     //点赞数
    private List<Long> thumbuplist = new ArrayList<>();

    private int replyNum;//回复数
}
