package cn.wolfcode.luowowo.comment.service;


import cn.wolfcode.luowowo.comment.domain.ShopComment;
import org.springframework.data.domain.Page;

public interface IShopCommentService {
    void save(ShopComment shopComment);

    Page query();
}
