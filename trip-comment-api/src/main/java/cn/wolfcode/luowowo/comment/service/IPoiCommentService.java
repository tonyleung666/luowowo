package cn.wolfcode.luowowo.comment.service;

import cn.wolfcode.luowowo.comment.domain.PoiComment;
import cn.wolfcode.luowowo.comment.query.PoiCommentQuery;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * 景点点评服务
 */
public interface IPoiCommentService {
    /**
     * 新增点评
     * @param comment
     */
    void save(PoiComment comment);

    /**
     * 查询点评列表
     * @param qo
     * @return
     */
    Page query(PoiCommentQuery qo);

    /**
     * 景点点评点赞
     * @param commentId
     * @param userId
     */
    boolean commentThumbUp(String commentId, Long userId);

    /**
     * 根据用户id查询该用户发表的点评
     * @param uid
     * @return
     */
    List<PoiComment> getByUserId(Long uid);

    /**
     * 查单个
     * @param cmtId
     * @return
     */
    PoiComment get(String cmtId);
}
