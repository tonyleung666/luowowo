package cn.wolfcode.luowowo.common.util;

import cn.wolfcode.luowowo.common.exception.LogicException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 参数判断工具
 */
public interface AssertUtil {
    /**
     * 判断指定value是否为空, 如果为空抛出逻辑异常, 异常信息为msg
     *
     * @param value
     * @param msg
     */
    public static void hasLength(String value, String msg) {
        if (value == null || "".equals(value.trim())) {
            throw new LogicException(msg);
        }
    }

    /**
     * 判断v1 v2 是否一致, 不一致则抛出LogicException, 异常信息为msg
     *
     * @param v1
     * @param v2
     * @param msg
     */
    public static void isEquels(String v1, String v2, String msg) {
        if (v1 == null || v2 == null) {
            throw new RuntimeException("参数不能为空!");
        }

        if (!v1.equals(v2)) {
            throw new LogicException(msg);
        }
    }

    /**
     * 判断phone是否为手机号码
     *
     * @param phone
     * @return
     */
    public static void isMobile(String phone) {
        Pattern p = null;
        Matcher m = null;
        String s2 = "^[1](([3|5|8][\\d])|([4][4,5,6,7,8,9])|([6][2,5,6,7])|([7][^9])|([9][1,8,9]))[\\d]{8}$";// 验证手机号
        hasLength(phone, "手机号不能为空!");
        p = Pattern.compile(s2);
        m = p.matcher(phone);
        if (!m.matches()){
            throw new LogicException("请输入正确的手机号码!");
        }
    }
}
