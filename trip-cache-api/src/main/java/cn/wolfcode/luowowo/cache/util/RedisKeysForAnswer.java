package cn.wolfcode.luowowo.cache.util;

import lombok.Getter;
import lombok.Setter;

@Getter
public enum  RedisKeysForAnswer {


    //回答顶的key
    Answer_STATIS_THUMB("answer_statis_thumb", -1L);

    @Setter
    private String prefix;
    @Setter
    private Long time;

    private RedisKeysForAnswer(String prefix, Long time) {
        this.prefix = prefix;
        this.time = time;
    }

    public String join(String... keys) {
        StringBuilder sb = new StringBuilder(50);
        sb.append(this.prefix);
        if (keys != null && keys.length > 0) {
            for (String key : keys) {
                sb.append(":").append(key);
            }
        }
        return sb.toString();
    }
}
