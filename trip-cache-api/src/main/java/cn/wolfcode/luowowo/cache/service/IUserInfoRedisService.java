package cn.wolfcode.luowowo.cache.service;

import cn.wolfcode.luowowo.member.domain.UserInfo;

/**
 * 用户redis缓存操作服务
 */
public interface IUserInfoRedisService {
    /**
     * 储存短信验证码
     * @param phone 用户手机号
     * @param code 短信验证码
     */
    void setVerifyCode(String phone, String code);
    /**
     * 获取短信验证码
     * @param phone 用户手机号
     */
    String getVerifyCode(String phone);

    /**
     * 判断是否可以重发短信验证码
     * @param phone
     */
    void isResendVerifyCode(String phone);

    /**
     * 存储令牌
     * @param token
     * @param userInfo
     */
    void setToken(String token, UserInfo userInfo);

    /**
     * 通过令牌获取当前登录用户
     * @param token
     * @return
     */
    UserInfo getUserByToken(String token);

    /**
     * 查询验证码
     * @param key
     * @return
     */
    String getNewVerifyCode(String key);
}
