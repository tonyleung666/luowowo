package cn.wolfcode.luowowo.cache.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 游记redis中统计数据
 * 运用模块：
 *  1：数据统计(回复，点赞，收藏，分享，查看)
 */
@Getter
@Setter
@ToString
public class TravelStatisVO implements Serializable {
    private Long TravelId;  //游记id

    private int viewnum;  //点击数
    private int replynum;  //游记评论数
    private int favornum; //收藏数
    private int sharenum; //分享数
    private int thumbsupnum; //点赞数
}

