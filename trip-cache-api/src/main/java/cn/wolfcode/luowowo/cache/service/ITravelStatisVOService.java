package cn.wolfcode.luowowo.cache.service;

import cn.wolfcode.luowowo.cache.vo.TravelStatisVO;

import java.util.List;

/**
 * 游记数据统计服务
 */
public interface ITravelStatisVOService {
    /**
     * 阅读数增加
     *
     * @param sid
     * @param num
     */
    void increaseViewnum(Long sid, int num);

    /**
     * 获取vo对象
     *
     * @param tid
     * @return
     */
    TravelStatisVO getTravelStatisVO(Long tid);

    /**
     * 评论数增加
     *
     * @param sid
     * @param num
     */
    void increaseReplynum(Long sid, int num);

    /**
     * 收藏
     *
     * @param uid
     * @param sid
     * @return true: 收藏; false: 取消收藏
     */
    boolean favor(Long uid, Long sid);

    /**
     * 顶攻略
     *
     * @param uid
     * @param sid
     * @return true:顶;  false: 今天顶过了
     */
    boolean travelThumbup(Long uid, Long sid);

    /**
     * 判断redis中是否存在这个vo的key
     *
     * @param sid
     * @return true: 存在, false:不存在
     */
    boolean voKeyExists(Long sid);

    /**
     * 将vo存到redis中
     *
     * @param vo
     */
    void setVO(TravelStatisVO vo);

    /**
     * 根据key的规则获取vo列表
     *
     * @param pattern
     * @return
     */
    List<TravelStatisVO> getVOs(String pattern);

    /**
     * 分享游记
     * @param tid
     */
    void travelShare(Long tid);

    /**
     * 根据用户id查询收藏的游记id集合
     * @param uid
     * @return
     */
    List<Long> getFavorTravelsByUser(Long uid);
}
