package cn.wolfcode.luowowo.cache.service;

import java.util.List;

/**
 * 景点redis缓存操作服务
 */
public interface IPoiRedisService {
    /**
     * 收藏
     * @param uid
     * @param pid
     * @return true: 收藏; false: 取消收藏
     */
    boolean favor(Long uid, Long pid);

    /**
     * 根据用户id查询收藏的景点id集合
     * @param uid
     * @return
     */
    List<Long> getFavorPoisByUser(Long uid);

}
