package cn.wolfcode.luowowo.cache.util;

import cn.wolfcode.luowowo.common.util.Consts;
import lombok.Getter;
import lombok.Setter;

/**
 * redis所有key的集合
 */
@Getter
public enum RedisKeys {
    //用户收藏游记的key
    USER_FAVOR_TRAVELS("user_favor_travels", -1L),

    //用户收藏攻略的key
    USER_FAVOR_STRATEGIES("user_favor_strategies", -1L),

    //用户收藏景点的key
    USER_FAVOR_POIS("user_favor_pois", -1L),

    //攻略国内推荐的key
    STRATEGY_ZSET_INLAND("strategy_zset_inland", -1L),

    //攻略海外推荐的key
    STRATEGY_ZSET_ABROAD("strategy_zset_abroad", -1L),

    //攻略热门推荐的key
    STRATEGY_ZSET_HOT("strategy_zset_hot", -1L),

    //攻略顶的key
    STRATEGY_STATIS_THUMB("strategy_statis_thumb", -1L),

    //攻略统计对象key
    STRATEGY_STATIS_VO("strategy_statis_vo", -1L),

    //游记顶的key
    TRAVEL_STATIS_THUMB("travel_statis_thumb", -1L),


    //游记统计对象key
    TRAVEL_STATIS_VO("travel_statis_vo", -1L),

     //验证码key
    VERIFY_CODE("verify_code", Consts.VERIFY_CODE_VAI_TIME * 60L),

    //登录令牌key
    LOGIN_TOKEN("login_token", Consts.USER_INFO_TOKEN_VAI_TIME * 60L);

    @Setter
    private String prefix;
    @Setter
    private Long time;

    private RedisKeys(String prefix, Long time) {
        this.prefix = prefix;
        this.time = time;
    }

    public String join(String... keys) {
        StringBuilder sb = new StringBuilder(50);
        sb.append(this.prefix);
        if (keys != null && keys.length > 0) {
            for (String key : keys) {
                sb.append(":").append(key);
            }
        }
        return sb.toString();
    }
}
