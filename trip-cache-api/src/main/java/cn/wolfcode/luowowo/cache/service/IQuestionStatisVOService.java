package cn.wolfcode.luowowo.cache.service;

import cn.wolfcode.luowowo.cache.vo.QuestionStatisVO;
import cn.wolfcode.luowowo.member.domain.UserInfo;

public interface IQuestionStatisVOService {
    boolean QuestionThumbup(Long id, String tid);

    QuestionStatisVO getQuestionStatisVO(String tid, UserInfo userInfo);
}
