package cn.wolfcode.luowowo.cache.service;

import cn.wolfcode.luowowo.cache.vo.StrategyStatisVO;

import java.util.List;

/**
 * 数据统计服务
 */
public interface IStrategyStatisVOService {
    /**
     * 阅读数增加
     * @param sid
     * @param num
     */
    void increaseViewnum(Long sid, int num);

    /**
     * 获取vo对象
     * @param sid
     * @return
     */
    StrategyStatisVO getStrategyStatisVO(Long sid);

    /**
     * 评论数增加
     * @param sid
     * @param num
     */
    void increaseReplynum(Long sid, int num);

    /**
     * 收藏
     * @param uid
     * @param sid
     * @return true: 收藏; false: 取消收藏
     */
    boolean favor(Long uid, Long sid);

    /**
     * 顶攻略
     * @param uid
     * @param sid
     * @return true:顶;  false: 今天顶过了
     */
    boolean strategyThumbup(Long uid, Long sid);

    /**
     * 分享攻略
     * @param sid
     */
    void strategyShare(Long sid);

    /**
     * 判断redis中是否存在这个vo的key
     * @param sid
     * @return true: 存在, false:不存在
     */
    boolean voKeyExists(Long sid);

    /**
     * 将vo存到redis中
     * @param vo
     */
    void setVO(StrategyStatisVO vo);

    /**
     * 根据key的规则获取vo列表
     * @param pattern
     * @return
     */
    List<StrategyStatisVO> getVOs(String pattern);

    /**
     * 攻略热门推荐加分
     * @param sid
     * @param score
     */
    void addHotScore(Long sid, int score);


    /**
     * 攻略国内外推荐加分
     * @param sid
     * @param score
     */
    void addAbroadScore(Long sid, int score);

    /**
     * 获取攻略热门推荐列表
     * @return
     */
    List<StrategyStatisVO> queryHotCommends();

    /**
     * 获取攻略海外推荐列表
     * @return
     */
    List<StrategyStatisVO> queryAbroadCommends();

    /**
     * 获取攻略国内推荐列表
     * @return
     */
    List<StrategyStatisVO> queryInlandCommends();

    /**
     * 判断指定推荐的ZSet中是否包含某个column
     * @param cdKey
     * @param sid
     * @return true:存在,  false:不存在
     */
    boolean isColumnExists(String cdKey, Long sid);

    /**
     * 根据用户id查询收藏的攻略id集合
     * @param uid
     * @return
     */
    List<Long> getFavorStrategiesByUser(Long uid);
}
