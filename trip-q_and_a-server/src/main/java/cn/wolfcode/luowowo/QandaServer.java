package cn.wolfcode.luowowo;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableDubbo
@MapperScan("cn.wolfcode.luowowo.qanda.mapper")
public class QandaServer {
    public static void main(String[] args) {
        SpringApplication.run(QandaServer.class,args);
    }
}
