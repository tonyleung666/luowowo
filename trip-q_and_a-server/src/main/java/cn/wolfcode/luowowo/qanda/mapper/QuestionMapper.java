package cn.wolfcode.luowowo.qanda.mapper;

import cn.wolfcode.luowowo.qanda.domain.Question;

import java.util.List;

public interface QuestionMapper {

    List<Question> selectAll();

    void insert(Question question);

    Question selectByQuestionId(Long id);
}
