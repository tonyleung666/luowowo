package cn.wolfcode.luowowo.qanda.service.impl;

import cn.wolfcode.luowowo.qanda.domain.QuestionTag;
import cn.wolfcode.luowowo.qanda.mapper.QuestionTagMapper;
import cn.wolfcode.luowowo.qanda.service.IQuestionTagService;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class QuestionTagServiceImpl implements IQuestionTagService {

    @Autowired
    private QuestionTagMapper questionTagMapper;

    public List<QuestionTag> query() {
        return questionTagMapper.selectAll();
    }

    public QuestionTag findTagById(Long tagId) {
        return questionTagMapper.selectByTagId(tagId);
    }
}
