package cn.wolfcode.luowowo.qanda.mapper;

import cn.wolfcode.luowowo.qanda.domain.QuestionContent;

public interface QuestionContentMapper {
    void insert(QuestionContent questionContent);

    QuestionContent selectContentById(Long id);
}
