package cn.wolfcode.luowowo.qanda.mapper;

import cn.wolfcode.luowowo.qanda.domain.QuestionTag;

import java.util.List;

public interface QuestionTagMapper {
    List<QuestionTag> selectAll();

    QuestionTag selectByTagId(Long id);
}
