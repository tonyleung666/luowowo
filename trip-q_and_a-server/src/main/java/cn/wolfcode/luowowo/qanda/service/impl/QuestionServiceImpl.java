package cn.wolfcode.luowowo.qanda.service.impl;

import cn.wolfcode.luowowo.member.domain.UserInfo;
import cn.wolfcode.luowowo.qanda.domain.Question;
import cn.wolfcode.luowowo.qanda.domain.QuestionContent;
import cn.wolfcode.luowowo.qanda.mapper.QuestionContentMapper;
import cn.wolfcode.luowowo.qanda.mapper.QuestionMapper;
import cn.wolfcode.luowowo.qanda.service.IQuestionService;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

@Service
public class QuestionServiceImpl implements IQuestionService {

    @Autowired
    private QuestionMapper questionMapper;

    @Autowired
    private QuestionContentMapper questionContentMapper;
    public void insert(Question question, UserInfo userInfo) {

        question.setCreateTime(new Date());
        question.setUser(userInfo);
        questionMapper.insert(question);
        QuestionContent questionContent=question.getContent();
        questionContent.setId(question.getId());
        questionContentMapper.insert(questionContent);
    }

    public List<Question> query() {

        return questionMapper.selectAll();
    }

    public Question selectQuestionById(Long id) {
        Question question = questionMapper.selectByQuestionId(id);
        question.setContent(questionContentMapper.selectContentById(id));
        return question;
    }
}
